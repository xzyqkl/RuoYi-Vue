package com.ruoyi.system.service.impl;

import com.ruoyi.common.service.AbstractService;
import com.ruoyi.common.utils.StringUtil;
import com.ruoyi.system.domain.SysCode;
import com.ruoyi.system.mapper.SysCodeMapper;
import com.ruoyi.system.service.ISysCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class SysCodeServiceImpl extends AbstractService<SysCodeMapper, SysCode> implements ISysCodeService {

    @Autowired
    private SysCodeMapper codeMapper;

    @Override
    public String genCode(String prefix, int length) {
        try {
            String maxCode = this.codeMapper.getMaxCode(prefix);

            String newCode = StringUtil.genCode(maxCode, prefix, length);

            SysCode code = new SysCode();
            code.setCode(newCode);
            code.setCreateDate(new Date());
            this.codeMapper.insert(code);

            return newCode;
        } catch (Exception e) {
            return genCode(prefix, length);
        }
    }

    @Override
    public List<String> genCodeList(String prefix, int length, int size) {
        try {
            if (size == 0) {
                return null;
            }
            String maxCode = this.codeMapper.getMaxCode(prefix);

            List<String> codeStrList = new ArrayList<>();
            List<SysCode> codeList = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                maxCode = StringUtil.genCode(maxCode, prefix, length);
                codeStrList.add(maxCode);

                SysCode code = new SysCode();
                code.setCode(maxCode);
                code.setCreateDate(new Date());
                codeList.add(code);
            }

            this.saveBatch(codeList);

            return codeStrList;
        } catch (Exception e) {
            return genCodeList(prefix, length, size);
        }
    }
}
