package com.ruoyi.system.service;


import com.ruoyi.common.service.BaseService;
import com.ruoyi.system.domain.SysCode;

import java.util.List;

public interface ISysCodeService extends BaseService<SysCode>
{

    // 生成编码前缀
    String genCode(String prefix, int length);

    /**
     * 生成多个code
     *
     * @param prefix
     * @param length
     * @param size
     * @return
     */
    List<String> genCodeList(String prefix, int length, int size);
}
