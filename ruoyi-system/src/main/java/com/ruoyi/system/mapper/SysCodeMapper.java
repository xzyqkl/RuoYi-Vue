package com.ruoyi.system.mapper;

import com.ruoyi.common.mapper.CommonMapper;
import com.ruoyi.system.domain.SysCode;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysCodeMapper extends CommonMapper<SysCode>
{
    /**
     * 获取最大的编码
     *
     * @param prefix
     * @return
     */
    String getMaxCode(String prefix);
}