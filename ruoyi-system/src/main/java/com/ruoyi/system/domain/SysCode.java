package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName(value = "sys_code")
public class SysCode
{
    /**
     * 编码
     */
    private String code;

    /**
     * 创建时间
     */
    private Date createDate;
}