
<template>
  <el-dialog
          :title="title"
          :close-on-click-modal="false"
          :visible.sync="visible">
    <el-form size="small" :model="inputForm" ref="inputForm" :class="method==='view'?'readonly':''"  :disabled="method==='view'" @keyup.enter.native="doSubmit()"
             label-width="120px">
      <el-row  :gutter="15">
          <#list table.columnList as c>
          <#if c.isForm?? && c.isForm == "1" && (c.isNotBaseField || c.simpleJavaField == 'remarks')>
          <#if c.showType == "wangeditor" || c.showType == "quilleditor" || c.showType == "fileselect" || c.showType == "imageselect">
        <el-col :span="24">
            <#else>
          <el-col :span="12">
              </#if>
            <el-form-item label="${c.comments}" prop="${c.javaFieldId}"
                          :rules="[
                <#if c.isNull != "1">
                  {required: true, message:'${c.comments}不能为空', trigger:'blur'}<#if c.validateType?? && c.validateType != ""><#if c.validateType != "string" ||  c.minLength?? && c.minLength != "">,</#if></#if>
                </#if>
                <#if c.minLength?? && c.minLength != "">
                  {min: ${c.minLength}, message: '最小长度是${c.minLength}个字符', trigger: 'blur'}<#if c.validateType?? && c.validateType != ""><#if c.validateType != "string">,</#if></#if>
                </#if>
                <#if c.validateType?? && c.validateType != "">
                <#if c.validateType != "string">
                  {validator: validator.${c.validateType}, trigger:'blur'}
                 </#if>
                 </#if>
                 ]">
                <#if c.showType == "input">
                  <el-input v-model="inputForm.${c.javaFieldId}" placeholder="请填写${c.comments}" <#if c.maxLength?? && c.maxLength != ""> maxlength="${c.maxLength}"</#if> <#if  c.minLength?? && c.minLength != ""> minlength="${c.minLength}"</#if> <#if c.maxValue?? && c.maxValue != ""> max="${c.maxValue}"</#if> <#if c.minValue?? && c.minValue != ""> min="${c.minValue}"</#if> ></el-input>
                <#elseif c.showType == "textarea">
                  <el-input type="textarea" v-model="inputForm.${c.javaFieldId}" placeholder="请填写${c.comments}" <#if c.maxLength?? && c.maxLength != ""> maxlength="${c.maxLength}"</#if> <#if  c.minLength?? && c.minLength != ""> minlength="${c.minLength}"</#if> <#if c.maxValue?? && c.maxValue != ""> max="${c.maxValue}"</#if> <#if c.minValue?? && c.minValue != ""> min="${c.minValue}"</#if> ></el-input>
                <#elseif c.showType == "select">
                  <el-select v-model="inputForm.${c.javaFieldId}" placeholder="请选择"  style="width: 100%;">
                    <el-option
                            v-for="item in $dictUtils.getDictList('${c.dictType}')"
                            :key="item.value"
                            :label="item.label"
                            :value="item.value">
                    </el-option>
                  </el-select>
                <#elseif c.showType == "checkbox">
                  <el-checkbox-group
                          v-model="${c.javaFieldId}List">
                    <el-checkbox v-for="${c.javaFieldId} in $dictUtils.getDictList('${c.dictType}')" :label="${c.javaFieldId}.value" :key="${c.javaFieldId}.value">{{${c.javaFieldId}.label}}</el-checkbox>
                  </el-checkbox-group>
                <#elseif c.showType == "radiobox">
                  <el-radio-group v-model="inputForm.${c.javaFieldId}">
                    <el-radio v-for="item in $dictUtils.getDictList('${c.dictType}')" :label="item.value" :key="item.value">{{item.label}}</el-radio>
                  </el-radio-group>
                <#elseif c.showType == "dateselect">
                  <el-date-picker
                          v-model="inputForm.${c.javaFieldId}"
                          type="datetime"
                          style="width: 100%;"
                          value-format="yyyy-MM-dd HH:mm:ss"
                          placeholder="选择日期时间">
                  </el-date-picker>
                <#elseif c.showType == "userselect">
                  <user-select :limit='1' :value="inputForm.${c.javaFieldId}" @getValue='(value, label) => {inputForm.${c.javaFieldId}=value, inputForm.${c.javaFieldName}=label}'></user-select>
                <#elseif c.showType == "officeselect">
                  <SelectTree
                          ref="${c.simpleJavaField}"
                          :props="{
                          value: 'id',             // ID字段名
                          label: 'name',         // 显示名称
                          children: 'children'    // 子级字段名
                        }"
                          v-if="visible"
                          url="/sys/office/treeData?type=2"
                          :value="inputForm.${c.javaFieldId}"
                          :clearable="true"
                          :accordion="true"
                          @getValue="(value, label) => {inputForm.${c.javaFieldId}=value, inputForm.${c.javaFieldName}=label}"/>
                <#elseif c.showType == "areaselect">
                  <SelectTree
                          ref="${c.simpleJavaField}"
                          :props="{
                          value: 'id',             // ID字段名
                          label: 'name',         // 显示名称
                          children: 'children'    // 子级字段名
                        }"

                          url="/sys/area/treeData"
                          :value="inputForm.${c.javaFieldId}"
                          :clearable="true"
                          :accordion="true"
                          @getValue="(value, label) => {inputForm.${c.javaFieldId}=value, inputForm.${c.javaFieldName}=label}"/>
                <#elseif c.showType == "cityselect">
                  <CityPicker
                          style="width:100%"
                          :value="inputForm.${c.javaFieldId}"
                          :clearable="true"
                          :accordion="true"
                          @getValue="(value) => {inputForm.${c.javaFieldId}=value}"/>
                <#elseif c.showType == "fileselect">
                  <el-upload ref="${c.javaFieldId}"
                             v-if="visible"
                             :action="`${"$"}{this.${"$"}http.BASE_URL}/sys/file/webupload/upload?uploadPath=/${moduleName}<#if subModuleName != "">/${subModuleName}</#if>/${className}`"
                             :headers="{token: ${"$"}cookie.get('token')}"
                             :on-preview="(file, fileList) => {$window.location.href = (file.response && file.response.url) || file.url}"
                             :on-success="(response, file, fileList) => {
                       inputForm.${c.javaFieldId} = fileList.map(item => (item.response && item.response.url) || item.url).join('|')
                    }"
                             :on-remove="(file, fileList) => {
                      ${"$"}http.post(`/sys/file/webupload/deleteByUrl?url=${"$"}{(file.response && file.response.url) || file.url}`).then(({data}) => {
                        ${"$"}message.success(data.msg)
                      })
                      inputForm.${c.javaFieldId} = fileList.map(item => item.url).join('|')
                    }"
                             :before-remove="(file, fileList) => {
                      return ${"$"}confirm(`确定移除 ${"$"}{file.name}？`)
                    }"
                             multiple
                             :limit="5"
                             :on-exceed="(files, fileList) =>{
                      $message.warning(`当前限制选择 5 个文件，本次选择了 ${"$"}{files.length} 个文件，共选择了 ${"$"}{files.length + fileList.length} 个文件`)
                    }"
                             :file-list="${c.javaFieldId}Arra">
                    <el-button size="small" type="primary">点击上传</el-button>
                    <div slot="tip" class="el-upload__tip">添加相关附件</div>
                  </el-upload>
                <#elseif c.showType == "imageselect">
                  <el-upload ref="${c.javaFieldId}"
                             v-if="visible"
                             list-type="picture-card"
                             :action="`${"$"}{this.${"$"}http.BASE_URL}/sys/file/webupload/upload?uploadPath=/${moduleName}<#if subModuleName != "">/${subModuleName}</#if>/${className}`"
                             :headers="{token: ${"$"}cookie.get('token')}"
                             :on-preview="(file, fileList) => {
                        $alert(`<img style='width:100%' src=' ${"$"}{(file.response && file.response.url) || file.url}'/>`,  {
                          dangerouslyUseHTMLString: true,
                          showConfirmButton: false,
                          closeOnClickModal: true,
                          customClass: 'showPic'
                        });
                    }"
                             :on-success="(response, file, fileList) => {
                       inputForm.${c.javaFieldId} = fileList.map(item => (item.response && item.response.url) || item.url).join('|')
                    }"
                             :on-remove="(file, fileList) => {
                      ${"$"}http.post(`/sys/file/webupload/deleteByUrl?url=${"$"}{(file.response && file.response.url) || file.url}`).then(({data}) => {
                        ${"$"}message.success(data.msg)
                      })
                      inputForm.${c.javaFieldId} = fileList.map(item => item.url).join('|')
                    }"
                             :before-remove="(file, fileList) => {
                      return ${"$"}confirm(`确定移除 ${"$"}{file.name}？`)
                    }"
                             multiple
                             :limit="5"
                             :on-exceed="(files, fileList) =>{
                      $message.warning(`当前限制选择 5 个文件，本次选择了 ${"$"}{files.length} 个文件，共选择了 ${"$"}{files.length + fileList.length} 个文件`)
                    }"
                             :file-list="${c.javaFieldId}Arra">
                    <i class="el-icon-plus"></i>
                  </el-upload>
                <#elseif c.showType == "treeselect">
                  <SelectTree
                          ref="${c.simpleJavaField}"
                          :props="{
                          value: 'id',             // ID字段名
                          label: 'name',         // 显示名称
                          children: 'children'    // 子级字段名
                        }"
                          url="${c.dataUrl}/treeData"
                          :value="inputForm.${c.javaFieldId}"
                          :clearable="true"
                          :accordion="true"
                          @getValue="(value) => {inputForm.${c.javaFieldId}=value}"/>
                <#elseif c.showType == "gridselect">
                  <GridSelect
                          title="选择${c.comments}"
                          labelName = '${c.javaFieldName?split(".")[1]}'
                          labelValue = '${c.javaFieldId?split(".")[1]}'
                          :value = "inputForm.${c.javaFieldId}"
                          :limit="1"
                          @getValue='(value) => {inputForm.${c.javaFieldId}=value}'
                          :columns="[
    <#list c.fieldLabels?split("|") as f>
            {
              prop: '${c.fieldKeys?split("|")[f_index]}',
              label: '${c.fieldLabels?split("|")[f_index]}'
            }<#if f_has_next>,</#if>
    </#list>
            ]"
                          :searchs="[
   <#list c.searchLabel?split("|") as f>
            {
              prop: '${c.searchKey?split("|")[f_index]}',
              label: '${c.searchLabel?split("|")[f_index]}'
            }<#if f_has_next>,</#if>
    </#list>
            ]"
                          dataListUrl="${c.dataUrl}/list"
                          entityBeanName="${c.dataUrl?split("/")[c.dataUrl?split("/")?size-1]}"
                          queryEntityUrl="${c.dataUrl}/queryById">
                  </GridSelect>
                <#elseif c.showType == "wangeditor">
                  <WangEditor ref="${c.javaFieldId}Editor" v-model="inputForm.${c.javaFieldId}"/>
                <#elseif c.showType == "quilleditor">
                  <vue-editor useCustomImageHandler @image-added="$utils.handleImageAdded" v-model="inputForm.${c.javaFieldId}"/>
                </#if>
            </el-form-item>
          </el-col>
            </#if>
            </#list>
      </el-row>
    </el-form>
    <span slot="footer" class="dialog-footer">
      <el-button size="small" v-if="method === 'add'" type="primary" @click="continueDoSubmit()">继续添加</el-button>
      <el-button size="small" @click="visible = false">关闭</el-button>
      <el-button size="small" v-if="method !== 'view'" type="primary" @click="doSubmit()">确定</el-button>
    </span>
  </el-dialog>
</template>

<script>
    <#list table.importFormList as c>
    ${c}
    </#list>
    export default {
        data () {
            return {
                title: '',
                method: '',
                visible: false,
                oldInputForm: '',
                <#list table.columnList as c>
                <#if c.isForm?? && c.isForm == "1" && (c.isNotBaseField || c.simpleJavaField == 'remarks')>
                <#if c.showType == "fileselect" || c.showType == "imageselect">
                ${c.javaFieldId}Arra: [],
                </#if>
                </#if>
                </#list>
                inputForm: {
                    id: ''<#if (table.formList?size>0)>,</#if>
            <#list table.formList as c>
            <#if c.javaFieldId?contains(".")>
            ${c.javaFieldId?split(".")[0]}: {
                ${c.javaFieldId?split(".")[1]}: ''
            }<#if c_has_next>,</#if>
            <#else>
            ${c.javaFieldId}: ''<#if c_has_next>,</#if>
            </#if>
            </#list>
        }
        }
        },
        components: {
        <#list table.formComponents as c>
        ${c}<#if c_has_next>,</#if>
        </#list>
    },
    <#assign cks=0/>
    <#list table.formList as c>
    <#if c.showType == "checkbox">
    <#assign cks= cks + 1/>
    </#if>
    </#list>
    <#if cks gt 0>
    computed: {
        <#assign ckIndex=0/>
        <#list table.formList as c>
        <#if c.showType == "checkbox">
        <#assign ckIndex= ckIndex + 1/>
        ${c.javaFieldId}List: {
            get: function () {
                return this.inputForm.${c.javaFieldId} !== '' ? this.inputForm.${c.javaFieldId}.split(',') : []
            },
            set: function (val) {
                this.inputForm.${c.javaFieldId} = val.join(',')
            }
        }<#if ckIndex lt cks >,</#if>
        </#if>
        </#list>
    },
    </#if>
    methods: {
        init (method, obj) {
            this.method = method
            if (method === 'add') {
                this.title = '添加${functionNameSimple}
            } else if (method === 'edit') {
                this.title = '修改${functionNameSimple}'
            } else if (method === 'view') {
                this.title = '查看${functionNameSimple}'
            }
            <#list table.columnList as c>
            <#if c.isForm?? && c.isForm == "1" && (c.isNotBaseField || c.simpleJavaField == 'remarks')>
            <#if c.showType == "imageselect" || c.showType == "fileselect">
            this.${c.javaFieldId}Arra = []
            </#if>
            </#if>
            </#list>
            this.visible = true
            this.$nextTick(() => {
                this.$refs.inputForm.resetFields()
                this.inputForm.id = ''
                this.oldInputForm = ''
                <#list table.columnList as c>
                <#if c.isForm?? && c.isForm == "1" && (c.isNotBaseField || c.simpleJavaField == 'remarks')>
                <#if c.showType == "wangeditor">
                this.$refs.${c.javaFieldId}Editor.init('')
                </#if>
                </#if>
                </#list>
                if (method === 'edit' || method === 'view') { // 修改或者查看
                    this.oldInputForm = obj
                    this.inputForm = JSON.parse(JSON.stringify(obj))
                    <#list table.columnList as c>
                    <#if c.isForm?? && c.isForm == "1" && (c.isNotBaseField || c.simpleJavaField == 'remarks')>
                    <#if c.showType == "wangeditor">
                    this.$refs.${c.javaFieldId}Editor.init(this.inputForm.${c.javaFieldId})
                    </#if>
                    </#if>
                    </#list>
                    <#list table.columnList as c>
                    <#if c.isForm?? && c.isForm == "1" && (c.isNotBaseField || c.simpleJavaField == 'remarks')>
                    <#if c.showType == "fileselect" || c.showType == "imageselect">
                    this.inputForm.${c.javaFieldId}.split('|').forEach((item) => {
                        if (item.trim().length > 0) {
                            this.${c.javaFieldId}Arra.push({name: decodeURIComponent(item.substring(item.lastIndexOf('/') + 1)), url: item})
                        }
                    })
                    </#if>
                    </#if>
                    </#list>
                }
            })
        },
        // 表单提交
        doSubmit () {
            this.$refs['inputForm'].validate((valid) => {
                if (valid) {
                    this.$emit('addRow', this.oldInputForm, JSON.parse(JSON.stringify(this.inputForm)))
                    this.visible = false
                }
            })
        },
        continueDoSubmit () {
            this.$refs['inputForm'].validate((valid) => {
                if (valid) {
                    this.$emit('addRow', this.oldInputForm, JSON.parse(JSON.stringify(this.inputForm)))
                    this.$refs['inputForm'].resetFields()
                }
            })
        }
    }
    }
</script>
