  <div class="page">
      <el-form size="small" :inline="true" class="query-form" ref="searchForm" :model="searchForm" @keyup.enter.native="refreshList()" @submit.native.prevent>
            <!-- 搜索框-->
<#list table.columnList as c>
            <#if c.isQuery?? && c.isQuery == "1">
         <el-form-item prop="${c.javaFieldId}">
      <#if c.showType == "input" || c.showType == "textarea">
                <el-input size="small" v-model="searchForm.${c.javaFieldId}" placeholder="${c.comments}" clearable></el-input>
      <#elseif c.showType == "select">
                  <el-select size="small" v-model="searchForm.${c.javaFieldId}" placeholder="请选择${c.comments}"  style="width: 100%;">
                    <el-option
                      v-for="item in $dictUtils.getDictList('${c.dictType}')"
                      :key="item.value"
                      :label="item.label"
                      :value="item.value">
                    </el-option>
                  </el-select>
        <#elseif c.showType == "checkbox">
                <el-checkbox-group
                        v-model="${c.javaFieldId}List">
                       <el-checkbox v-for="${c.javaFieldId} in $dictUtils.getDictList('${c.dictType}')" :label="${c.javaFieldId}.value" :key="${c.javaFieldId}.value">{{${c.javaFieldId}.label}}</el-checkbox>
                    </el-checkbox-group>
        <#elseif c.showType == "radiobox">
                  <el-radio-group v-model="searchForm.${c.javaFieldId}">
                    <el-radio v-for="item in $dictUtils.getDictList('${c.dictType}')" :label="item.value" :key="item.id">{{item.label}}</el-radio>
                  </el-radio-group>
            <#elseif c.showType == "dateselect" && c.queryType == "between">
               <el-date-picker
                    v-model="searchForm.${c.javaFieldId}"
                    type="daterange"
                    size="small"
                    align="right"
                    value-format="yyyy-MM-dd hh:mm:ss"
                    unlink-panels
                    range-separator="至"
                    start-placeholder="开始日期"
                    end-placeholder="结束日期">
                 </el-date-picker>
        <#elseif c.showType == "dateselect">
                <el-date-picker
                  v-model="searchForm.${c.javaFieldId}"
                  type="datetime"
                  size="small" 
                  value-format="yyyy-MM-dd HH:mm:ss"
                  placeholder="请选择${c.comments}">
                </el-date-picker>
        <#elseif c.showType == "userselect">
            <user-select :limit='1' size="small" placeholder="请选择${c.comments}" :value="searchForm.${c.javaFieldId}" @getValue='(value) => {searchForm.${c.javaFieldId}=value}'></user-select>
        <#elseif c.showType == "officeselect">
            <SelectTree
                  ref="${c.javaFieldId}"
                  :props="{
                      value: 'id',             // ID字段名
                      label: 'name',         // 显示名称
                      children: 'children'    // 子级字段名
                    }"
                  size="small" 
                  placeholder="请选择${c.comments}"
                  :url="`/sys/office/treeData?type=2`"
                  :value="searchForm.${c.javaFieldId}"
                  :clearable="true"
                  :accordion="true"
                  @getValue="(value) => {searchForm.${c.javaFieldId}=value}"/>
        <#elseif c.showType == "areaselect">
                <SelectTree
                  ref="${c.javaFieldId}"
                  :props="{
                      value: 'id',             // ID字段名
                      label: 'name',         // 显示名称
                      children: 'children'    // 子级字段名
                    }"
                  size="small"
                  placeholder="请选择${c.comments}"
                  :url="/sys/area/treeData"
                  :value="searchForm.${c.javaFieldId}"
                  :clearable="true"
                  :accordion="true"
                  @getValue="(value) => {searchForm.${c.javaFieldId}=value}"/>
        <#elseif c.showType == "cityselect">
                  <CityPicker
                     style="width:100%"
                     size="small" 
                     placeholder="请选择${c.comments}"
                    :value="searchForm.${c.javaFieldId}"
                    :clearable="true"
                    :accordion="true"
                    @getValue="(value) => {searchForm.${c.javaFieldId}=value}"/>
        <#elseif c.showType == "treeselect">
            <SelectTree
                  ref="${c.javaFieldId}"
                  :props="{
                      value: 'id',             // ID字段名
                      label: 'name',         // 显示名称
                      children: 'children'    // 子级字段名
                    }"
                  size="small" 
                  placeholder="请选择${c.comments}"
                  url="${c.dataUrl}/treeData"
                  :value="searchForm.${c.javaFieldId}"
                  :clearable="true"
                  :accordion="true"
                  @getValue="(value) => {searchForm.${c.javaFieldId}=value}"/>
        <#elseif c.showType == "gridselect">
            <GridSelect
                    title="选择${c.comments}"
                    placeholder="请选择${c.comments}"
                    labelName = '${c.javaFieldName?split(".")[1]}'
                    labelValue = '${c.javaFieldId?split(".")[1]}'
                    :value = "searchForm.${c.javaFieldId}"
                    :limit="1"
                    size="small" 
                    @getValue='(value) => {searchForm.${c.javaFieldId}=value}'
                    :columns="[
            <#list c.fieldLabels?split("|") as f>
                    {
                      prop: '${c.fieldKeys?split("|")[f_index]}',
                      label: '${c.fieldLabels?split("|")[f_index]}'
                    }<#if f_has_next>,</#if>
            </#list>
                    ]"
                    :searchs="[
           <#list c.searchLabel?split("|") as f>
                    {
                      prop: '${c.searchKey?split("|")[f_index]}',
                      label: '${c.searchLabel?split("|")[f_index]}'
                    }<#if f_has_next>,</#if>
            </#list>
                    ]"
                    dataListUrl="${c.dataUrl}/list"
                    entityBeanName="${c.dataUrl?split("/")[c.dataUrl?split("/")?size-1]}"
                    queryEntityUrl="${c.dataUrl}/queryById">
                  </GridSelect>
        </#if>
         </el-form-item>
        </#if>
    </#list>
          <el-form-item>
            <el-button type="primary" @click="refreshList()" size="small">查询</el-button>
            <el-button @click="resetSearch()" size="small">重置</el-button>
          </el-form-item>
      </el-form>
        <!-- 导入导出-->
        <el-dialog  title="导入Excel" :visible.sync="isImportCollapse">
          <el-form :inline="true" v-show="isImportCollapse"  class="query-form" ref="importForm">
             <el-form-item>
              <el-button type="default" @click="downloadTpl()" size="small">下载模板</el-button>
             </el-form-item>
             <el-form-item prop="loginName">
                <el-upload
                  class="upload-demo"
                  :action="`${"$"}{this.$http.BASE_URL}/${urlPrefix}/import`"
                  :on-success="uploadSuccess"
                   :show-file-list="true">
                  <el-button size="small" type="primary">点击上传</el-button>
                  <div slot="tip" class="el-upload__tip">只允许导入“xls”或“xlsx”格式文件！</div>
                </el-upload>
            </el-form-item>
          </el-form>
      </el-dialog>
      <div class="bg-white top">
      <el-row>
        <el-button v-if="hasPermission('${permissionPrefix}:add')" type="primary" size="small" icon="el-icon-plus" @click="add()">新建</el-button>
        <el-button v-if="hasPermission('${permissionPrefix}:edit')" type="warning" size="small" icon="el-icon-edit-outline" @click="edit()"
         :disabled="dataListSelections.length != 1" plain>修改</el-button>
        <el-button v-if="hasPermission('${permissionPrefix}:del')" type="danger"   size="small" icon="el-icon-delete" @click="del()"
                  :disabled="dataListSelections.length <= 0" plain>删除
        </el-button>
        <el-button-group class="pull-right">
            <el-button v-if="hasPermission('${permissionPrefix}:import')" type="default" size="small" icon="el-icon-upload2" title="导入" @click="isImportCollapse = !isImportCollapse, isSearchCollapse=false"></el-button>
            <el-button v-if="hasPermission('${permissionPrefix}:export')" type="default" size="small" icon="el-icon-download" title="导出" @click="exportExcel()"></el-button>
            <el-button
              type="default"
              size="small"
              icon="el-icon-refresh"
              @click="refreshList">
            </el-button>
        </el-button-group>
      </el-row>
    <el-table
      :data="dataList"
      @selection-change="selectionChangeHandle"
      @sort-change="sortChangeHandle"
      v-loading="loading"
      size="small"
      height="calc(100% - 80px)"
      @expand-change="detail"
      class="table">
      <el-table-column
        type="selection"
        header-align="center"
        align="center"
        width="50">
      </el-table-column>
      <el-table-column type="expand">
      <template slot-scope="scope">
      <el-tabs>
        <#list table.childList as child>
            <el-tab-pane label="${child.comments}">
                  <el-table
                  size="small"
                  :data="scope.row.${child.className?uncap_first}List"
                  style="width: 100%">
                  <#assign childColumnCount = 0 />
                <#list child.columnList as c>
                    <#if c.isForm?? && c.isForm == "1" && (c.isNotBaseField || c.simpleJavaField == 'remarks') && c.name != c.genTable.parentTableFk>
              <#if c.isList?? && c.isList == "1">
          <#if c.showType == "userselect" || c.showType == "officeselect" || c.showType == "areaselect" || c.showType == "treeselect"|| c.showType == "gridselect">
                  <el-table-column
                    prop="${c.javaFieldName}"
                    label="${c.comments}">
                  </el-table-column>
        <#elseif c.showType == "select">
                <el-table-column
                    prop="${c.javaFieldId}"
                    label="${c.comments}">
                    <template slot-scope="scope">
                        {{ $dictUtils.getDictLabel("${c.dictType}", scope.row.${c.javaFieldId}, '-') }}
                    </template>
                  </el-table-column>
      <#elseif c.showType == "checkbox">
                <el-table-column
                    prop="${c.javaFieldId}"
                    show-overflow-tooltip
                    label="${c.comments}">
                    <template slot-scope="scope">
                        {{scope.row.${c.javaFieldId}.split(",").map( (item)=> { return $dictUtils.getDictLabel("${c.dictType}", item, '-')}).join(",")  }}
                    </template>
                  </el-table-column>
      <#elseif c.showType == "radiobox">
                <el-table-column
                    prop="${c.javaFieldId}"
                    show-overflow-tooltip
                    label="${c.comments}">
                    <template slot-scope="scope">
                        {{ $dictUtils.getDictLabel("${c.dictType}", scope.row.${c.javaFieldId}, '-') }}
                    </template>
                  </el-table-column>
        <#elseif c.showType == "fileselect">
      <el-table-column
            prop="${c.javaFieldId}"
            show-overflow-tooltip
            sortable="custom"
            label="${c.comments}">
            <template slot-scope="scope" v-if="scope.row.${c.javaFieldId}">
                <a :href="item" target="_blank" :key="index" v-for="(item, index) in scope.row.${c.javaFieldId}.split('|')">
                    {{decodeURIComponent(item.substring(item.lastIndexOf("/")+1))}}
                </a>
            </template>
      </el-table-column>
    <#elseif c.showType == "imageselect">
      <el-table-column
            prop="${c.javaFieldId}"
            show-overflow-tooltip
            sortable="custom"
            label="${c.comments}">
            <template slot-scope="scope" v-if="scope.row.${c.javaFieldId}">
              <el-image
                style="height: 50px;width:50px;margin-right:10px;"
                :src="src" v-for="(src, index) in scope.row.${c.javaFieldId}.split('|')" :key="index"
                :preview-src-list="scope.row.${c.javaFieldId}.split('|')">
              </el-image>
            </template>
          </el-table-column>
         <#elseif c.showType == "wangeditor" || c.showType == "quilleditor">
                  <el-table-column
                    prop="${c.javaFieldId}"
                    show-overflow-tooltip
                    label="${c.comments}">
                    <template slot-scope="scope">
                      <p v-html="$utils.unescapeHTML(scope.row.${c.javaFieldId})"></p>
                    </template>
                  </el-table-column>
      <#else>
                <el-table-column
                    prop="${c.javaFieldId}"
                    show-overflow-tooltip
                    label="${c.comments}">
                  </el-table-column>
          </#if>
                    </#if>
                    </#if>
                </#list>
                </el-table>
              </el-tab-pane>
         </#list>
            </el-tabs>
      </template>
      </el-table-column>
      <#assign firstListField = true>
      <#list table.columnList as c>
        <#if c.isList?? && c.isList == "1">
          <#if c.showType == "userselect" || c.showType == "officeselect" || c.showType == "areaselect" || c.showType == "treeselect"|| c.showType == "gridselect">
      <el-table-column
        prop="${c.javaFieldName}"
        show-overflow-tooltip
        sortable="custom"
        label="${c.comments}">
      <#if firstListField>
            <template slot-scope="scope">
              <el-link  type="primary" :underline="false" v-if="hasPermission('${permissionPrefix}:edit')" @click="edit(scope.row.id)">{{scope.row.${c.javaFieldName}}}</el-link>
              <el-link  type="primary" :underline="false" v-else-if="hasPermission('${permissionPrefix}:view')"  @click="view(scope.row.id)">{{scope.row.${c.javaFieldName}}}</el-link>
              <span v-else>{{scope.row.${c.javaFieldName}}}</span>
            </template>
        </#if>
      </el-table-column>
        <#elseif c.showType == "select">
    <el-table-column
        prop="${c.javaFieldId}"
        show-overflow-tooltip
        sortable="custom"
        label="${c.comments}">
        <template slot-scope="scope">
        <#if firstListField>
              <el-link  type="primary" :underline="false" v-if="hasPermission('${permissionPrefix}:edit')" @click="edit(scope.row.id)"> {{ $dictUtils.getDictLabel("${c.dictType}", scope.row.${c.javaFieldId}, '-') }}</el-link>
              <el-link  type="primary" :underline="false" v-else-if="hasPermission('${permissionPrefix}:view')"  @click="view(scope.row.id)"> {{ $dictUtils.getDictLabel("${c.dictType}", scope.row.${c.javaFieldId}, '-') }}</el-link>
              <span v-else>{{ $dictUtils.getDictLabel("${c.dictType}", scope.row.${c.javaFieldId}, '-') }}</span>
        <#else>
              {{ $dictUtils.getDictLabel("${c.dictType}", scope.row.${c.javaFieldId}, '-') }}
        </#if>
        </template>
      </el-table-column>
      <#elseif c.showType == "checkbox">
    <el-table-column
        prop="${c.javaFieldId}"
        show-overflow-tooltip
        sortable="custom"
        label="${c.comments}">
        <template slot-scope="scope">
       <#if firstListField>
              <el-link  type="primary" :underline="false" v-if="hasPermission('${permissionPrefix}:edit')" @click="edit(scope.row.id)">{{scope.row.${c.javaFieldId}.split(",").map( (item)=> { return $dictUtils.getDictLabel("${c.dictType}", item, '-')}).join(",")  }}</el-link>
              <el-link  type="primary" :underline="false" v-else-if="hasPermission('${permissionPrefix}:view')"  @click="view(scope.row.id)">{{scope.row.${c.javaFieldId}.split(",").map( (item)=> { return $dictUtils.getDictLabel("${c.dictType}", item, '-')}).join(",")  }}</el-link>
              <span v-else>{{scope.row.${c.javaFieldId}.split(",").map( (item)=> { return $dictUtils.getDictLabel("${c.dictType}", item, '-')}).join(",")  }}</span>
        <#else>
              {{scope.row.${c.javaFieldId}.split(",").map( (item)=> { return $dictUtils.getDictLabel("${c.dictType}", item, '-')}).join(",")  }}
        </#if>
        </template>
      </el-table-column>
      <#elseif c.showType == "radiobox">
    <el-table-column
        prop="${c.javaFieldId}"
        show-overflow-tooltip
        sortable="custom"
        label="${c.comments}">
        <template slot-scope="scope">
       <#if firstListField>
              <el-link  type="primary" :underline="false" v-if="hasPermission('${permissionPrefix}:edit')" @click="edit(scope.row.id)">{{ $dictUtils.getDictLabel("${c.dictType}", scope.row.${c.javaFieldId}, '-') }}</el-link>
              <el-link  type="primary" :underline="false" v-else-if="hasPermission('${permissionPrefix}:view')"  @click="view(scope.row.id)">{{ $dictUtils.getDictLabel("${c.dictType}", scope.row.${c.javaFieldId}, '-') }}</el-link>
              <span v-else> {{ $dictUtils.getDictLabel("${c.dictType}", scope.row.${c.javaFieldId}, '-') }}</span>
        <#else>
             {{ $dictUtils.getDictLabel("${c.dictType}", scope.row.${c.javaFieldId}, '-') }}
        </#if>
        </template>
      </el-table-column>
        <#elseif c.showType == "fileselect">
    <el-table-column
        prop="${c.javaFieldId}"
        show-overflow-tooltip
        sortable="custom"
        label="${c.comments}">
        <template slot-scope="scope" v-if="scope.row.${c.javaFieldId}">
            <a :href="item" target="_blank" :key="index" v-for="(item, index) in scope.row.${c.javaFieldId}.split('|')">
                {{decodeURIComponent(item.substring(item.lastIndexOf("/")+1))}}
            </a>
        </template>
      </el-table-column>
              <#elseif c.showType == "imageselect">
    <el-table-column
        prop="${c.javaFieldId}"
        show-overflow-tooltip
        sortable="custom"
        label="${c.comments}">
        <template slot-scope="scope" v-if="scope.row.${c.javaFieldId}">
          <el-image
            style="height: 50px;width:50px;margin-right:10px;"
            :src="src" v-for="(src, index) in scope.row.${c.javaFieldId}.split('|')" :key="index"
            :preview-src-list="scope.row.${c.javaFieldId}.split('|')">
          </el-image>
        </template>
      </el-table-column>
        <#elseif c.showType == "wangeditor" || c.showType == "quilleditor">
      <el-table-column
        prop="${c.javaFieldId}"
        show-overflow-tooltip
        sortable="custom"
        label="${c.comments}">
        <template slot-scope="scope">
            <p v-html="$utils.unescapeHTML(scope.row.${c.javaFieldId})"></p>
        </template>
      </el-table-column>
      <#else>
    <el-table-column
        prop="${c.javaFieldId}"
        show-overflow-tooltip
        sortable="custom"
        label="${c.comments}">
        <#if firstListField>
            <template slot-scope="scope">
              <el-link  type="primary" :underline="false" v-if="hasPermission('${permissionPrefix}:edit')" @click="edit(scope.row.id)">{{scope.row.${c.javaFieldId}}}</el-link>
              <el-link  type="primary" :underline="false" v-else-if="hasPermission('${permissionPrefix}:view')"  @click="view(scope.row.id)">{{scope.row.${c.javaFieldId}}}</el-link>
              <span v-else>{{scope.row.${c.javaFieldId}}}</span>
            </template>
        </#if>
      </el-table-column>
          </#if>
        <#assign firstListField = false>
      </#if>
      </#list>
      <el-table-column
        header-align="center"
        align="center"
        fixed="right"
        :key="Math.random()"
        width="200"
        label="操作">
        <template  slot-scope="scope">
          <el-button v-if="hasPermission('${permissionPrefix}:view')" type="text" icon="el-icon-view" size="small" @click="view(scope.row.id)">查看</el-button>
          <el-button v-if="hasPermission('${permissionPrefix}:edit')" type="text" icon="el-icon-edit" size="small" @click="edit(scope.row.id)">修改</el-button>
          <el-button v-if="hasPermission('${permissionPrefix}:del')" type="text"  icon="el-icon-delete" size="small" @click="del(scope.row.id)">删除</el-button>
        </template>
      </el-table-column>
    </el-table>
    <el-pagination
      @size-change="sizeChangeHandle"
      @current-change="currentChangeHandle"
      :current-page="pageNo"
      :page-sizes="[10, 20, 50, 100]"
      :page-size="pageSize"
      :total="total"
      background
      layout="total, sizes, prev, pager, next, jumper">
    </el-pagination>
    </div>
        <!-- 弹窗, 新增 / 修改 -->
    <${ClassName}Form  ref="${className}Form" @refreshDataList="refreshList"></${ClassName}Form>
  </div>
</template>

<script>
  import ${ClassName}Form from './${ClassName}Form'
<#list table.importQueryList as c>
  ${c}
</#list>
  export default {
    data () {
      return {
        searchForm: {
        <#list table.queryList as c>
          <#if c.javaFieldId?contains(".")>
          ${c.javaFieldId?split(".")[0]}: {
            ${c.javaFieldId?split(".")[1]}: ''
          }<#if c_has_next>,</#if>
          <#else>
          ${c.javaFieldId}: ''<#if c_has_next>,</#if>
          </#if>
    </#list>
        },
        dataList: [],
        pageNo: 1,
        pageSize: 10,
        total: 0,
        orderBy: '',
        dataListSelections: [],
        isImportCollapse: false,
        loading: false
      }
    },
    components: {
      <#list table.queryComponents as c>
      ${c},
    </#list>
      ${ClassName}Form
    },
    activated () {
      this.refreshList()
    },
    <#assign cks=0/>
    <#list table.columnList as c>
    <#if c.isQuery?? && c.isQuery == "1">
     <#if c.showType == "checkbox">
     <#assign cks= cks + 1/>
    </#if>
    </#if>
    </#list>
    <#if cks gt 0>
    computed: {
    <#assign ckIndex=0/>
    <#list table.columnList as c>
    <#if c.isQuery?? && c.isQuery == "1">
     <#if c.showType == "checkbox">
     <#assign ckIndex= ckIndex + 1/>
      ${c.javaFieldId}List: {
        get: function () {
          return this.searchForm.${c.javaFieldId} !== '' ? this.searchForm.${c.javaFieldId}.split(',') : []
        },
        set: function (val) {
          this.searchForm.${c.javaFieldId} = val.join(',')
        }
      }<#if ckIndex lt cks >,</#if>
    </#if>
    </#if>
    </#list>
    },
    </#if>
    methods: {
      // 获取数据列表
      refreshList () {
        this.loading = true
        this.$http({
          url: '/${urlPrefix}/list',
          method: 'get',
          params: {
            'pageNo': this.pageNo,
            'pageSize': this.pageSize,
            'orderBy': this.orderBy,
            <#if (table.queryBetwinDate?size > 0)>
            <#list table.queryBetwinDate as c>
            begin${c.javaFieldId?cap_first}: this.searchForm.${c.javaFieldId}[0],
            end${c.javaFieldId?cap_first}: this.searchForm.${c.javaFieldId}[1],
            </#list>
            ...this.lodash.omit(this.searchForm, <#list table.queryBetwinDate as c>'${c.javaFieldId}'<#if c_has_next>, </#if></#list>)
            <#else>
            ...this.searchForm
            </#if>
          }
        }).then(({data}) => {
          if (data && data.success) {
            this.dataList = data.page.list
            this.total = data.page.count
            this.loading = false
          }
        })
      },
      // 每页数
      sizeChangeHandle (val) {
        this.pageSize = val
        this.pageNo = 1
        this.refreshList()
      },
      // 当前页
      currentChangeHandle (val) {
        this.pageNo = val
        this.refreshList()
      },
      // 多选
      selectionChangeHandle (val) {
        this.dataListSelections = val
      },

    // 排序
      sortChangeHandle (obj) {
        if (obj.order === 'ascending') {
          this.orderBy = obj.prop + ' asc'
        } else if (obj.order === 'descending') {
          this.orderBy = obj.prop + ' desc'
        } else {
          this.orderBy = ''
        }
        this.refreshList()
      },
      // 新增
      add () {
        this.$refs.${className}Form.init('add', '')
      },
      // 修改
      edit (id) {
        id = id || this.dataListSelections.map(item => {
          return item.id
        })[0]
        this.$refs.${className}Form.init('edit', id)
      },
      // 查看
      view (id) {
        this.$refs.${className}Form.init('view', id)
      },
      // 删除
      del (id) {
        let ids = id || this.dataListSelections.map(item => {
          return item.id
        }).join(',')
        this.$confirm(`确定删除所选项吗?`, '提示', {
          confirmButtonText: '确定',
          cancelButtonText: '取消',
          type: 'warning'
        }).then(() => {
          this.loading = true
          this.$http({
            url: '/${urlPrefix}/delete',
            method: 'delete',
            params: {'ids': ids}
          }).then(({data}) => {
            if (data && data.success) {
              this.$message.success(data.msg)
              this.refreshList()
            }
            this.loading = false
          })
        })
      },
     // 查看详情
      detail (row) {
        this.$http.get(`/${urlPrefix}/queryById?id=${"$"}{row.id}`).then(({data}) => {
          this.dataList.forEach((item, index) => {
            if (item.id === row.id) {
              <#list table.childList as child>
              item.${child.className?uncap_first}List = data.${className}.${child.className?uncap_first}List
              </#list>
            }
          })
        })
      },
      // 导入成功
      uploadSuccess (res, file) {
        if (res.success) {
          this.$message.success({dangerouslyUseHTMLString: true,
            message: res.msg})
        } else {
          this.$message.error(res.msg)
        }
      },
      // 下载模板
      downloadTpl () {
        this.$utils.download('/${urlPrefix}/import/template')
      },
      exportExcel () {
        let params = {
            <#if (table.queryBetwinDate?size > 0)>
            <#list table.queryBetwinDate as c>
          begin${c.javaFieldId?cap_first}: this.searchForm.${c.javaFieldId}[0],
          end${c.javaFieldId?cap_first}: this.searchForm.${c.javaFieldId}[1],
            </#list>
          ...this.lodash.omit(this.searchForm, <#list table.queryBetwinDate as c>'${c.javaFieldId}'<#if c_has_next>, </#if></#list>)
            <#else>
          ...this.searchForm
            </#if>
        }
        this.$utils.download('/${urlPrefix}/export', params)
      },
      resetSearch () {
        this.$refs.searchForm.resetFields()
        this.refreshList()
      }
    }
  }
</script>
