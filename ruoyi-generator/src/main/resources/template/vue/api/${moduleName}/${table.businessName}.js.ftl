import request from '@/utils/request'

// 查询${table.functionName}列表
export function list${table.businessName?cap_first}(query) {
  return request({
    url: '/${table.moduleName}/${table.businessName}/list',
    method: 'get',
    params: query
  })
}

// 查询${table.functionName}详细
export function get${table.businessName?cap_first}(${table.pkColumn.javaField}) {
  return request({
    url: '/${table.moduleName}/${table.businessName}/' + ${table.pkColumn.javaField},
    method: 'get'
  })
}

// 新增${table.functionName}
export function add${table.businessName?cap_first}(data) {
  return request({
    url: '/${table.moduleName}/${table.businessName}',
    method: 'post',
    data: data
  })
}

// 修改${table.functionName}
export function update${table.businessName?cap_first}(data) {
  return request({
    url: '/${table.moduleName}/${table.businessName}',
    method: 'put',
    data: data
  })
}

// 删除${table.functionName}
export function del${table.businessName?cap_first}(${table.pkColumn.javaField}) {
  return request({
    url: '/${table.moduleName}/${table.businessName}/' + ${table.pkColumn.javaField},
    method: 'delete'
  })
}

// 导出${table.functionName}
export function export${table.businessName?cap_first}(query) {
  return request({
    url: '/${table.moduleName}/${table.businessName}/export',
    method: 'get',
    params: query
  })
}
<#if table.interface>

// 获取${table.functionName}接口
export function list${table.businessName?cap_first}Data(query) {
  return request({
    url: '/${table.moduleName}/${table.businessName}/data',
    method: 'get',
    params: query
  })
}
</#if>