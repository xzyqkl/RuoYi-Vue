<#assign dictType=column.dictType!''>
<#assign parentheseIndex=column.columnComment?index_of("（")>
<#if parentheseIndex != -1>
<#assign comment=column.columnComment?substring(0, parentheseIndex)>
<#else>
<#assign comment=column.columnComment>
</#if>
<#if column.htmlType == "input">
      <el-form-item label="<#if prefixName?? && prefixName?length gt 0>${prefixName}</#if>${comment}" prop="<#if prefix?? && prefix?length gt 0>${prefix}.</#if>${column.javaField}">
        <el-input
          v-model="queryParams.<#if prefix?? && prefix?length gt 0>${prefix}.</#if>${column.javaField}"
          placeholder="请输入<#if prefixName?? && prefixName?length gt 0>${prefixName}</#if>${comment}"
          clearable
          @keyup.enter.native="handleQuery"
        />
      </el-form-item>
<#elseif column.htmlType == "select" || column.htmlType == "radio">
    <#if dictType??>
      <el-form-item label="<#if prefixName?? && prefixName?length gt 0>${prefixName}</#if>${comment}" prop="<#if prefix?? && prefix?length gt 0>${prefix}.</#if>${column.javaField}">
        <el-select v-model="queryParams.<#if prefix?? && prefix?length gt 0>${prefix}.</#if>${column.javaField}" placeholder="请选择<#if prefixName?? && prefixName?length gt 0>${prefixName}</#if>${comment}"<#if column.multi> multiple</#if> clearable>
          <el-option
            v-for="dict in dict.type.${column.dictType}"
            :key="dict.value"
            :label="dict.label"
            :value="dict.value"
          />
        </el-select>
      </el-form-item>
    <#else>
      <el-form-item label="<#if prefixName?? && prefixName?length gt 0>${prefixName}</#if>${comment}" prop="<#if prefix?? && prefix?length gt 0>${prefix}.</#if>${column.javaField}">
        <el-select v-model="queryParams.<#if prefix?? && prefix?length gt 0>${prefix}.</#if>${column.javaField}" placeholder="请选择<#if prefixName?? && prefixName?length gt 0>${prefixName}</#if>${comment}" clearable>
          <el-option label="请选择字典生成" value=""/>
        </el-select>
      </el-form-item>
    </#if>
<#elseif column.htmlType == "date" || column.htmlType == "datetime">
  <#if column.queryType == "BETWEEN">
      <el-form-item label="<#if prefixName?? && prefixName?length gt 0>${prefixName}</#if>${comment}">
        <el-date-picker
          v-model="daterange<#if prefix?? && prefix?length gt 0>${prefix?cap_first}</#if>${column.javaField?cap_first}"
          style="width: 240px"
          value-format="yyyy-MM-dd"
          type="daterange"
          range-separator="-"
          start-placeholder="开始日期"
          end-placeholder="结束日期"
        ></el-date-picker>
      </el-form-item>
    <#else>
      <el-form-item label="<#if prefixName?? && prefixName?length gt 0>${prefixName}</#if>${comment}" prop="<#if prefix?? && prefix?length gt 0>${prefix}.</#if>${column.javaField}">
        <el-date-picker clearable
          v-model="queryParams.<#if prefix?? && prefix?length gt 0>${prefix}.</#if>${column.javaField}"
          type="date"
          value-format="yyyy-MM-dd"
          placeholder="选择<#if prefixName?? && prefixName?length gt 0>${prefixName}</#if>${comment}">
        </el-date-picker>
      </el-form-item>
  </#if>
</#if>