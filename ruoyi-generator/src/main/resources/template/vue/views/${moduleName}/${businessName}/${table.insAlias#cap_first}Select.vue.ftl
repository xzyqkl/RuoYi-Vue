<#-- 选择框 -->
<template>
  <div>
<#-- 搜索条件 -->
    <el-form size="mini" :model="queryParams" ref="queryForm" :inline="true" v-show="showSearch" label-width="90px">
    <#list table.columns as column>
      <#assign prefix = ''>
      <#assign prefixName = ''>
      <#if column.assocTable??>
        <#assign prefix = column.assocTable.insAlias?uncap_first>
        <#assign prefixName = column.assocTable.functionName>
        <#list column.assocTable.columns as column>
          <#if column.queryType??>
            <#include "/vue/views/${r'$'}{moduleName}/${r'$'}{businessName}/fragment/search.vue.ftl">
          </#if>
        </#list>
      <#else>
        <#if column.queryType?? && !column.pk>
          <#include "/vue/views/${r'$'}{moduleName}/${r'$'}{businessName}/fragment/search.vue.ftl">
        </#if>
      </#if>
    </#list>
      <el-form-item>
        <el-button type="primary" icon="el-icon-search" size="mini" @click="handleQuery">搜索</el-button>
        <el-button icon="el-icon-refresh" size="mini" @click="resetQuery">重置</el-button>
      </el-form-item>
    </el-form>

    <el-table v-loading="loading" :data="${table.businessName}List">
      <el-table-column label="操作" align="center" class-name="small-padding fixed-width">
        <template slot-scope="scope">
          <el-button
            size="mini"
            type="text"
            icon="el-icon-edit"
            @click="handleSelect(scope.row)"
          >选择
          </el-button>
        </template>
      </el-table-column>
<#-- 表本身属性 -->
<#assign prefix = ''>
<#assign prefixName = ''>
<#list table.columns as column>
  <#if column.pk || !column.list>
      <#continue>
  </#if>
<@format blank=6><#include "/vue/views/${r'$'}{moduleName}/${r'$'}{businessName}/fragment/table.vue.ftl"></@format>
</#list>
    </el-table>

    <pagination
      v-show="total>0"
      :total="total"
      :page.sync="queryParams.pageNum"
      :limit.sync="queryParams.pageSize"
      @pagination="getList"
    />
  </div>
</template>

<script>
import {
  list${table.businessName?cap_first}<#if table.tableName != table.parentTable.tableName>Data</#if>
} from '@/api/${table.moduleName}/${table.businessName}'

export default {
  name: '${table.businessName?cap_first}',
  <#include "/vue/views/${r'$'}{moduleName}/${r'$'}{businessName}/dicts.vue.ftl">
  components: {},
  <#if table.appearCount gt 1>
  props: {
    selectFlag: {
      type: String,
      default: ''
    }
  },
  </#if>
<#-- 处理，用来做提示信息的属性。主键下边input类型的那个字段用来做提示-->
  data() {
    return {
      // 遮罩层
      loading: true,
      // 选中数组
      data: [],
      // 非单个禁用
      single: true,
      // 非多个禁用
      multiple: true,
      // 显示搜索条件
      showSearch: true,
      // 总条数
      total: 0,
      // ${table.functionName}表格数据
      ${table.businessName}List: [],
<#if table.subTables?? && table.subTables?size gt 0>
  <#list table.subTables as subTable>
      // ${subTable.functionName}表格数据
      ${subTable.insAlias?uncap_first}List: [],
  </#list>
</#if>
<#-- 特殊检索条件处理 -->
<#include "/vue/views/${r'$'}{moduleName}/${r'$'}{businessName}/params.vue.ftl">
      // 查询参数
      queryParams: {
        pageNum: 1,
        pageSize: 10,
<#-- 主表属性查询 -->
<#list table.columns as column>
<#if column.queryType??>
        ${column.javaField}: null,
</#if>
</#list>
      }
    }
  },
  created() {
    this.getList()
  },
  methods: {
    /** 查询${table.functionName}列表 */
    getList() {
      this.loading = true
<#list table.columns as column>
<#if column.htmlType?? && column.queryType?? && column.htmlType == "datetime" && column.queryType == "BETWEEN">
      this.queryParams = this.addDateRange(this.queryParams, this.daterange${column.javaField?cap_first}, '${column.javaField?cap_first}')
</#if>
</#list>
      list${table.businessName?cap_first}<#if table.tableName != table.parentTable.tableName>Data</#if>(this.queryParams).then(response => {
        this.${table.businessName}List = response.rows
        this.total = response.total
        this.loading = false
      })
    },
    /** 搜索按钮操作 */
    handleQuery() {
      this.queryParams.pageNum = 1
      this.getList()
    },
    /** 重置按钮操作 */
    resetQuery() {
<#list table.columns as column>
<#if column.htmlType?? && column.queryType?? && column.htmlType == "datetime" && column.queryType == "BETWEEN">
      this.daterange${column.javaField?cap_first} = []
</#if>
</#list>
      this.resetForm('queryForm')
      this.handleQuery()
    },
    /** 处理选择 */
    handleSelect(row) {
      let data = null
      if (row.id) {
        data = row
      }
      if (data == null) {
        this.$message.error('请至少选择一条')
      }
      this.$emit('select', data<#if table.appearCount gt 1>, this.selectFlag</#if>)
    }
  }
}
</script>