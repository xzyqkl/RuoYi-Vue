<#-- 单表 -->
<#include "/vue/views/${r'$'}{moduleName}/${r'$'}{businessName}/fragment/commonVar.vue.ftl">
<template>
  <div class="app-container">
<#-- 搜索条件 -->
    <el-form size="mini" :model="queryParams" ref="queryForm" :inline="true" v-show="showSearch" label-width="90px">
    <#list table.columns as column>
      <#assign prefix = ''>
      <#assign prefixName = ''>
      <#if column.assocTable??>
        <#assign prefix = column.assocTable.insAlias?uncap_first>
        <#assign prefixName = column.assocTable.functionName>
        <#list column.assocTable.columns as column>
          <#if column.queryType??>
            <#include "/vue/views/${r'$'}{moduleName}/${r'$'}{businessName}/fragment/search.vue.ftl">
          </#if>
        </#list>
      <#else>
        <#if column.queryType?? && !column.pk>
          <#include "/vue/views/${r'$'}{moduleName}/${r'$'}{businessName}/fragment/search.vue.ftl">
        </#if>
      </#if>
    </#list>
      <el-form-item>
        <el-button type="primary" icon="el-icon-search" size="mini" @click="handleQuery">搜索</el-button>
        <el-button icon="el-icon-refresh" size="mini" @click="resetQuery">重置</el-button>
      </el-form-item>
    </el-form>

    <el-row :gutter="10" class="mb8">
      <el-col :span="1.5">
        <el-button
          type="primary"
          plain
          icon="el-icon-plus"
          size="mini"
          @click="handleAdd"
          v-hasPermi="['${moduleName}:${businessName}:add']"
        >新增
        </el-button>
      </el-col>
      <el-col :span="1.5">
        <el-button
          type="success"
          plain
          icon="el-icon-edit"
          size="mini"
          :disabled="single"
          @click="handleUpdate"
          v-hasPermi="['${moduleName}:${businessName}:edit']"
        >修改
        </el-button>
      </el-col>
      <el-col :span="1.5">
        <el-button
          type="danger"
          plain
          icon="el-icon-delete"
          size="mini"
          :disabled="multiple"
          @click="handleDelete"
          v-hasPermi="['${moduleName}:${businessName}:remove']"
        >删除
        </el-button>
      </el-col>
      <el-col :span="1.5">
        <el-button
          type="warning"
          plain
          icon="el-icon-download"
          size="mini"
          @click="handleExport"
          v-hasPermi="['${moduleName}:${businessName}:export']"
        >导出
        </el-button>
      </el-col>
      <right-toolbar :showSearch.sync="showSearch" @queryTable="getList"></right-toolbar>
    </el-row>

    <el-table v-loading="loading" :data="${businessName}List" @selection-change="handleSelectionChange">
      <el-table-column type="selection" width="55" align="center"/>
<#list table.columns as column>
  <#assign prefix = ''>
  <#assign prefixName = ''>
  <#if column.assocTable??>
    <#assign prefix = column.assocTable.insAlias?uncap_first>
    <#assign prefixName = column.assocTable.functionName>
    <#list column.assocTable.columns as column>
      <#if column.list>
          <@format blank=6><#include "/vue/views/${r'$'}{moduleName}/${r'$'}{businessName}/fragment/table.vue.ftl"></@format>
      </#if>
    </#list>
  <#else>
    <#if column.pk || !column.list>
      <#continue>
    </#if>
      <@format blank=6><#include "/vue/views/${r'$'}{moduleName}/${r'$'}{businessName}/fragment/table.vue.ftl"></@format>
  </#if>
</#list>
      <el-table-column label="操作" align="center" class-name="small-padding fixed-width">
        <template slot-scope="scope">
          <el-button
            size="mini"
            type="text"
            icon="el-icon-edit"
            @click="handleUpdate(scope.row)"
            v-hasPermi="['${moduleName}:${businessName}:edit']"
          >修改
          </el-button>
          <el-button
            size="mini"
            type="text"
            icon="el-icon-delete"
            @click="handleDelete(scope.row)"
            v-hasPermi="['${moduleName}:${businessName}:remove']"
          >删除
          </el-button>
        </template>
      </el-table-column>
    </el-table>

    <pagination
      v-show="total>0"
      :total="total"
      :page.sync="queryParams.pageNum"
      :limit.sync="queryParams.pageSize"
      @pagination="getList"
    />

    <!-- 添加或修改${table.functionName}对话框 -->
    <el-dialog :title="title" :visible.sync="open" width="60%" append-to-body>
      <el-form ref="form" :model="form" :rules="rules" label-width="80px">
        <el-row>
          <#include "/vue/views/${r'$'}{moduleName}/${r'$'}{businessName}/fragment/formItem.vue.ftl">
        </el-row>
      </el-form>
      <div slot="footer" class="dialog-footer">
        <el-button type="primary" @click="submitForm">确 定</el-button>
        <el-button @click="cancel">取 消</el-button>
      </div>
    </el-dialog>
    <#-- 引入选择弹出框 -->
    <#include "/vue/views/${r'$'}{moduleName}/${r'$'}{businessName}/fragment/formSelect.vue.ftl">
  </div>
</template>

<script>
import {
  list${businessName?cap_first},
  get${businessName?cap_first},
  del${businessName?cap_first},
  add${businessName?cap_first},
  update${businessName?cap_first},
  export${businessName?cap_first}
} from '@/api/${moduleName}/${businessName}'
<#-- 2. 引入通用组件（imageUpload、fileUpload等）、选择框组件等 -->
<#if importCommponents?length gt 0>
  <#list importCommponents?split(",") as ic>
    ${ic}
  </#list>
</#if>

export default {
  name: '${businessName?cap_first}',
  <#include "/vue/views/${r'$'}{moduleName}/${r'$'}{businessName}/dicts.vue.ftl">
  components: {
  <#if commponents?length gt 0>
    <#list commponents?split(",") as c>
      ${c}<#if c_has_next>,</#if>
    </#list>
  </#if>
  },
<#-- 处理，用来做提示信息的属性。主键下边input类型的那个字段用来做提示 优先使用包含name的字段 -->
<#assign selectedJavaField = ''>
<#assign findName = false>
<#list table.columns as column>
  <#if column.javaField?lower_case?index_of("name") != -1>
    <#assign findName = true>
    <#assign selectedJavaField = column.javaField>
    <#break>
  </#if>
</#list>
<#if !findName>
  <#assign findPk = false> <#-- 标识是否找到主键 -->
  <#list table.columns as column>
    <#if findPk && column.list && column.htmlType == 'input'>
      <#assign selectedJavaField = column.javaField>
      <#break>
    </#if>
    <#if column.pk>
      <#assign findPk = true>
    </#if>
  </#list>
</#if>
  data() {
    return {
      // 遮罩层
      loading: true,
      // 导出遮罩层
      exportLoading: false,
      // 选中数组
      ids: [],
      // 提示信息(code、name等)
      tips: [],
      // 非单个禁用
      single: true,
      // 非多个禁用
      multiple: true,
      // 显示搜索条件
      showSearch: true,
      // 总条数
      total: 0,
      // ${table.functionName}表格数据
      ${businessName}List: [],
      // 弹出层标题
      title: '',
      // 是否显示弹出层
      open: false,
<#-- 处理选择框。当有多个相同表名时需要一个标识参数 -->
<#list table.assocTableList as assocTable>
  <#if assocTable.appearCount gt 1>
      selectFlag: '',
      <#break>
  </#if>
</#list>
<#-- 特殊检索条件处理 -->
<#include "/vue/views/${r'$'}{moduleName}/${r'$'}{businessName}/params.vue.ftl">
      // 查询参数
      queryParams: {
        pageNum: 1,
        pageSize: 10,
<#-- 主表属性查询 -->
<#list table.columns as column>
<#if column.queryType??>
        ${column.javaField}: null,
</#if>
</#list>
<#-- 关联表属性查询 -->
<#if table.assocTableList?? && table.assocTableList?size gt 0>
<#list table.assocTableList as assocTable>
        ${assocTable.insAlias?uncap_first}: {
        <#list assocTable.columns as column>
          <#if column.queryType??>
          ${column.javaField}: null,
          </#if>
        </#list>
        },
</#list>
</#if>
      },
      // 表单参数
      form: {
        <#if table.assocTableList?? && table.assocTableList?size gt 0>
        <#list table.assocTableList as assocTable>
        ${assocTable.insAlias?uncap_first}: {}<#if assocTable_index != table.assocTableList?size - 1>,</#if>
        </#list>
        </#if>
      },
  <#-- 处理选择框 -->
  <#assign tableNames = ''>
  <#list table.columns as column>
  <#if !column.select>
  <#continue>
  </#if>
  <#if tableNames?index_of(assocTable.tableName) != -1>
  <#continue>
  </#if>
  <#assign assocTable = column.assocTable>
  <#assign insAlias = ''>
  <#assign functionName = ''>
  <#if assocTable.appearCount gt 1>
  <#assign insAlias = assocTable.referenceTable.insAlias>
  <#assign functionName = assocTable.referenceTable.functionName>
  <#else>
  <#assign insAlias = assocTable.insAlias>
  <#assign functionName = assocTable.functionName>
  </#if>
  <#assign tableNames = tableNames + assocTable.tableName + ','>
      // 是否显示选择${functionName}层
      open${insAlias?cap_first}: false,
      // ${functionName}选择遮罩层
      ${insAlias?uncap_first}Loading: true,
  </#list>
      // 表单校验
      rules: {
<#-- 主表字段验证 -->
<#list table.columns as column>
<#if column.required>
<#assign parentheseIndex = column.columnComment?index_of("（")>
<#if parentheseIndex != -1>
<#assign comment = column.columnComment?substring(0, parentheseIndex)>
<#else>
<#assign comment = column.columnComment>
</#if>
        ${column.javaField}: [
          {required: true, message: '${comment}不能为空', trigger: <#if column.htmlType == "select">'change'<#else>'blur'</#if>}
        ]<#if column_index != table.columns?size - 1>,</#if>
</#if>
</#list>
      }
    }
  },
  created() {
    this.getList()
  },
  methods: {
    /** 查询${table.functionName}列表 */
    getList() {
      this.loading = true
<#list table.columns as column>
<#if column.htmlType?? && column.queryType?? && column.htmlType == "datetime" && column.queryType == "BETWEEN">
      this.queryParams = this.addDateRange(this.queryParams, this.daterange${column.javaField?cap_first}, '${column.javaField?cap_first}')
</#if>
</#list>
      list${businessName?cap_first}(this.queryParams).then(response => {
        this.${businessName}List = response.rows
        this.total = response.total
        this.loading = false
      })
    },
    /** 取消按钮 */
    cancel() {
      this.open = false
      this.reset()
    },
    /** 表单重置 */
    reset() {
      this.form = {
<#list table.columns as column>
<#if column.htmlType == "radio">
        ${column.javaField}: <#if column.javaType.className == "Integer" || column.javaType.className == "Long">0<#else>"0"</#if><#if column_index != table.columns?size - 1 || table.assocTableList?size gt 0>,</#if>
<#elseif column.htmlType == "checkbox">
        ${column.javaField}: []<#if column_index != table.columns?size - 1 || table.assocTableList?size gt 0>,</#if>
<#else>
        ${column.javaField}: null<#if column_index != table.columns?size - 1 || table.assocTableList?size gt 0>,</#if>
</#if>
</#list>
<#if table.assocTableList?? && table.assocTableList?size gt 0>
  <#list table.assocTableList as assocTable>
        ${assocTable.insAlias?uncap_first}: {}<#if assocTable_index != table.assocTableList?size - 1>,</#if>
  </#list>
</#if>
      }
      this.resetForm('queryForm')
      this.resetQuery()
    },
    /** 搜索按钮操作 */
    handleQuery() {
      this.queryParams.pageNum = 1
      this.getList()
    },
    /** 重置按钮操作 */
    resetQuery() {
<#list table.columns as column>
<#if column.htmlType?? && column.queryType?? && column.htmlType == "datetime" && column.queryType == "BETWEEN">
      this.daterange${column.javaField?cap_first} = []
</#if>
</#list>
      this.resetForm('queryForm')
      this.handleQuery()
    },
    /** 多选框选中数据 */
    handleSelectionChange(selection) {
      this.ids = selection.map(item => item.${table.pkColumn.javaField})
      this.tips = selection.map(item => item.${selectedJavaField})
      this.single = selection.length !== 1
      this.multiple = !selection.length
    },
    /** 新增按钮操作 */
    handleAdd() {
      this.reset()
      this.open = true
      this.title = '添加${table.functionName}'
    },
    /** 修改按钮操作 */
    handleUpdate(row) {
      this.reset()
      const ${table.pkColumn.javaField} = row.${table.pkColumn.javaField} || this.ids
      get${businessName?cap_first}(${table.pkColumn.javaField}).then(response => {
        <#-- 考虑复合对象为空的情况 -->
        <#list table.columns as column>
        <#if column.javaType.type == 'YES'>
        <#continue>
        </#if>
        <#assign assocTable = column.assocTable>
        if (response.data.${assocTable.insAlias?uncap_first} == null) {
          let _${assocTable.insAlias?uncap_first} = {
            ${assocTable.primaryKey}: '',
            <#list assocTable.columns as col>
            <#if assocTable.primaryKey == col.javaField>
                <#continue>
            </#if>
            ${col.javaField}: ''<#if col_index != assocTable.columns?size - 1>,</#if>
            </#list>
          }
          response.data.${assocTable.insAlias?uncap_first} = _${assocTable.insAlias?uncap_first}
        }
        Object.assign(response.data, { ${column.javaField}: response.data.${assocTable.insAlias?uncap_first}.${assocTable.primaryKey} })
        </#list>
        this.form = response.data
        this.open = true
        this.title = '修改${table.functionName}'
      })
    },
    /** 提交按钮 */
    submitForm() {
      this.$refs['form'].validate(valid => {
        if (valid) {
          if (this.form.${table.pkColumn.javaField} != null) {
            update${businessName?cap_first}(this.form).then(response => {
              this.$modal.msgSuccess('修改成功')
              this.open = false
              this.getList()
            })
          } else {
            add${businessName?cap_first}(this.form).then(response => {
              this.$modal.msgSuccess('新增成功')
              this.open = false
              this.getList()
            })
          }
        }
      })
    },
    /** 删除按钮操作 */
    handleDelete(row) {
      const ${table.pkColumn.javaField}s = row.${table.pkColumn.javaField} || this.ids
      const tips = row.${selectedJavaField} || this.tips
      this.$modal.confirm('是否确认删除${table.functionName}编号为"' + tips + '"的数据项?').then(function() {
          return del${businessName?cap_first}(${table.pkColumn.javaField}s)
      }).then(() => {
          this.getList()
          this.$modal.msgSuccess('删除成功')
      }).catch(() => {})
    },
    <#-- 处理选择框 -->
    <#assign tableNames = ''>
    <#list table.columns as column>
    <#if !column.select>
    <#continue>
    </#if>
    <#if tableNames?index_of(assocTable.tableName) != -1>
    <#continue>
    </#if>
    <#assign assocTable = column.assocTable>
    <#assign insAlias = ''>
    <#assign functionName = ''>
    <#if assocTable.appearCount gt 1>
    <#assign insAlias = assocTable.referenceTable.insAlias>
    <#assign functionName = assocTable.referenceTable.functionName>
    <#else>
    <#assign insAlias = assocTable.insAlias>
    <#assign functionName = assocTable.functionName>
    </#if>
    <#assign tableNames = tableNames + assocTable.tableName + ','>
    /** 打开选择${functionName}对话框 */
    openSelect${insAlias?cap_first}Dialog(<#if assocTable.appearCount gt 1>flag</#if>) {
      this.title = '选择${functionName}'
      this.open${insAlias?cap_first} = true
      <#if assocTable.appearCount gt 1>
      this.selectFlag = flag
      </#if>
    },
    /** 处理选择的${functionName} */
    handle${insAlias?cap_first}Select(data<#if assocTable.appearCount gt 1>, flag</#if>) {
      this.open${insAlias?cap_first} = false
      <#if assocTable.appearCount gt 1>
      switch (flag) {
        <#list table.assocTableList as assTable>
        <#if assTable.tableName != assocTable.tableName>
            <#continue>
        </#if>
        case '${assTable.insAlias?uncap_first}':
          let _${assTable.insAlias?uncap_first} = {
            ${assocTable.primaryKey}: data.${assocTable.primaryKey},
            <#list assocTable.columns as col>
            <#if assocTable.primaryKey == col.javaField>
            <#continue>
            </#if>
            ${col.javaField}: data.${col.javaField}<#if column_index != assocTable.columns?size - 1>,</#if>
            </#list>
          }
          this.form.${assTable.insAlias?uncap_first} = _${assTable.insAlias?uncap_first}
          this.form.${assTable.parentColumn.javaField} = data.${assocTable.primaryKey}
          break
        </#list>
      }
      <#else>
      this.form.${column.javaField} = data.${assocTable.primaryKey}
      let _${assocTable.insAlias?uncap_first} = {
        ${assocTable.primaryKey}: data.${assocTable.primaryKey},
        <#list assocTable.columns as col>
        <#if assocTable.primaryKey == col.javaField>
        <#continue>
        </#if>
          ${col.javaField}: data.${col.javaField}<#if column_index != assocTable.columns?size - 1>,</#if>
        </#list>
      }
      this.form.${assocTable.insAlias?uncap_first} = _${assocTable.insAlias?uncap_first}
      </#if>
    },
    </#list>
    /** 导出按钮操作 */
    handleExport() {
        const queryParams = this.queryParams
        this.$modal.confirm('是否确认导出所有${table.functionName}数据项?').then(() => {
            this.exportLoading = true
            return export${businessName?cap_first}(queryParams)
        }).then(response => {
            this.$download.name(response.msg);
            this.exportLoading = false;
        }).catch(() => {})
    }
  }
}
</script>