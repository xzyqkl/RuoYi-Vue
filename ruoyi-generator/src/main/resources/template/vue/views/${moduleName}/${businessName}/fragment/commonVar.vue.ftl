<#-- 通用变量定义 -->
<#-- 处理需要引入的组件-->
<#assign importCommponents = "">
<#assign commponents = "">
<#assign selectMap = {}>
<#assign tableNames = ''>
<#list table.columns as column>
  <#-- 处理通用组件 -->
  <#assign ic = "">
  <#assign c = "">
  <#if column.isEdit == '1' && !column.superColumn && !column.pk>
    <#if column.htmlType == "imageUpload">
      <#assign ic = "import ImageUpload from '@/components/ImageUpload'">
      <#assign c = "ImageUpload">
    <#elseif column.htmlType == "fileUpload">
      <#assign ic = "import FileUpload from '@/components/FileUpload'">
      <#assign c = "FileUpload">
    <#elseif column.htmlType == "editor">
      <#assign ic = "import Editor from '@/components/Editor'">
      <#assign c = "Editor">
    </#if>
    <#if commponents?index_of(c) == -1>
      <#if commponents?length == 0>
        <#assign importCommponents = ic>
        <#assign commponents = c>
      <#else>
        <#assign importCommponents = importCommponents + "," + ic>
        <#assign commponents = commponents + "," + c>
      </#if>
    </#if>
  </#if>
  <#-- 处理选择框组件 -->
  <#assign assocTable = column.assocTable!>
  <#if column.select && assocTable?? && tableNames?index_of(assocTable.tableName) == -1>
  <#-- 如果对用一个表关联了多次，则使用表的原有实例名称，多个子实例共同引用。例如：人（people)的父亲为father母亲为mother ，在选择时使用people作为组件名称 -->
    <#assign insAlias = ''>
    <#if assocTable.appearCount gt 1>
        <#assign insAlias = assocTable.referenceTable.insAlias>
    <#else>
        <#assign insAlias = assocTable.insAlias>
    </#if>
    <#assign tableNames = tableNames + assocTable.tableName + ','>
    <#if commponents?length == 0>
      <#assign importCommponents = "import ${insAlias?cap_first}Select from './${insAlias?cap_first}Select'">
      <#assign commponents = "${insAlias?cap_first}Select">
    <#else>
      <#assign importCommponents = importCommponents + "," + "import ${insAlias?cap_first}Select from './${insAlias?cap_first}Select'">
      <#assign commponents = commponents + "," + "${insAlias?cap_first}Select">
    </#if>
  </#if>
</#list>
<#list table.subTables as subTable>
  <#assign ic = "import ${subTable.insAlias?cap_first}Form from './${subTable.insAlias?cap_first}Form'">
  <#assign c = "${subTable.insAlias?cap_first}Form">
  <#if commponents?length == 0>
    <#assign importCommponents = ic>
    <#assign commponents = c>
  <#else>
    <#assign importCommponents = importCommponents + "," + ic>
    <#assign commponents = commponents + "," + c>
  </#if>
</#list>