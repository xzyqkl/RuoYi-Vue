<#-- 生成特殊检索条件参数 -->
<#assign prefix = ''>
<#assign prefixName = ''>
<#list table.columns as column>
  <#if column.queryType??>
    <#assign parentheseIndex = column.columnComment?index_of("（")>
    <#if parentheseIndex != -1>
      <#assign comment = column.columnComment?substring(0, parentheseIndex)>
    <#else>
      <#assign comment = column.columnComment>
    </#if>
    <#if column.htmlType?? && column.queryType?? && (column.htmlType == "datetime" || column.htmlType == "date") && column.queryType == "BETWEEN">
      // <#if prefixName?? && prefixName?length gt 0>${prefixName}</#if>${comment}时间范围
      daterange<#if prefix?? && prefix?length gt 0>${prefix?cap_first}</#if>${column.javaField?cap_first}: [],
    </#if>
  </#if>
</#list>
<#if table.assocTableList?? && table.assocTableList?size gt 0>
  <#list table.assocTableList as assocTable>
    <#assign prefix = assocTable.insAlias?uncap_first>
    <#assign prefixName = assocTable.functionName>
    <#list assocTable.columns as column>
      <#if column.queryType??>
        <#assign parentheseIndex = column.columnComment?index_of("（")>
        <#if parentheseIndex != -1>
          <#assign comment = column.columnComment?substring(0, parentheseIndex)>
        <#else>
          <#assign comment = column.columnComment>
        </#if>
        <#if column.htmlType?? && column.queryType?? && (column.htmlType == "datetime" || column.htmlType == "date") && column.queryType == "BETWEEN">
          // <#if prefixName?? && prefixName?length gt 0>${prefixName}</#if>${comment}时间范围
          daterange<#if prefix?? && prefix?length gt 0>${prefix?cap_first}</#if>${column.javaField?cap_first}: [],
        </#if>
      </#if>
    </#list>
  </#list>
</#if>