<#-- 主副表结构：主表-->
<template>
  <div class="app-container">
    <#-- 搜索条件 -->
    <el-form size="mini" :model="queryParams" ref="queryForm" :inline="true" v-show="showSearch" label-width="90px">
    <#list table.columns as column>
      <#assign prefix = ''>
      <#assign prefixName = ''>
      <#if column.assocTable??>
        <#assign prefix = column.assocTable.insAlias?uncap_first>
        <#assign prefixName = column.assocTable.functionName>
        <#list column.assocTable.columns as column>
          <#if column.queryType??>
            <#include "/vue/views/${r'$'}{moduleName}/${r'$'}{businessName}/fragment/search.vue.ftl">
          </#if>
        </#list>
      <#else>
        <#if column.queryType?? && !column.pk>
          <#include "/vue/views/${r'$'}{moduleName}/${r'$'}{businessName}/fragment/search.vue.ftl">
        </#if>
      </#if>
    </#list>
      <el-form-item>
        <el-button type="primary" icon="el-icon-search" size="mini" @click="handleQuery">搜索</el-button>
        <el-button icon="el-icon-refresh" size="mini" @click="resetQuery">重置</el-button>
      </el-form-item>
    </el-form>

    <el-row :gutter="10" class="mb8">
      <el-col :span="1.5">
        <el-button
          type="primary"
          plain
          icon="el-icon-plus"
          size="mini"
          @click="handleAdd"
          v-hasPermi="['${moduleName}:${businessName}:add']"
        >新增
        </el-button>
      </el-col>
      <el-col :span="1.5">
        <el-button
          type="success"
          plain
          icon="el-icon-edit"
          size="mini"
          :disabled="single"
          @click="handleUpdate"
          v-hasPermi="['${moduleName}:${businessName}:edit']"
        >修改
        </el-button>
      </el-col>
      <el-col :span="1.5">
        <el-button
          type="danger"
          plain
          icon="el-icon-delete"
          size="mini"
          :disabled="multiple"
          @click="handleDelete"
          v-hasPermi="['${moduleName}:${businessName}:remove']"
        >删除
        </el-button>
      </el-col>
      <el-col :span="1.5">
        <el-button
          type="warning"
          plain
          icon="el-icon-download"
          size="mini"
          @click="handleExport"
          v-hasPermi="['${moduleName}:${businessName}:export']"
        >导出
        </el-button>
      </el-col>
      <right-toolbar :showSearch.sync="showSearch" @queryTable="getList"></right-toolbar>
    </el-row>

    <el-table v-loading="loading" :data="${businessName}List" @selection-change="handleSelectionChange">
      <el-table-column type="selection" width="55" align="center"/>
<#list table.columns as column>
  <#assign prefix = ''>
  <#assign prefixName = ''>
  <#if column.assocTable??>
    <#assign prefix = column.assocTable.insAlias?uncap_first>
    <#assign prefixName = column.assocTable.functionName>
    <#list column.assocTable.columns as column>
      <#if column.list>
    <@format blank=6><#include "/vue/views/${r'$'}{moduleName}/${r'$'}{businessName}/fragment/table.vue.ftl"></@format>
      </#if>
    </#list>
  <#else>
    <#if column.pk || !column.list>
      <#continue>
    </#if>
    <@format blank=6><#include "/vue/views/${r'$'}{moduleName}/${r'$'}{businessName}/fragment/table.vue.ftl"></@format>
  </#if>
</#list>
      <el-table-column label="操作" align="center" class-name="small-padding fixed-width">
        <template slot-scope="scope">
          <el-button
            size="mini"
            type="text"
            icon="el-icon-view"
            @click="handleView(scope.row)"
            v-hasPermi="['livestock:buy:add']"
          >查看
          </el-button>
          <el-button
            size="mini"
            type="text"
            icon="el-icon-edit"
            @click="handleUpdate(scope.row)"
            v-hasPermi="['${moduleName}:${businessName}:edit']"
          >修改
          </el-button>
          <el-button
            size="mini"
            type="text"
            icon="el-icon-delete"
            @click="handleDelete(scope.row)"
            v-hasPermi="['${moduleName}:${businessName}:remove']"
          >删除
          </el-button>
        </template>
      </el-table-column>
    </el-table>

    <pagination
      v-show="total>0"
      :total="total"
      :page.sync="queryParams.pageNum"
      :limit.sync="queryParams.pageSize"
      @pagination="getList"
    />

    <${table.insAlias?cap_first}Form  ref="${table.insAlias?uncap_first}Form" @refreshData="handleQuery"></${table.insAlias?cap_first}Form>
  </div>
</template>

<script>
import {
  list${businessName?cap_first},
  del${businessName?cap_first},
  export${businessName?cap_first}
} from '@/api/${moduleName}/${businessName}'
import ${table.insAlias?cap_first}Form from './${table.insAlias?cap_first}Form'
export default {
  name: '${businessName?cap_first}',
  <#include "/vue/views/${r'$'}{moduleName}/${r'$'}{businessName}/dicts.vue.ftl">
  components: {
      ${table.insAlias?cap_first}Form
  },
<#-- 处理，用来做提示信息的属性。主键下边input类型的那个字段用来做提示 优先使用包含name的字段 -->
<#assign selectedJavaField = ''>
<#assign findName = false>
<#list table.columns as column>
  <#if column.javaField?lower_case?index_of("name") != -1>
    <#assign findName = true>
    <#assign selectedJavaField = column.javaField>
    <#break>
  </#if>
</#list>
<#if !findName>
  <#assign findPk = false> <#-- 标识是否找到主键 -->
  <#list table.columns as column>
    <#if findPk && column.list && column.htmlType == 'input'>
      <#assign selectedJavaField = column.javaField>
      <#break>
    </#if>
    <#if column.pk>
      <#assign findPk = true>
    </#if>
  </#list>
</#if>
  data() {
    return {
      // 遮罩层
      loading: true,
      // 选中数组
      ids: [],
      // 提示信息(code、name等)
      tips: [],
      // 非单个禁用
      single: true,
      // 非多个禁用
      multiple: true,
      // 显示搜索条件
      showSearch: true,
      // 总条数
      total: 0,
      // ${table.functionName}表格数据
      ${businessName}List: [],
<#-- 特殊检索条件处理 -->
<#include "/vue/views/${r'$'}{moduleName}/${r'$'}{businessName}/params.vue.ftl">
      // 查询参数
      queryParams: {
        pageNum: 1,
        pageSize: 10,
<#-- 主表属性查询 -->
<#list table.columns as column>
<#if column.queryType??>
        ${column.javaField}: null,
</#if>
</#list>
<#-- 关联表属性查询 -->
<#if table.assocTableList?? && table.assocTableList?size gt 0>
<#list table.assocTableList as assocTable>
        ${assocTable.insAlias?uncap_first}: {
        <#list assocTable.columns as column>
          <#if column.queryType??>
          ${column.javaField}: null,
          </#if>
        </#list>
        },
</#list>
</#if>
      },

    }
  },
  created() {
    this.getList()
  },
  methods: {
    /** 查询${table.functionName}列表 */
    getList() {
      this.loading = true
<#list table.columns as column>
<#if column.htmlType?? && column.queryType?? && column.htmlType == "datetime" && column.queryType == "BETWEEN">
      this.queryParams = this.addDateRange(this.queryParams, this.daterange${column.javaField?cap_first}, '${column.javaField?cap_first}')
</#if>
</#list>
      list${businessName?cap_first}(this.queryParams).then(response => {
        this.${businessName}List = response.rows
        this.total = response.total
        this.loading = false
      })
    },
    /** 搜索按钮操作 */
    handleQuery() {
      this.queryParams.pageNum = 1
      this.getList()
    },
    /** 重置按钮操作 */
    resetQuery() {
<#list table.columns as column>
<#if column.htmlType?? && column.queryType?? && column.htmlType == "datetime" && column.queryType == "BETWEEN">
      this.daterange${column.javaField?cap_first} = []
</#if>
</#list>
      this.resetForm('queryForm')
      this.handleQuery()
    },
    /** 多选框选中数据 */
    handleSelectionChange(selection) {
      this.ids = selection.map(item => item.${table.pkColumn.javaField})
      this.tips = selection.map(item => item.${selectedJavaField})
      this.single = selection.length !== 1
      this.multiple = !selection.length
    },
    /** 新增按钮操作 */
    handleAdd() {
        this.$refs.${table.insAlias?uncap_first}Form.init('add', '')
    },
    handleView(row) {
        const ${table.pkColumn.javaField} = row.${table.pkColumn.javaField} || this.ids
        this.$refs.${table.insAlias?uncap_first}Form.init('view', ${table.pkColumn.javaField})
    },
    handleUpdate(row) {
        const ${table.pkColumn.javaField} = row.${table.pkColumn.javaField} || this.ids
        this.$refs.${table.insAlias?uncap_first}Form.init('edit', ${table.pkColumn.javaField})
    },
    /** 删除按钮操作 */
    handleDelete(row) {
      const ${table.pkColumn.javaField}s = row.${table.pkColumn.javaField} || this.ids
      const tips = row.${selectedJavaField} || this.tips
      this.$modal.confirm('是否确认删除${table.functionName}编号为"' + tips + '"的数据项?').then(function() {
        return del${businessName?cap_first}(${table.pkColumn.javaField}s)
      }).then(() => {
        this.getList()
        this.$modal.msgSuccess('删除成功')
      }).catch(() => {})
    },
    /** 导出按钮操作 */
    handleExport() {
      const queryParams = this.queryParams
      this.$modal.confirm('是否确认导出所有${table.functionName}数据项?').then(() => {
        this.exportLoading = true
        return export${businessName?cap_first}(queryParams)
      }).then(response => {
        this.$download.name(response.msg);
        this.exportLoading = false;
      }).catch(() => {})
    }
  }
}
</script>