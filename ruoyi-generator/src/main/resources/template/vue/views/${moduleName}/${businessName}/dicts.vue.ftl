<#-- 字典引入 -->
<#-- 处理数据字典开始 -->
  <#assign dictArray = ''>
  <#list table.columns as column>
  <#if column.dictType?? && dictArray?index_of(column.dictType) == -1>
    <#if dictArray?length == 0>
      <#assign dictArray = '\'' + column.dictType + '\''>
      <#else>
      <#assign dictArray = dictArray + ',' + '\'' + column.dictType + '\''>
    </#if>
  </#if>
  </#list>
  <#if table.assocTableList?? && table.assocTableList?size gt 0>
    <#list table.assocTableList as assocTable>
      <#list assocTable.columns as column>
        <#if column.dictType??>
          <#if dictArray?length == 0>
            <#assign dictArray = '\'' + column.dictType + '\''>
            <#else>
            <#assign dictArray = dictArray + ',' + '\'' + column.dictType + '\''>
          </#if>
        </#if>
      </#list>
    </#list>
  </#if>
  <#if table.subTables?? && table.subTables?size gt 0>
    <#list table.subTables as subTable>
      <#list subTable.columns as column>
        <#if column.dictType??>
          <#if dictArray?length == 0>
            <#assign dictArray = '\'' + column.dictType + '\''>
            <#else>
            <#assign dictArray = dictArray + ',' + '\'' + column.dictType + '\''>
          </#if>
        </#if>
      </#list>
    </#list>
  </#if>
  <#if dictArray?length &gt; 0>
  dicts: [${dictArray}],
  </#if>
  <#-- 处理数据字典结束-->