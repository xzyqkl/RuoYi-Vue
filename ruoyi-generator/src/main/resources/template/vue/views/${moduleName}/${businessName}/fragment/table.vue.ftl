<#assign parentheseIndex = column.columnComment?index_of("（")>
<#if parentheseIndex != -1>
<#assign comment = column.columnComment?substring(0, parentheseIndex)>
<#else>
<#assign comment = column.columnComment>
</#if>
<#if column.htmlType == "imageUpload">
<el-table-column label="<#if prefixName?? && prefixName?length gt 0>${prefixName}</#if>${comment}" align="center">
  <template slot-scope="scope">
    <el-image
      style="width: 100px; height: 100px"
      :src="dealImg(scope.row.<#if prefix?? && prefix?length gt 0>${prefix}.</#if>${column.javaField})[0]"
      :preview-src-list="dealImg(scope.row.<#if prefix?? && prefix?length gt 0>${prefix}.</#if>${column.javaField})"
    >
      <div slot="placeholder" class="image-slot">
        加载中<span class="dot">...</span>
      </div>
      <div slot="error" class="image-slot">
        <i class="el-icon-picture-outline"></i>
      </div>
    </el-image>
  </template>
</el-table-column>
<#elseif column.htmlType == "datetime">
<el-table-column label="<#if prefixName?? && prefixName?length gt 0>${prefixName}</#if>${comment}" align="center" width="180">
  <template slot-scope="scope">
    <span>{{ parseTime(scope.row.<#if prefix?? && prefix?length gt 0>${prefix}.</#if>${column.javaField}, '{y}-{m}-{d}') }}</span>
  </template>
</el-table-column>
<#elseif column.dictType??>
<el-table-column label="<#if prefixName?? && prefixName?length gt 0>${prefixName}</#if>${comment}" align="center" prop="<#if prefix?? && prefix?length gt 0>${prefix}.</#if>${column.javaField}">
  <template slot-scope="scope">
    <dict-tag :options="dict.type.${column.dictType}" :value="scope.row.<#if prefix?? && prefix?length gt 0>${prefix}.</#if>${column.javaField}"/>
  </template>
</el-table-column>
<#else>
<el-table-column label="<#if prefixName?? && prefixName?length gt 0>${prefixName}</#if>${comment}" align="center" prop="<#if prefix?? && prefix?length gt 0>${prefix}.</#if>${column.javaField}"/>
</#if>
