<#-- 用于生成form item -->
<#-- 输出表字段。非主键、可编辑或仅显示的字段-->
          <#list table.columns as column>
            <#assign field = column.javaField>
            <#if column.isEdit == '0' || column.pk>
              <#continue>
            </#if>
            <#assign parentheseIndex = column.columnComment?index_of("（")>
            <#if parentheseIndex != -1>
              <#assign comment = column.columnComment?substring(0, parentheseIndex)>
            <#else>
              <#assign comment = column.columnComment>
            </#if>
            <#-- 设置文本域、上传图片、上传文件、编辑器 独占一行-->
            <#assign allSpanFilter = "textarea,imageUpload,fileUpload,editor">
            <#if allSpanFilter?contains(column.htmlType)>
                <#assign colSpan = 24>
            <#else>
                <#assign colSpan = 24 / columnSize>
            </#if>
          <el-col :md="${colSpan}" :sm="24">
            <#-- 是否为弹出选择框-->
            <#if column.select>
              <#assign assocTable = column.assocTable>
              <#assign insAlias = ''>
              <#if assocTable.appearCount gt 1>
                  <#assign insAlias = assocTable.referenceTable.insAlias>
              <#else>
                  <#assign insAlias = assocTable.insAlias>
              </#if>
              <#assign field = assocTable.columns[0].javaField>
            <el-form-item label="${assocTable.functionName}" prop="${column.javaField}">
              <el-input placeholder="请输入${assocTable.functionName}" v-model="form.${assocTable.insAlias?uncap_first}.${field}" :disabled="true">
                <el-button slot="append" icon="el-icon-search" @click="openSelect${insAlias?cap_first}Dialog<#if assocTable.appearCount gt 1>('${assocTable.insAlias}')</#if>"></el-button>
              </el-input>
            </el-form-item>
            <#-- 仅显示 -->
            <#elseif column.isEdit == '2'>
            <el-form-item label="${comment}" prop="${field}">
              <span>{{ form.${field} }}</span>
            </el-form-item>
            <#elseif column.htmlType == "input">
            <el-form-item label="${comment}" prop="${field}">
              <el-input v-model="form.${field}" placeholder="请输入${comment}"/>
            </el-form-item>
            <#elseif column.htmlType == "textarea">
            <el-form-item label="${comment}" prop="${field}">
              <el-input v-model="form.${field}" type="textarea" placeholder="请输入${comment}"/>
            </el-form-item>
            <#elseif column.htmlType == "select">
              <#if column.dictType??>
            <el-form-item label="${comment}" prop="${field}">
              <el-select v-model="form.${field}" placeholder="请选择${comment}"<#if column.multi> multiple</#if>>
                <el-option
                  v-for="dict in dict.type.${column.dictType}"
                  :key="dict.value"
                  :label="dict.label"
                  <#if column.javaType.className == "Integer" || column.javaType.className == "Long">:value="parseInt(dict.value)"<#else>:value="dict.value"</#if>
                ></el-option>
              </el-select>
            </el-form-item>
              <#else>
            <el-form-item label="${comment}" prop="${field}">
              <el-select v-model="form.${field}" placeholder="请选择${comment}">
                <el-option label="请选择字典生成" value=""/>
              </el-select>
            </el-form-item>
              </#if>
            <#elseif column.htmlType == "checkbox">
              <#if column.dictType??>
            <el-form-item label="${comment}" prop="${field}">
              <el-checkbox-group v-model="form.${field}">
                <el-checkbox
                  v-for="dict in dict.type.${column.dictType}"
                  :key="dict.value"
                  :label="dict.value">
                  {{dict.label}}
                </el-checkbox>
              </el-checkbox-group>
            </el-form-item>
              <#else>
            <el-form-item label="${comment}">
              <el-checkbox-group v-model="form.${field}">
                <el-checkbox>请选择字典生成</el-checkbox>
              </el-checkbox-group>
            </el-form-item>
              </#if>
            <#elseif column.htmlType == "radio">
              <#if column.dictType??>
            <el-form-item label="${comment}" prop="${field}">
              <el-radio-group v-model="form.${field}">
                <el-radio
                  v-for="dict in dict.type.${column.dictType}"
                  :key="dict.value"
                  <#if column.javaType.className == "Integer" || column.javaType.className == "Long">:label="parseInt(dict.value)"<#else>:label="dict.value"</#if>
                >{{dict.label}}</el-radio>
              </el-radio-group>
            </el-form-item>
              <#else>
            <el-form-item label="${comment}" prop="${field}">
              <el-radio-group v-model="form.${field}">
                <el-radio label="1">请选择字典生成</el-radio>
              </el-radio-group>
            </el-form-item>
              </#if>
            <#elseif column.htmlType == "date">
            <el-form-item label="${comment}" prop="${field}">
              <el-date-picker
                clearable
                v-model="form.${field}"
                type="date"
                value-format="yyyy-MM-dd"
                placeholder="选择${comment}">
              </el-date-picker>
            </el-form-item>
            <#elseif column.htmlType == "datetime">
            <el-form-item label="${comment}" prop="${field}">
              <el-date-picker
                clearable
                v-model="form.${field}"
                type="datetime"
                value-format="yyyy-MM-dd HH:mm:ss"
                placeholder="选择${comment}">
              </el-date-picker>
            </el-form-item>
            <#elseif column.htmlType == "imageUpload">
            <el-form-item label="${comment}" prop="${field}">
              <imageUpload v-model="form.${field}"/>
            </el-form-item>
            <#elseif column.htmlType == "fileUpload">
            <el-form-item label="${comment}" prop="${field}">
              <fileUpload v-model="form.${field}"/>
            </el-form-item>
            <#elseif column.htmlType == "editor">
            <el-form-item label="${comment}" prop="${field}">
              <editor v-model="form.${field}" :min-height="192"/>
            </el-form-item>
            </#if>
          </el-col>
          </#list>