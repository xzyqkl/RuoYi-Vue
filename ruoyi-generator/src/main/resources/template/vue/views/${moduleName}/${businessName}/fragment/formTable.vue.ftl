<#--
主Form的子表表格
 添加
 表格
  列字段 操作
 -->
<el-row :gutter="10" class="mb8">
  <el-col :span="1.5">
    <el-button v-if="method != 'view'" type="primary" icon="el-icon-plus" size="mini"
               @click="add${subTable.insAlias?cap_first}Row">添加
    </el-button>
  </el-col>
</el-row>
<el-table :data="form.${subTable.insAlias?uncap_first}List" ref="${subTable.insAlias?uncap_first}">
  <#assign prefix = ''>
  <#assign prefixName = ''>
  <#list subTable.columns as column>
    <#if column.list>
  <@format blank=2><#include "/vue/views/${r'$'}{moduleName}/${r'$'}{businessName}/fragment/table.vue.ftl"></@format>
    </#if>
  </#list>
  <el-table-column v-if="method != 'view'" label="操作" align="center" class-name="small-padding fixed-width">
    <template slot-scope="scope">
      <el-button
        size="mini"
        type="text"
        icon="el-icon-edit"
        @click="view${subTable.insAlias?cap_first}Row(scope.row)"
      >查看
      </el-button>
      <el-button
        size="mini"
        type="text"
        icon="el-icon-edit"
        @click="edit${subTable.insAlias?cap_first}Row(scope.row)"
      >修改
      </el-button>
      <el-button
        size="mini"
        type="text"
        icon="el-icon-delete"
        @click="del${subTable.insAlias?cap_first}Row(scope.row)"
      >删除
      </el-button>
    </template>
  </el-table-column>
</el-table>
