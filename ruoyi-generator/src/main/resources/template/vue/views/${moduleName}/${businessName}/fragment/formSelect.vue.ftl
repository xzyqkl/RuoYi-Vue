<#-- form 里的选择框-->
<#-- 处理选择对话框（相同名称的对话框只有一个） -->
    <#assign tableNames = ''>
    <#list table.columns as column>
        <#if !column.select || tableNames?index_of(assocTable.tableName) != -1>
            <#continue>
        </#if>
        <#assign assocTable = column.assocTable>
        <#assign insAlias = ''>
        <#if assocTable.appearCount gt 1>
            <#assign insAlias = assocTable.referenceTable.insAlias>
        <#else>
            <#assign insAlias = assocTable.insAlias>
        </#if>
        <#assign tableNames = tableNames + assocTable.tableName + ','>

    <!-- 选择${assocTable.functionName}对话框 -->
    <el-dialog :title="title" :visible.sync="open${insAlias?cap_first}" width="60%" append-to-body>
        <${insAlias?cap_first}Select<#if assocTable.appearCount gt 1> :selectFlag="selectFlag"</#if> v-if="open${insAlias?cap_first}" @select="handle${insAlias?cap_first}Select"></${insAlias?cap_first}Select>
    </el-dialog>
      </#list>