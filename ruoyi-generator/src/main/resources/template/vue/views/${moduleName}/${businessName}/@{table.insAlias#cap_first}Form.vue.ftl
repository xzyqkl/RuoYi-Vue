<#--
子记录，不考虑子表
-->
<#include "/vue/views/${r'$'}{moduleName}/${r'$'}{businessName}/fragment/commonVar.vue.ftl">
<template>
  <div>
    <!-- 添加或修改${table.functionName}对话框 -->
    <el-dialog :title="title" :visible.sync="open" width="60%" append-to-body>
      <el-form ref="form" :model="form" :disabled="method == 'view'" :rules="rules"
               label-width="80px">
        <el-row>
          <#include "/vue/views/${r'$'}{moduleName}/${r'$'}{businessName}/fragment/formItem.vue.ftl">
        </el-row>
        <#-- 处理子表 -->
        <#if table.subTables?size gt 0>
        <el-tabs v-model="${table.insAlias?uncap_first}Tab">
        <#list table.subTables as subTable>
          <#assign parentheseIndex = subTable.tableComment?index_of("（")>
          <#if parentheseIndex != -1>
              <#assign comment = subTable.tableComment?substring(0, parentheseIndex)>
          <#else>
              <#assign comment = subTable.tableComment>
          </#if>
          <el-tab-pane label="${comment}">
          <#include "/vue/views/${r'$'}{moduleName}/${r'$'}{businessName}/fragment/formTable.vue.ftl">
          </el-tab-pane>
        </#list>
        </el-tabs>
        </#if>
      </el-form>
      <div slot="footer" class="dialog-footer">
        <el-button v-if="method != 'view'" type="primary" @click="continueSubmitForm">继续添加</el-button>
        <el-button v-if="method != 'view'" type="primary" @click="submitForm">确 定</el-button>
        <el-button @click="cancel">取 消</el-button>
      </div>
    </el-dialog>
    <#list table.subTables as subTable>

    <${subTable.insAlias?cap_first}Form ref="${subTable.insAlias?uncap_first}Form" @addRow="save${subTable.insAlias?cap_first}Row(arguments)"></${subTable.insAlias?cap_first}Form>
    </#list>
    <#-- 引入选择弹出框 -->
    <#include "/vue/views/${r'$'}{moduleName}/${r'$'}{businessName}/fragment/formSelect.vue.ftl">
  </div>
</template>
<script>
<#-- 2. 引入通用组件（imageUpload、fileUpload等）、选择框组件等 -->
<#if importCommponents?length gt 0>
  <#list importCommponents?split(",") as ic>
${ic}
  </#list>
</#if>
<#-- 4. 若有子表，则引入子表Form组件 -->
<#-- 5. 编写export default -->
export default {
  <#-- 1. -->
  name: '${table.insAlias?cap_first}Form',
  <#include "/vue/views/${r'$'}{moduleName}/${r'$'}{businessName}/dicts.vue.ftl">
  components: {
  <#if commponents?length gt 0>
  <#list commponents?split(",") as c>
    ${c}<#if c_has_next>,</#if>
  </#list>
  </#if>
  },
  data() {
    return {
      title: '',
      method: '',
      open: false,
      oldForm: '',
      <#if table.subTables?size gt 0>
      ${table.insAlias?uncap_first}Tab: '0',
      </#if>
      // 表单参数
      form: {
        <#list table.columns as column>
        ${column.javaField}: null,
        </#list>
        <#if table.assocTableList?? && table.assocTableList?size gt 0>
        <#list table.assocTableList as assocTable>
        ${assocTable.insAlias?uncap_first}: {}<#if assocTable_index != table.assocTableList?size - 1>,</#if>
        </#list>
        </#if>
      },
    <#-- 特殊检索条件处理 -->
    <#include "/vue/views/${r'$'}{moduleName}/${r'$'}{businessName}/params.vue.ftl">
    <#-- 处理选择框。当有多个相同表名时需要一个标识参数 -->
    <#list table.assocTableList as assocTable>
    <#if assocTable.appearCount gt 1>
    selectFlag: '',
      <#break>
    </#if>
    </#list>
      <#-- 处理选择框 -->
      <#assign tableNames = ''>
      <#list table.columns as column>
      <#if !column.select || tableNames?index_of(assocTable.tableName) != -1>
      <#continue>
      </#if>
      <#assign assocTable = column.assocTable>
      <#assign insAlias = ''>
      <#assign functionName = ''>
      <#if assocTable.appearCount gt 1>
      <#assign insAlias = assocTable.referenceTable.insAlias>
      <#assign functionName = assocTable.referenceTable.functionName>
      <#else>
      <#assign insAlias = assocTable.insAlias>
      <#assign functionName = assocTable.functionName>
      </#if>
      <#assign tableNames = tableNames + assocTable.tableName + ','>
      // 是否显示选择${functionName}层
      open${insAlias?cap_first}: false,
      // ${functionName}选择遮罩层
      ${insAlias?uncap_first}Loading: true,
      </#list>
      // 表单校验
      rules: {
        <#-- 主表字段验证 -->
        <#list table.columns as column>
        <#if column.required>
        <#assign parentheseIndex = column.columnComment?index_of("（")>
        <#if parentheseIndex != -1>
        <#assign comment = column.columnComment?substring(0, parentheseIndex)>
        <#else>
        <#assign comment = column.columnComment>
        </#if>
        ${column.javaField}: [
          {required: true, message: '${comment}不能为空', trigger: <#if column.htmlType == "select">'change'<#else>'blur'</#if>}
        ]<#if column_index != table.columns?size - 1>,</#if>
        </#if>
        </#list>
      }
    }
  },
  created() {
  },
  methods: {
    init(method, obj) {
      this.method = method
      if (method === 'add') {
        this.title = '添加${table.functionName}'
      } else if (method === 'edit') {
        this.title = '修改${table.functionName}'
      } else if (method === 'view') {
        this.title = '查看${table.functionName}'
      }
      this.open = true
      this.reset()
      this.form.${table.pkColumn.javaField} = ''
      this.oldForm = ''
      <#if table.subTables?size gt 0>
      this.${table.insAlias?uncap_first}Tab = '0'
      </#if>
      <#list table.subTables as subTable>
      this.form.${subTable.insAlias?uncap_first}List = []
      </#list>
      if (method === 'edit' || method === 'view') { // 修改或者查看
        this.oldForm = obj
        this.form = JSON.parse(JSON.stringify(obj))
      }
    },
    /** 重置表单 */
    reset() {
      this.form = {
      <#list table.columns as column>
        <#if column.htmlType == "radio">
        ${column.javaField}: <#if column.javaType.className == "Integer" || column.javaType.className == "Long">0<#else>"0"</#if>,
        <#elseif column.htmlType == "checkbox">
        ${column.javaField}: [],
        <#else>
        ${column.javaField}: null,
        </#if>
      </#list>
      <#list table.assocTableList as assocTable>
        ${assocTable.insAlias?uncap_first}: {},
      </#list>
      <#list table.subTables as subTable>
        ${subTable.insAlias?uncap_first}List: [],
      </#list>
      }
      this.resetForm('form')
    },
    /** 取消按钮 */
    cancel() {
      this.open = false
      this.reset()
    },
    // 表单提交
    submitForm() {
      this.$refs['form'].validate((valid) => {
        if (valid) {
          this.$emit('addRow', this.oldForm, JSON.parse(JSON.stringify(this.form)))
          this.open = false
        }
      })
    },
    continueSubmitForm() {
      this.$refs['form'].validate((valid) => {
        if (valid) {
          this.$emit('addRow', this.oldForm, JSON.parse(JSON.stringify(this.form)))
          this.reset()
        }
      })
    },
    <#list table.subTables as subTable>
    add${subTable.insAlias?cap_first}Row(child) {
      this.$refs.${subTable.insAlias?uncap_first}Form.init('add')
    },
    view${subTable.insAlias?cap_first}Row(child) {
      this.$refs.${subTable.insAlias?uncap_first}Form.init('view', child)
    },
    edit${subTable.insAlias?cap_first}Row(child) {
      this.$refs.${subTable.insAlias?uncap_first}Form.init('edit', child)
    },
    del${subTable.insAlias?cap_first}Row(child)  {
      <#-- 从内存中删除数据 -->
      this.form.${subTable.insAlias?uncap_first}List.forEach((item, index) => {
        if (item === child) {
          this.form.${subTable.insAlias?uncap_first}List.splice(index, 1)
        }
      })
    },
    </#list>
    <#-- 处理选择框 -->
    <#assign tableNames = ''>
    <#list table.columns as column>
    <#if !column.select>
    <#continue>
    </#if>
    <#if tableNames?index_of(assocTable.tableName) != -1>
    <#continue>
    </#if>
    <#assign assocTable = column.assocTable>
    <#assign insAlias = ''>
    <#assign functionName = ''>
    <#if assocTable.appearCount gt 1>
    <#assign insAlias = assocTable.referenceTable.insAlias>
    <#assign functionName = assocTable.referenceTable.functionName>
    <#else>
    <#assign insAlias = assocTable.insAlias>
    <#assign functionName = assocTable.functionName>
    </#if>
    <#assign tableNames = tableNames + assocTable.tableName + ','>
    /** 打开选择${functionName}对话框 */
    openSelect${insAlias?cap_first}Dialog(<#if assocTable.appearCount gt 1>flag</#if>) {
        this.title = '选择${functionName}'
        this.open${insAlias?cap_first} = true
        <#if assocTable.appearCount gt 1>
        this.selectFlag = flag
        </#if>
    },
    /** 处理选择的${functionName} */
    handle${insAlias?cap_first}Select(data<#if assocTable.appearCount gt 1>, flag</#if>) {
      this.open${insAlias?cap_first} = false
      <#if assocTable.appearCount gt 1>
      switch (flag) {
        <#list table.assocTableList as assTable>
        <#if assTable.tableName != assocTable.tableName>
        <#continue>
        </#if>
        case '${assTable.insAlias?uncap_first}':
            let _${assTable.insAlias?uncap_first} = {
        ${assocTable.primaryKey}: data.${assocTable.primaryKey},
        <#list assocTable.columns as col>
        <#if assocTable.primaryKey == col.javaField>
        <#continue>
        </#if>
        ${col.javaField}: data.${col.javaField}<#if column_index != assocTable.columns?size - 1>,</#if>
        </#list>
        }
        this.form.${assTable.insAlias?uncap_first} = _${assTable.insAlias?uncap_first}
        this.form.${assTable.parentColumn.javaField} = data.${assocTable.primaryKey}
        break
      </#list>
      }
      <#else>
      this.form.${column.javaField} = data.${assocTable.primaryKey}
          let _${assocTable.insAlias?uncap_first} = {
      ${assocTable.primaryKey}: data.${assocTable.primaryKey},
      <#list assocTable.columns as col>
      <#if assocTable.primaryKey == col.javaField>
      <#continue>
      </#if>
      ${col.javaField}: data.${col.javaField}<#if column_index != assocTable.columns?size - 1>,</#if>
      </#list>
      }
      this.form.${assocTable.insAlias?uncap_first} = _${assocTable.insAlias?uncap_first}
      </#if>
    },
    </#list>
  }
}
</script>