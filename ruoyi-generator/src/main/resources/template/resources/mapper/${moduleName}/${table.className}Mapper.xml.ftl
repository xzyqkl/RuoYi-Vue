<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<#assign existDelFlag = false>
<mapper namespace="${packageName}.mapper.${table.className}Mapper">
  <resultMap id="BaseResultMap" type="${table.className}">
<#assign columns = table.columns>
<#list columns as col>
  <#if col.columnName == 'del_flag'>
    <#assign existDelFlag = true>
  </#if>
  <#if col.pk>
    <id column="${col.columnName}" property="${col.javaField}"/>
  <#else>
    <result column="${col.columnName}" property="<#if col.javaType.type == "NO">${col.assocTable.insAlias?uncap_first}.${col.assocTable.primaryKey}<#else>${col.javaField}</#if>"<#if col.javaType.type == "NO"> typeHandler="com.ruoyi.common.handler.PrimaryKeyTypeHandler"</#if><#if col.multi || col.htmlType == 'checkbox'> typeHandler="com.ruoyi.common.handler.MyJacksonTypeHandler"</#if>/>
  </#if>
</#list>
  </resultMap>

  <#if table.assocTableList?? && table.assocTableList?size gt 0>
  <sql id="mainTableColumns">
    <#list table.assocTableList as assocTable>
      <#list assocTable.columns as col>
        <#if assocTable_index=table.assocTableList?size-1 && col_index=assocTable.columns?size-1>
    ${assocTable.sqlAlias}.${col.columnName} ${assocTable.sqlAlias}_${col.columnName}
        <#else>
    ${assocTable.sqlAlias}.${col.columnName} ${assocTable.sqlAlias}_${col.columnName},
        </#if>
      </#list>
    </#list>
  </sql>

  <sql id="mainTableLeftJoin">
  <#list table.assocTableList as assocTable>
    LEFT JOIN ${assocTable.tableName} ${assocTable.sqlAlias} ON ${assocTable.sqlAlias}.id = ${table.sqlAlias}.${assocTable.foreignKey}
  </#list>
  </sql>

  </#if>
  <#-- 主表关联 -->
  <resultMap extends="BaseResultMap" id="SearchMap" type="${table.className}">
    <#-- 主表扩展字段 -->
    <#if table.assocTableList?? && table.assocTableList?size gt 0>
        <#list table.assocTableList as assocTable>
            <#list assocTable.columns as col>
      <result column="${assocTable.sqlAlias}_${col.columnName}" property="${assocTable.insAlias?uncap_first}.${col.javaField}"<#if col.javaType.type == "NO"> typeHandler="com.ruoyi.common.handler.PrimaryKeyTypeHandler"</#if><#if col.multi || col.htmlType == 'checkbox'> typeHandler="com.ruoyi.common.handler.MyJacksonTypeHandler"</#if>/>
            </#list>
        </#list>
    </#if>
  </resultMap>
  <#if !(subRecordBySelect = true && table.foreignKey??)>

  <select id="search" parameterType="${table.className}" resultMap="SearchMap">
    <#if !(table.assocTableList?? && table.assocTableList?size gt 0)>
    SELECT
      ${table.sqlAlias}.*
    <#else>
    SELECT
      ${table.sqlAlias}.*,
      <include refid="mainTableColumns"/>
    </#if>
    FROM ${table.tableName} ${table.sqlAlias}
    <#if table.assocTableList?? && table.assocTableList?size gt 0>
    <include refid="mainTableLeftJoin"/>
    </#if>
    WHERE 1 = 1
    <#if existDelFlag>
      AND ${table.sqlAlias}.del_flag = '0'
    </#if>
    <#-- 根据字段的匹配增加检索条件 -->
    <#list table.columns as column>
      <#if column.queryType??>
      <#-- 如果是多选的下拉列表，则需要特殊处理 -->
    <#if column.multi || column.htmlType == 'checkbox'>
    <if test="${column.javaField} != null and ${column.javaField}.length > 0">
      AND
      <foreach collection="${column.javaField}" item="item" open="(" separator=" OR " close=")">
        ${table.sqlAlias}.${column.columnName} like concat('%', ${r'#'}{item}, '%')
      </foreach>
    </if>
      <#else>
      <#-- between 属性有点特殊，需要两个属性字段，需要单独摘出来-->
      <#if column.queryType = 'BETWEEN'>
    <if test="params.begin${column.javaField?cap_first} != null and params.begin${column.javaField?cap_first} != ''">
      AND date_format(${table.sqlAlias}.${column.columnName}, '%y%m%d') &gt;= date_format(${r'#'}{params.begin${column.javaField?cap_first}}, '%y%m%d')
    </if>
    <if test="params.end${column.javaField?cap_first} != null and params.end${column.javaField?cap_first} != ''">
      AND date_format(${table.sqlAlias}.${column.columnName}, '%y%m%d') &lt;= date_format(${r'#'}{params.end${column.javaField?cap_first}}, '%y%m%d')
    </if>
      </#if>
      <#if column.queryType != 'BETWEEN'>
      <#if column.javaType.className != 'String'>
    <if test="${column.javaField} != null">
      <#else>
    <if test="${column.javaField} != null and ${column.javaField} != ''">
      </#if>
      <#if column.queryType = 'EQ'>
      AND ${table.sqlAlias}.${column.columnName} = ${r'#'}{${column.javaField}}
      <#elseif column.queryType = 'NE'>
      AND ${table.sqlAlias}.${column.columnName} != ${r'#'}{${column.javaField}}
      <#elseif column.queryType = 'GT'>
      AND ${table.sqlAlias}.${column.columnName} &gt; ${r'#'}{${column.javaField}}
      <#elseif column.queryType = 'LT'>
      AND ${table.sqlAlias}.${column.columnName} &lt; ${r'#'}{${column.javaField}}
      <#elseif column.queryType = 'GTE'>
      AND ${table.sqlAlias}.${column.columnName} &gt;= ${r'#'}{${column.javaField}}
      <#elseif column.queryType = 'LTE'>
      AND ${table.sqlAlias}.${column.columnName} &lt;= ${r'#'}{${column.javaField}}
      <#elseif column.queryType = 'LIKE'>
      AND ${table.sqlAlias}.${column.columnName} like concat('%', ${r'#'}{${column.javaField}}, '%')
      <#elseif column.queryType = 'RLIKE'>
      AND ${table.sqlAlias}.${column.columnName} like concat(${r'#'}{${column.javaField}} ,'%')
      <#elseif column.queryType = 'LLIKE'>
      AND ${table.sqlAlias}.${column.columnName} like concat('%', ${r'#'}{${column.javaField}})
      </#if>
    </if>
    </#if>
      </#if>
      </#if>
    </#list>
    <#-- 关联表 -->
    <#list table.assocTableList as assocTable>
    <#list assocTable.columns as column>
    <#if column.queryType??>
    <#if column.multi || column.htmlType == 'checkbox'>
    <if test="${assocTable.insAlias?uncap_first} != null and ${assocTable.insAlias?uncap_first}.${column.javaField} != null and ${assocTable.insAlias?uncap_first}.${column.javaField}.length > 0">
      AND
      <foreach collection="${assocTable.insAlias?uncap_first}.${column.javaField}" item="item" open="(" separator=" OR " close=")">
        ${assocTable.sqlAlias}.${column.columnName} like concat('%', ${r'#'}{item}, '%')
      </foreach>
    </if>
    <#else>
    <#if column.queryType != 'BETWEEN'>
    <#if column.javaType.className != 'String'>
    <if test="${assocTable.insAlias?uncap_first} != null and ${assocTable.insAlias?uncap_first}.${column.javaField} != null">
    <#else>
    <if test="${assocTable.insAlias?uncap_first} != null and ${assocTable.insAlias?uncap_first}.${column.javaField} != null and ${assocTable.insAlias?uncap_first}.${column.javaField} != ''">
    </#if>
    <#if column.queryType = 'EQ'>
      AND ${assocTable.sqlAlias}.${column.columnName} = ${r'#'}{${assocTable.insAlias?uncap_first}.${column.javaField}}
    <#elseif column.queryType = 'NE'>
      AND ${assocTable.sqlAlias}.${column.columnName} != ${r'#'}{${assocTable.insAlias?uncap_first}.${column.javaField}}
    <#elseif column.queryType = 'GT'>
      AND ${assocTable.sqlAlias}.${column.columnName} &gt; ${r'#'}{${assocTable.insAlias?uncap_first}.${column.javaField}}
    <#elseif column.queryType = 'LT'>
      AND ${assocTable.sqlAlias}.${column.columnName} &lt; ${r'#'}{${assocTable.insAlias?uncap_first}.${column.javaField}}
    <#elseif column.queryType = 'GTE'>
      AND ${assocTable.sqlAlias}.${column.columnName} &gt;= ${r'#'}{${assocTable.insAlias?uncap_first}.${column.javaField}}
    <#elseif column.queryType = 'LTE'>
      AND ${assocTable.sqlAlias}.${column.columnName} &lt;= ${r'#'}{${assocTable.insAlias?uncap_first}.${column.javaField}}
    <#elseif column.queryType = 'LIKE'>
      AND ${assocTable.sqlAlias}.${column.columnName} like concat('%', ${r'#'}{${assocTable.insAlias?uncap_first}.${column.javaField}}, '%')
    <#elseif column.queryType = 'RLIKE'>
      AND ${assocTable.sqlAlias}.${column.columnName} like concat(${r'#'}{${assocTable.insAlias?uncap_first}.${column.javaField}}, '%')
    <#elseif column.queryType = 'LLIKE'>
      AND ${assocTable.sqlAlias}.${column.columnName} like concat('%', ${r'#'}{${assocTable.insAlias?uncap_first}.${column.javaField}})
    </#if>
    </if>
    </#if>
    </#if>
    </#if>
    </#list>
   </#list>
      ORDER BY ${table.sqlAlias}.create_time desc
  </select>
  </#if>

<#assign subTables=table.subTables>
<#if !(subRecordBySelect = true && table.foreignKey??)>
<#if subTables?size gt 0>
  <#-- 所需关联子表的字段 -->
  <#list subTables as subTab>
  <#assign subColumns=subTab.columns>
  <sql id="${subTab.sqlAlias}Columns">
  <#list subColumns as subCol>
    <#if subCol_index=subColumns?size-1>
    ${subTab.sqlAlias}.${subCol.columnName} ${subTab.sqlAlias}_${subCol.columnName}
      <#else>
    ${subTab.sqlAlias}.${subCol.columnName} ${subTab.sqlAlias}_${subCol.columnName},
    </#if>
  </#list>
  </sql>

  </#list>
  <#-- 所需关联子表的字段 -->
  <sql id="leftJoinSql">
  <#list subTables as subTab>
    LEFT JOIN ${subTab.tableName} ${subTab.sqlAlias} ON ${subTab.sqlAlias}.${subTab.foreignKey} = ${table.sqlAlias}.id<#if existDelFlag> AND ${subTab.sqlAlias}.del_flag = '0'</#if>
  </#list>
  </sql>

</#if>
</#if>
  <resultMap extends="SearchMap" id="SelectOneMap" type="${table.className}">
    <#-- 关联子表：collection -->
  <#if subTables?size gt 0>
    <#list subTables as subTab>
    <collection property="${subTab.insAlias?uncap_first}List" ofType="${packageName}.domain.${subTab.className}"<#if subRecordBySelect = true && table.foreignKey??> column="${table.pkColumn.javaField}" select="${packageName}.mapper.${table.className}Mapper.selectByLinkId"</#if>>
    <#if subRecordBySelect = false>
      <#list subTab.columns as col>
        <#if col.pk>
      <id column="${subTab.sqlAlias}_${col.columnName}" property="${col.javaField}"/>
        <#else>
      <result column="${subTab.sqlAlias}_${col.columnName}" property="<#if col.javaType.type == "NO">${col.assocTable.insAlias?uncap_first}.${col.assocTable.primaryKey}<#else>${col.javaField}</#if>"<#if col.javaType.type == "NO"> typeHandler="com.ruoyi.common.handler.PrimaryKeyTypeHandler"</#if><#if col.multi || col.htmlType == 'checkbox'> typeHandler="com.ruoyi.common.handler.MyJacksonTypeHandler"</#if>/>
        </#if>
      </#list>
    </#if>
    </collection>
    </#list>
  </#if>
  </resultMap>
  <#if !(subRecordBySelect = true && table.foreignKey??)>

  <select id="selectById" parameterType="java.lang.String" resultMap="SelectOneMap">
    <#if (subTables ?? && subTables?size gt 0) || (table.assocTableList?? && table.assocTableList?size gt 0)>
    SELECT
      ${table.sqlAlias}.*,
      <#if table.assocTableList?? && table.assocTableList?size gt 0>
        <#if subTables?? && subTables?size gt 0>
      <include refid="mainTableColumns"/>,
        <#else>
      <include refid="mainTableColumns"/>
        </#if>
      </#if>
      <#list subTables as subTab>
        <#if subTab_index = subTables?size - 1>
      <include refid="${subTab.sqlAlias}Columns"/>
        <#else>
      <include refid="${subTab.sqlAlias}Columns"/>,
        </#if>
      </#list>
    <#else>
    SELECT
      ${table.sqlAlias}.*
    </#if>
    FROM ${table.tableName} ${table.sqlAlias}
    <#if table.assocTableList?? && table.assocTableList?size gt 0>
      <include refid="mainTableLeftJoin"/>
    </#if>
    <#if subTables?? && subTables?size gt 0>
      <include refid="leftJoinSql"/>
    </#if>
    WHERE ${table.sqlAlias}.id = ${r"#"}{id}
  </select>
  </#if>
  <#if subRecordBySelect = true && table.foreignKey??>

  <select id="selectByLinkId" parameterType="java.lang.String" resultMap="SelectOneMap">
    SELECT * FROM ${table.tableName} ${table.sqlAlias} WHERE <#if existDelFlag>${table.sqlAlias}.del_flag = '0' AND </#if>${table.sqlAlias}.${table.foreignKey} = ${r'#'}{linkId}
  </select>
  </#if>
</mapper>