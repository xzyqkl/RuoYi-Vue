<#assign isAutoResMap = false/>
<#list table.columns as col>
    <#if col.multi || col.htmlType == 'checkbox'>
      <#assign isAutoResMap = true/>
      <#break/>
    </#if>
</#list>

<#assign filterFields="createBy,createTime,updateBy,updateTime,remark">
package ${packageName}.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
<#list table.importList as import>
import ${import};
</#list>
<#if table.crud || table.subTables?? && table.subTables?size gt 0>
import com.ruoyi.common.core.domain.BaseEntity;
<#elseif table.tree>
import com.ruoyi.common.core.domain.TreeEntity;
</#if>

/**
 * @Description: ${table.functionName}
 * @Author: ${functionAuthor}
 * @Date: ${.now?string["yyyy-MM-dd"]}
 * @Version: V1.0
 */
<#if table.tree>
<#assign baseEntity="TreeEntity">
<#else>
<#assign baseEntity="BaseEntity">
</#if>
@Data
@Accessors(chain = true)
@TableName(value = "${table.tableName}"<#if isAutoResMap == true>, autoResultMap = true</#if>)
public class ${table.className} extends ${baseEntity} {
<#-- 主键 -->
    /**
     * 主键
     */
    <#if table.pkColumn.increment == true>
    @TableId(type = IdType.AUTO)
        <#else>
    @TableId(type = IdType.ASSIGN_ID)
    </#if>
    private String ${table.pkColumn.javaField};
<#list table.columns as col>
    <#if filterFields?contains(col.javaField) || (col.pk == true)>
        <#continue >
    </#if>

<#-- 处理excel、jsonFormat等 -->
    <#assign comment = ''>
    <#assign parentheseIndex = col.columnComment?index_of("（")>
    <#if parentheseIndex != -1>
        <#assign comment = col.columnComment?substring(0, parentheseIndex)>
    <#else>
        <#assign comment = col.columnComment>
    </#if>
    /**
     * ${col.columnComment}
     */
    <#if col.javaType.className =='Date'>
        <#if col.columnType =='date'>
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "${comment}", width = 30, dateFormat = "yyyy-MM-dd")
        <#elseif col.columnType =='datetime'>
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "${comment}", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
        </#if>
        <#else>
        <#if col.dictType??>
    @Excel(name = "${comment}", dictType = "${col.dictType}")
            <#elseif col.htmlType == 'imageUpload'>
    @Excel(name = "${comment}", cellType = Excel.ColumnType.IMAGE)
            <#elseif col.javaType.type == 'NO'>
    <#assign attr = ""/>
    <#if col.assocTable?? && col.assocTable.columns?size gt 0>
      <#list col.assocTable.columns as assCol>
        <#assign attr = assCol.columnName/>
        <#if assCol.columnName?lower_case?index_of("name") != -1>
            <#break>
        </#if>
      </#list>
    </#if>
    @Excel(name = "${comment}", targetAttr = "${attr}")
            <#else>
            <#if parentheseIndex != -1>
    @Excel(name = "${comment}", readConverterExp = "${col.readConverterExp()}")
            <#else>
    @Excel(name = "${comment}")
            </#if>
        </#if>
    </#if>
    <#-- 如果是多选的，则使用数组 -->
    <#if col.multi || col.htmlType == 'checkbox'>
    @TableField(typeHandler = MyJacksonTypeHandler.class)
    </#if>
    <#if col.javaType.type == "NO">
    <#-- 关联表，获取关联表的主键 -->
    @TableField(value = "${col.columnName}", typeHandler = PrimaryKeyTypeHandler.class)
    </#if>
    private ${col.javaType.className}<#if col.multi || col.htmlType == 'checkbox'>[]</#if> <#if col.javaType.type == "NO">${col.assocTable.insAlias?uncap_first}<#else>${col.javaField}</#if>;
</#list>
    // ------------------------------------- extends -------------------------------------------
<#if table.subTables?? && table.subTables?size gt 0>
    <#list table.subTables as subTab>

        /**
        * ${subTab.functionName}列表
        */
        @TableField(exist = false)
        private List<${subTab.className}> ${subTab.insAlias?uncap_first}List;
    </#list>
</#if>
<#-- 扩展搜索条件 between情况 -->
<#-- 搜索条件使用params参数 -->

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
        <#list table.columns as col>
            <#if col.javaType.type == "NO">
                .append("${col.assocTable.insAlias?uncap_first}", get${col.assocTable.insAlias?cap_first}())
            <#else>
                .append("${col.javaField}", get${col.javaField?cap_first}())
            </#if>
        </#list>
        <#list table.subTables as subTab>
                .append("${subTab.insAlias?uncap_first}List", get${subTab.insAlias?cap_first}List())
        </#list>
                .toString();
    }
}