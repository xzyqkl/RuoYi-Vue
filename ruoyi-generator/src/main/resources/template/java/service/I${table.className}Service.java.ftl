package ${packageName}.service;

import com.ruoyi.common.service.BaseService;
import ${packageName}.domain.${table.className};

import java.util.List;

/**
 * @Description: ${table.functionName} Service接口
 * @Author: ${functionAuthor}
 * @Date: ${.now?string["yyyy-MM-dd"]}
 * @Version: V1.0
 */
public interface I${table.className}Service extends BaseService<${table.className}> {

    /**
     * 分页查询
     *
     * @param ${table.insAlias?uncap_first}
     * @return
     */
    List<${table.className}> search(${table.className} ${table.insAlias?uncap_first});

    /**
     * 新增、修改保存方法
     *
     * @param ${table.insAlias?uncap_first}
     * @param updateAll       当为true时，全量更新
     * @return
     */
    ${table.className} commonSave(${table.className} ${table.insAlias?uncap_first}, Boolean updateAll) throws Exception;

    /**
     * 新增、修改保存方法（按属性不为空修改）
     *
     * @param ${table.insAlias?uncap_first}
     * @return
     */
    ${table.className} commonSave(${table.className} ${table.insAlias?uncap_first}) throws Exception;

    /**
     * 查询单个${table.functionName}记录
     * 若有子记录，则也查询出子记录
     *
     * @param id
     * @return
     */
    ${table.className} selectById(String id);

    /**
     * 删除单个${table.functionName}
     *
     * @param id
     * @return
     */
    boolean removeById(String id);

    /**
     * 删除多个${table.functionName}
     *
     * @param ids
     * @return
     */
    boolean removeByIds(String[] ids);
    <#list table.columns as column>
        <#if column.unique>

    /**
     * 验证${column.javaField}是否唯一
     *
     * @param ${table.insAlias?uncap_first}
     * @return
     */
    boolean validate${column.javaField?cap_first}Unique(${table.className} ${table.insAlias?uncap_first});
        </#if>
    </#list>
}