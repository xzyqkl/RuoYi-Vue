    <#list t1.subTables as subTab>
    @Autowired
    private I${subTab.className}Service ${subTab.insAlias?uncap_first}Service;
    <#assign t1=subTab>
    <#include "autowired.ftl">
    </#list>