package ${packageName}.service.impl;

<#assign needWrapper = false>
<#list table.columns as column >
<#if column.unique>
    <#assign needWrapper = true>
    <#break>
</#if>
</#list>
<#if table.subTables?size gt 0 || needWrapper>
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
</#if>
import com.ruoyi.common.service.AbstractService;
import ${packageName}.mapper.${table.className}Mapper;
import com.ruoyi.common.utils.EntityUtil;
<#if table.subTables?size gt 0>
import com.ruoyi.common.utils.SaveDiffUtil;
</#if>

<#assign hasCode=false>
<#assign hasUnique=false>
<#list table.columns as col>
    <#if col.javaField == "code">
import com.ruoyi.system.service.ISysCodeService;
        <#assign hasCode=true>
        <#break>
    </#if>
</#list>
<#assign t1=table>
<#include "/java/service/impl/import.ftl">
<#list table.columns as column>
    <#if column.unique>
import org.apache.commons.collections4.CollectionUtils;
        <#assign hasUnique = true>
        <#break>
    </#if>
</#list>
<#if hasCode == true || hasUnique == true>
import com.ruoyi.common.utils.StringUtil;
</#if>
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @Description: ${table.functionName} Service接口实现
 * @Author: ${functionAuthor}
 * @Date: ${.now?string["yyyy-MM-dd"]}
 * @Version: V1.0
 */
@Service
public class ${table.className}ServiceImpl extends AbstractService<${table.className}Mapper, ${table.className}> implements I${table.className}Service {
    <#if hasCode == true>
    public static final Integer CODE_LENGTH = 15;
    public static final String PREFIX = "JH";
    </#if>
    @Autowired
    private ${table.className}Mapper ${table.insAlias?uncap_first}Mapper;
    <#assign t1=table>
    <#include "/java/service/impl/autowired.ftl">
    <#if hasCode == true>

    @Autowired
    private ISysCodeService codeService;
    </#if>

    /**
     * 分页查询
     *
     * @param ${table.insAlias?uncap_first}
     * @return
     */
    @Override
    public List<${table.className}> search(${table.className} ${table.insAlias?uncap_first}) {
        return this.${table.insAlias?uncap_first}Mapper.search(${table.insAlias?uncap_first});
    }

    /**
     * 新增、修改保存方法
     *
     * @param ${table.insAlias?uncap_first}
     * @param updateAll       当为true时，全量更新
     * @return
     */
    @Override
    @Transactional
    public ${table.className} commonSave(${table.className} ${table.insAlias?uncap_first}, Boolean updateAll) throws Exception {
        if (updateAll == null) {
            updateAll = false;
        }
        EntityUtil.trimEntity(${table.insAlias?uncap_first});

        // 1. 基础数据处理
        <#if hasCode == true>
        if (updateAll) {
            if (StringUtil.isEmpty(${table.insAlias?uncap_first}.getCode())) {
                // 编号
                String prefix = StringUtil.genCode(PREFIX);
                String code = this.codeService.genCode(prefix, CODE_LENGTH);
                ${table.insAlias?uncap_first}.setCode(code);
            }
        }
        </#if>

        if (${table.insAlias?uncap_first}.getId() == null) {
            this.save(${table.insAlias?uncap_first});
        } else {
            if (updateAll) {
                this.updateAll(${table.insAlias?uncap_first});
            } else {
                this.updateById(${table.insAlias?uncap_first});
            }
        }
        <#if table.subTables?? && table.subTables?size gt 0>

        if (updateAll) {

            <#assign tab=table>
            <#list tab.subTables as subTab>
            // ${subTab.functionName}处理
            for (${subTab.className} ${subTab.insAlias?uncap_first} : ${tab.insAlias?uncap_first}.get${subTab.insAlias?cap_first}List()) {
                EntityUtil.trimEntity(${subTab.insAlias?uncap_first});
                ${subTab.insAlias?uncap_first}.set${subTab.foreignKeyProperty?cap_first}(${tab.insAlias?uncap_first}.getId());
            }

            List<${subTab.className}> form${subTab.insAlias?cap_first}List = ${tab.insAlias?uncap_first}.get${subTab.insAlias?cap_first}List();
            List<${subTab.className}> ori${subTab.insAlias?cap_first}List = this.${subTab.insAlias?uncap_first}Service.list(Wrappers.<${subTab.className}>lambdaQuery()
                .eq(${subTab.className}::get${subTab.foreignKeyProperty?cap_first}, ${tab.insAlias?uncap_first}.get${tab.pkColumn.javaField?cap_first}())
            );
            <#if subTab.uniqueColumnProperty??>
            SaveDiffUtil.updateObject(ori${subTab.insAlias?cap_first}List, form${subTab.insAlias?cap_first}List, "${subTab.uniqueColumnProperty?uncap_first}", "id");
            </#if>
            SaveDiffUtil.saveFormDiff(form${subTab.insAlias?cap_first}List, ori${subTab.insAlias?cap_first}List, ${subTab.className}.class, this.${subTab.insAlias?uncap_first}Service);
            <#-- 递归参数：上级表类名称，本级记录在上级记录列表 -->
            <#assign count=0>
            <#include "/java/service/impl/children.ftl">
            </#list>
        }
        </#if>

        return ${table.insAlias?uncap_first};
    }

    /**
     * 新增、修改保存方法（按属性不为空修改）
     *
     * @param ${table.insAlias?uncap_first}
     * @return
     */
    @Override
    @Transactional
    public ${table.className} commonSave(${table.className} ${table.insAlias?uncap_first}) throws Exception {
        return this.commonSave(${table.insAlias?uncap_first}, false);
    }

    /**
     * 查询单个${table.functionName}
     *
     * @param id
     * @return
     */
    @Override
    public ${table.className} selectById(String id) {
        return this.${table.insAlias?uncap_first}Mapper.selectById(id);
    }

    /**
     * 删除单个${table.functionName}
     *
     * @param id
     * @return
     */
    @Override
    @Transactional
    public boolean removeById(String id) {
        // 删除
        boolean res = super.removeById(id);

        <#if table.subTables?size gt 0>
        if (res) {
            <#list table.subTables as subTab>

            // 删除${subTab.functionName}
            this.${subTab.insAlias?uncap_first}Service.remove(Wrappers.<${subTab.className}>lambdaQuery().eq(${subTab.className}::get${subTab.foreignKeyProperty?cap_first}, id));
            </#list>
        }
        </#if>
        return res;
    }

    /**
     * 删除多个${table.functionName}
     *
     * @param ids
     * @return
     */
    @Override
    @Transactional
    public boolean removeByIds(String[] ids) {
        // 删除
        List<String> idList = Arrays.asList(ids);
        boolean res = super.removeByIds(idList);

        <#if table.subTables?size gt 0>
            if (res) {
            <#list table.subTables as subTab>

                // 删除${subTab.functionName}
                this.${subTab.insAlias?uncap_first}Service.remove(Wrappers.<${subTab.className}>lambdaQuery().in(${subTab.className}::get${subTab.foreignKeyProperty?cap_first}, idList));
            </#list>
            }
        </#if>
        return res;
    }
    <#list table.columns as column>
        <#if column.unique>

    /**
     * 验证${column.javaField}是否唯一
     *
     * @param ${table.insAlias?uncap_first}
     * @return
     */
    public boolean validate${column.javaField?cap_first}Unique(${table.className} ${table.insAlias?uncap_first}) {
        return CollectionUtils.isNotEmpty(this.list(Wrappers.<${table.className}>lambdaQuery()
                .ne(StringUtil.isNotBlank(${table.insAlias?uncap_first}.get${table.pkColumn.javaField?cap_first}()), ${table.className}::get${table.pkColumn.javaField?cap_first}, ${table.insAlias?uncap_first}.get${table.pkColumn.javaField?cap_first}())
                .eq(${table.className}::get${column.javaField?cap_first}, ${table.insAlias?uncap_first}.get${column.javaField?cap_first}())));
    }
        </#if>
    </#list>
}