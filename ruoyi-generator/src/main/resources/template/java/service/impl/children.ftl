            <#assign blandCount = count * 4>
            <#-- 判断是否有grandson -->
            <#if subTab.subTables?? && subTab.subTables?size gt 0>
            <#list subTab.subTables as grandson>

            // ${grandson.functionName}处理
            for (${subTab.className} ${subTab.insAlias?uncap_first} : ${tab.insAlias?uncap_first}.get${subTab.insAlias?cap_first}List()) {
                if (CollectionUtils.isEmpty(${subTab.insAlias?uncap_first}.get${grandson.insAlias?cap_first}List())) {
                    continue;
                }

                for (${grandson.className} ${grandson.insAlias?uncap_first} : ${subTab.insAlias?uncap_first}.get${grandson.insAlias?cap_first}List()) {
                    EntityStringTrimUtil.trimEntity(${grandson.insAlias?uncap_first});
                    ${grandson.insAlias?uncap_first}.set${grandson.foreignKeyProperty?cap_first}(${subTab.insAlias?uncap_first}.getId());
                }

                List<${grandson.className}> form${grandson.insAlias?cap_first}List = ${subTab.insAlias?uncap_first}.get${grandson.insAlias?cap_first}List();
                List<${grandson.className}> ori${grandson.insAlias?cap_first}List = this.${grandson.insAlias?uncap_first}Service.list(Wrappers.<${grandson.className}>lambdaQuery().eq(${grandson.className}::get${grandson.foreignKeyProperty?cap_first}, ${subTab.insAlias?uncap_first}.get${subTab.pkColumn.javaField?cap_first}()));
                <#if grandson.uniqueColumnProperty??>
                SaveDiffUtil.updateObject(ori${grandson.insAlias?cap_first}List, form${grandson.insAlias?cap_first}List, "${grandson.uniqueColumnProperty?uncap_first}", "id");
                </#if>
                SaveDiffUtil.saveFormDiff(form${grandson.insAlias?cap_first}List, ori${grandson.insAlias?cap_first}List, ${grandson.className}.class, this.${grandson.insAlias?uncap_first}Service<#if grandson.updateAll = true>, true</#if>);
                <#assign tab=subTab>
                <#assign count = count + 1>
                <#list tab.subTables as subTab>
                    <@format blank=4><#include "children.ftl"></@format>
                </#list>

            }
            </#list>
            </#if>