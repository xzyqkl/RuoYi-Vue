package ${packageName}.controller;

<#if table.interface>
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.common.utils.StringUtil;
</#if>
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import ${packageName}.domain.${table.className};
import ${packageName}.service.I${table.className}Service;
import com.ruoyi.common.utils.poi.ExcelUtil;
<#if table.tree == false>
import com.ruoyi.common.core.page.TableDataInfo;
<#elseif $table.tree>
</#if>
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Description: ${table.functionName}Controller
 * @Author: ${functionAuthor}
 * @Date: ${.now?string["yyyy-MM-dd"]}
 * @Version: V1.0
 */
@RestController
@RequestMapping("/${moduleName}/${table.businessName}")
public class ${table.className}Controller extends BaseController {
    @Autowired
    private I${table.className}Service ${table.insAlias?uncap_first}Service;

    /**
     * 查询${table.functionName}列表
     */
    @PreAuthorize("@ss.hasPermi('${permissionPrefix}:list')")
    @GetMapping("/list")
<#if table.tree == false>
    public TableDataInfo list(${table.className} ${table.insAlias?uncap_first}) {
        startPage();
        List<${table.className}> list = ${table.insAlias?uncap_first}Service.search(${table.insAlias?uncap_first});
        return getDataTable(list);
    }
<#elseif table.tree == true>
    public AjaxResult list(${table.className} ${table.insAlias?uncap_first}) {
        List<${table.className}> list = ${table.insAlias?uncap_first}Service.search(${table.insAlias?uncap_first});
        return AjaxResult.success(list);
    }
</#if>

    /**
     * 导出${table.functionName}列表
     */
    @PreAuthorize("@ss.hasPermi('${permissionPrefix}:export')")
    @Log(title = "${table.functionName}", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(${table.className} ${table.insAlias?uncap_first}) {
        List<${table.className}> list = ${table.insAlias?uncap_first}Service.search(${table.insAlias?uncap_first});
        ExcelUtil<${table.className}> util = new ExcelUtil<${table.className}>(${table.className}.class);
        return util.exportExcel(list, "${table.businessName}");
    }

    /**
     * 获取${table.functionName}详细信息
     */
    @PreAuthorize("@ss.hasPermi('${permissionPrefix}:query')")
    @GetMapping(value = "/{${table.pkColumn.javaField}}")
    public AjaxResult getInfo(@PathVariable("${table.pkColumn.javaField}") ${table.pkColumn.javaType.className} ${table.pkColumn.javaField}) {
        return AjaxResult.success(${table.insAlias?uncap_first}Service.selectById(${table.pkColumn.javaField}));
    }

    /**
     * 新增${table.functionName}
     */
    @PreAuthorize("@ss.hasPermi('${permissionPrefix}:add')")
    @Log(title = "${table.functionName}", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ${table.className} ${table.insAlias?uncap_first}) {
        try {
            <#list table.columns as column>
                <#if column.unique>
            if (this.${table.insAlias?uncap_first}Service.validate${column.javaField?cap_first}Unique(${table.insAlias?uncap_first})) {
                return error("${table.tableComment}${column.columnComment}已存在");
            }
                </#if>
            </#list>
            return toAjax(${table.insAlias?uncap_first}Service.commonSave(${table.insAlias?uncap_first}, true) != null);
        } catch (Exception e) {
            e.printStackTrace();
            return error(e.getMessage());
        }
    }

    /**
     * 修改${table.functionName}
     */
    @PreAuthorize("@ss.hasPermi('${permissionPrefix}:edit')")
    @Log(title = "${table.functionName}", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ${table.className} ${table.insAlias?uncap_first}) {
        try {
            <#list table.columns as column>
                <#if column.unique>
            if (this.${table.insAlias?uncap_first}Service.validate${column.javaField?cap_first}Unique(${table.insAlias?uncap_first})) {
                return error("${table.tableComment}${column.columnComment}已存在");
            }
                </#if>
            </#list>
            return toAjax(${table.insAlias?uncap_first}Service.commonSave(${table.insAlias?uncap_first}, true) != null);
        } catch (Exception e) {
            e.printStackTrace();
            return error(e.getMessage());
        }
    }

    /**
     * 删除${table.functionName}
     */
    @PreAuthorize("@ss.hasPermi('${permissionPrefix}:remove')")
    @Log(title = "${table.functionName}", businessType = BusinessType.DELETE)
    @DeleteMapping("/{${table.pkColumn.javaField}s}")
    public AjaxResult remove(@PathVariable ${table.pkColumn.javaType.className}[] ${table.pkColumn.javaField}s) {
        return toAjax(${table.insAlias?uncap_first}Service.removeByIds(${table.pkColumn.javaField}s));
    }
    <#if table.interface>

    /**
    * ${table.functionName}接口。
    * 对外提供接口，不做权限限制，但是只能显示部分信息
    *
    * @param ${table.insAlias?uncap_first}
    * @return
    */
    @GetMapping("/data")
    public TableDataInfo get${table.insAlias?cap_first}Data(${table.className} ${table.insAlias?uncap_first}) {
        startPage();
        List<${table.className}> list = ${table.insAlias?uncap_first}Service.list(Wrappers.<${table.className}>lambdaQuery()
        <#list table.columns as column>
            <#if column.queryType??>
                <#if column.queryType == 'LIKE'>
                    <#if column.javaType.className == 'String'>
            .like(StringUtil.isNotEmpty(${table.insAlias?uncap_first}.get${column.javaField?cap_first}()), ${table.className}::get${column.javaField?cap_first}, ${table.insAlias?uncap_first}.get${column.javaField?cap_first}())
                    </#if>
            .like(${table.insAlias?uncap_first}.get${column.javaField?cap_first}() != null, ${table.className}::get${column.javaField?cap_first}, ${table.insAlias?uncap_first}.get${column.javaField?cap_first}())
                    <#elseif column.queryType == 'EQ'>
                </#if>
            </#if>
        </#list>
        <#-- 需要获取的字段 -->
        <#assign fields = ''/>
        <#list table.columns as column>
            <#if fields == ''>
                <#assign fields = '${table.className}::get${column.javaField?cap_first}'/>
                <#else>
                <#assign fields = fields + ', ${table.className}::get${column.javaField?cap_first}'/>
            </#if>
        </#list>
            .select(${fields}));

        return getDataTable(list);
    }
    </#if>
}
