package ${packageName}.mapper;

import com.ruoyi.common.mapper.CommonMapper;
import ${packageName}.domain.${table.className};
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Description: ${table.functionName}Mapper接口
 * @Author: ${functionAuthor}
 * @Date: ${.now?string["yyyy-MM-dd"]}
 * @Version: V1.0
 */
@Mapper
public interface ${table.className}Mapper extends CommonMapper<${table.className}> {
    <#-- 最后一个表不做数据获取，在倒数第二个表获取数据时级联获取，可以减少数据库交互-->
    /**
     * 用于分页查询
     *
     * @param ${table.insAlias?uncap_first}
     * @return
     */
    List<${table.className}> search(${table.className} ${table.insAlias?uncap_first});

    /**
     * 用于查询单条记录
     *
     * @param id
     * @return
     */
    ${table.className} selectById(String id);
    <#if subRecordBySelect = true && table.foreignKey??>

    /**
     * 用于关联查询
     *
     * @param linkId
     * @return
     */
    List<${table.className}> selectByLinkId(String linkId);
    </#if>
}