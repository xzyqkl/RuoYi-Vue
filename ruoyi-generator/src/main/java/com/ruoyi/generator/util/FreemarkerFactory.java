package com.ruoyi.generator.util;

import com.ruoyi.generator.config.FormatDirective;
import freemarker.template.Configuration;

/**
 * FreemarkerEngine工厂
 *
 * @author ruoyi
 */
public class FreemarkerFactory {
    /**
     * 初始化vm方法
     */
    public static Configuration getConfig() {
        Configuration cfg = null;
        try {
            cfg = new Configuration(Configuration.VERSION_2_3_28);
            cfg.setSharedVariable(FormatDirective.DIRECTIVE_NAME, new FormatDirective());
            cfg.setClassForTemplateLoading(FreemarkerFactory.class, "/template/");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return cfg;
    }
}
