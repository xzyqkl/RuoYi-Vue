package com.ruoyi.generator.util;

import com.ruoyi.generator.config.FormatDirective;
import com.ruoyi.generator.domain.GenTable;
import com.ruoyi.generator.domain.Settings;
import com.ruoyi.generator.domain.enums.TplCategory;
import freemarker.cache.FileTemplateLoader;
import freemarker.cache.MultiTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class FreeMarkerUtil {
    /**
     * 项目空间路径
     */
    private static final String PROJECT_PATH = "main/java";

    /**
     * mybatis空间路径
     */
    private static final String MYBATIS_PATH = "main/resources/mapper";

    /**
     * html空间路径
     */
    private static final String TEMPLATES_PATH = "main/resources/templates";

    /**
     * 默认上级菜单，系统工具
     */
    public static final String DEFAULT_PARENT_MENU_ID = "3";

    /**
     * @param settings
     * @param table
     * @param root     是否跟节点，只有跟节点生成Controller
     * @return 路径，内容
     * @throws IOException
     * @throws TemplateException
     */
    public static Map<String, String> genCode(final Settings settings, GenTable table, boolean root, boolean isSelect) throws IOException {
        Map<String, String> mapRes = new LinkedHashMap<>();
        // 先处理子记录
        if (CollectionUtils.isNotEmpty(table.getSubTables())) {
            for (GenTable t : table.getSubTables()) {
                Map<String, String> subMap = genCode(settings, t, false, isSelect);
                mapRes.putAll(subMap);
            }
        }

        settings.setTable(table);

        //1.创建配置实例Cofiguraion
        Configuration cfg = FreemarkerFactory.getConfig();

        // 2. 获取模板
        List<String> templateList = getTemplateList(root, table.getTplCategory(), isSelect);
        for (String templateStr : templateList) {
            String fileName = FreeMarkerUtil.processTemplate(templateStr.replace("#", "?").replace("@", "$").replace("!", ""), settings, cfg).replace(".ftl", "");

            // 解析模板
            Template template = cfg.getTemplate(templateStr);
            mapRes.put(fileName, processTemplate(template, settings));
        }

        return mapRes;
    }

    public static Configuration getConfig(List<File> paramList, String paramString1) throws IOException {
        Configuration configuration = new Configuration(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);
        configuration.setSharedVariable(FormatDirective.DIRECTIVE_NAME, new FormatDirective());
        configuration.setClassForTemplateLoading(FreemarkerFactory.class, "/template/");

        FileTemplateLoader[] arrayOfFileTemplateLoader = new FileTemplateLoader[paramList.size()];
        for (int i = 0; i < paramList.size(); i++) {
            File file = paramList.get(i);
            arrayOfFileTemplateLoader[i] = new FileTemplateLoader(file);
        }
        MultiTemplateLoader multiTemplateLoader = new MultiTemplateLoader(arrayOfFileTemplateLoader);
        configuration.setTemplateLoader(multiTemplateLoader);
        configuration.setNumberFormat("###############");
        configuration.setBooleanFormat("true,false");
        configuration.setDefaultEncoding(paramString1);
        return configuration;
    }

    public static String processTemplate(String templateStr, Settings paramMap, Configuration config) {
        StringWriter stringWriter = new StringWriter();
        try {
            Template template = new Template("myTempName", new StringReader(templateStr), config);
            template.process(paramMap, stringWriter);
            return stringWriter.toString();
        } catch (Exception exception) {
            throw new IllegalStateException("cannot process templateString:" + templateStr + " cause:" + exception, exception);
        }
    }

    public static String processTemplate(Template template, Settings paramMap) {
        StringWriter stringWriter = new StringWriter();
        try {
            template.process(paramMap, stringWriter);
            return stringWriter.toString();
        } catch (Exception exception) {
            throw new IllegalStateException("cannot process templateString:" + template.getName() + " cause:" + exception, exception);
        }
    }

    public static String processTemplate(File file, Settings paramMap, Configuration config) {
        StringWriter stringWriter = new StringWriter();
        try {
            FileReader reader = new FileReader(file);
            Template template = new Template("myTempName", reader, config);
            template.process(paramMap, stringWriter);
            return stringWriter.toString();
        } catch (Exception exception) {
            throw new IllegalStateException("cannot process templateString:" + file.getName() + " cause:" + exception, exception);
        }
    }

    public static void processTemplate(Object date, Template template, String outFile, String charsetName) throws IOException, TemplateException
    {
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile), charsetName));
        template.process(date, bufferedWriter);
        bufferedWriter.close();
    }

    private static List<String> getTemplateList(boolean root, TplCategory tplCategory, boolean isSelect) {
        List<String> templates = new ArrayList<>();
        if (!isSelect) {
            templates.add("java/domain/${table.className}.java.ftl");
            templates.add("java/mapper/${table.className}Mapper.java.ftl");
            templates.add("resources/mapper/${moduleName}/${table.className}Mapper.xml.ftl");
            templates.add("java/service/I${table.className}Service.java.ftl");
            templates.add("java/service/impl/${table.className}ServiceImpl.java.ftl");
            if (root) {
                templates.add("java/controller/${table.className}Controller.java.ftl");
                templates.add("vue/api/${moduleName}/${table.businessName}.js.ftl");
                templates.add("sql/sql.ftl");
            }
            switch (tplCategory) {
                case SINGLE:
                    templates.add("vue/views/${moduleName}/${businessName}/!index.vue.ftl");
                    break;
                case PRIMARY:
                    templates.add("vue/views/${moduleName}/${businessName}/index.vue.ftl");
                    templates.add("vue/views/${moduleName}/${businessName}/${table.insAlias#cap_first}Form.vue.ftl");
                    break;
                case ATTACHED:
                    templates.add("vue/views/${moduleName}/${businessName}/@{table.insAlias#cap_first}Form.vue.ftl");
                    break;
                case TREE:
                    templates.add("resources/templates/${moduleName}/${businessName}/tree.html.ftl");
                    templates.add("resources/templates/${moduleName}/${businessName}/list-tree.html.ftl");
                    break;
            }
        } else {
            // 子记录，仅生成select对话框
            templates.add("java/controller/${table.className}Controller.java.ftl"); // 新增接口
            templates.add("vue/api/${moduleName}/${table.businessName}.js.ftl"); // 接口js
            templates.add("vue/views/${moduleName}/${businessName}/${table.insAlias#cap_first}Select.vue.ftl");
        }

        return templates;
    }
}
