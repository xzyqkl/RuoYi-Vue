package com.ruoyi.generator.util;

import com.ruoyi.common.constant.GenConstants;
import com.ruoyi.common.enums.YesOrNo;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.StringUtil;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.generator.domain.GenAssocTable;
import com.ruoyi.generator.domain.GenTable;
import com.ruoyi.generator.domain.GenTableColumn;
import com.ruoyi.generator.domain.Settings;
import com.ruoyi.generator.domain.enums.QueryType;
import org.apache.commons.collections.CollectionUtils;

import java.util.*;

public class DealFieldUtil {
    private static int getLevel(GenTable table, int level) {
        int maxLevel = level;
        if (CollectionUtils.isNotEmpty(table.getSubTables())) {
            for (GenTable tab : table.getSubTables()) {
                maxLevel = level + 1;
                int l = getLevel(tab, level + 1);

                if (l > maxLevel) {
                    maxLevel = l;
                }
            }
        }

        return maxLevel;
    }

    public static void dealField(Settings settings, String parentTableName, GenTable table) {
        // 当table的层级超过2级，就要默认将subRecordBySelect打开
        if (!settings.isSubRecordBySelect()) {
            int level = getLevel(table, 1);
            if (level > 2) {
                settings.setSubRecordBySelect(true);
            }
        }

        if (CollectionUtils.isNotEmpty(table.getSubTables())) {
            for (GenTable subTable : table.getSubTables()) {
                dealField(settings, table.getTableName(), subTable);
            }
        }

        //region 处理table属性
        if (StringUtils.isEmpty(table.getTableName())) {
            throw new ServiceException("表名称不可为空");
        }

        if (StringUtils.isEmpty(table.getClassName())) {
            table.setClassName(StringUtils.convertToCamelCase(table.getTableName()));
        }

        if (StringUtils.isEmpty(table.getInsAlias())) {
            table.setInsAlias(StringUtils.toCamelCase(table.getClassName()));
        }

        // sql查询的别名
        table.setSqlAlias(StringUtil.getFirstChar(table.getTableName(), "_"));
        if (StringUtils.isNotEmpty(parentTableName) && StringUtils.isEmpty(table.getForeignKey())) {
            throw new ServiceException(String.format("子表(%s)外键不可为空", table.getTableName()));
        }

        if (StringUtils.isNotEmpty(table.getForeignKey())) {
            table.setForeignKeyProperty(StringUtils.toCamelCase(table.getForeignKey()));
        }

        if (StringUtils.isNotEmpty(table.getUniqueColumn())) {
            table.setUniqueColumnProperty(StringUtils.toCamelCase(table.getUniqueColumn()));
        }

        //endregion

        //region 处理column
        Map<String, GenAssocTable> assocTableMap = new HashMap<>();
        for (GenAssocTable assocTable : table.getAssocTableList()) {
            assocTableMap.put(assocTable.getForeignKey(), assocTable);
        }
        for (GenTableColumn column : table.getColumns()) {
            if (assocTableMap.containsKey(column.getColumnName())) {
                column.setAssocTable(assocTableMap.get(column.getColumnName()));
                // 设置关联表的数据接口
                assocTableMap.get(column.getColumnName()).setDataUrl(column.getJavaType().getDataUrl());
            }
        }
        //endregion

        //region 处理关联表(有可能同一个表关联了多次，需要处理的，1. sql 2. 页面选择框)
        Map<String, Integer> assocTableTotalCountMap = new HashMap<>(); // 表名称，出现次数
        if (CollectionUtils.isNotEmpty(table.getAssocTableList())) {
            for (GenAssocTable assocTable : table.getAssocTableList()) {
                Integer count = 1;
                if (assocTableTotalCountMap.containsKey(assocTable.getTableName())) {
                    count = assocTableTotalCountMap.get(assocTable.getTableName()) + 1;
                }

                assocTableTotalCountMap.put(assocTable.getTableName(), count);
            }
        }
        Map<String, Integer> assocTableCountMap = new HashMap<>(); // 表名称，出现次数
        if (CollectionUtils.isNotEmpty(table.getAssocTableList())) {
            Iterator<GenAssocTable> it = table.getAssocTableList().iterator();
            while (it.hasNext()) {
                GenAssocTable assocTable = it.next();
                if (StringUtils.isEmpty(assocTable.getPackageName())) {
                    assocTable.setPackageName(settings.getPackageName() + "." + settings.getBusinessName() + ".domain");
                }
                if (CollectionUtils.isEmpty(assocTable.getColumns())) {
                    throw new ServiceException(assocTable.getTableName() + "未设置关联字段");
                }

                if (assocTableCountMap.get(assocTable.getTableName()) == null) {
                    assocTableCountMap.put(assocTable.getTableName(), 1);
                } else {
                    Integer count = assocTableCountMap.get(assocTable.getTableName());
                    assocTableCountMap.put(assocTable.getTableName(), count + 1);
                }

                if (StringUtils.isEmpty(assocTable.getClassName())) {
                    assocTable.setClassName(StringUtils.convertToCamelCase(assocTable.getTableName()));
                }

//                // 处理insAlias （手工填写的，不需要处理）
//                if (StringUtils.isEmpty(assocTable.getInsAlias())) {
//                    assocTable.setInsAlias(StringUtils.toCamelCase(assocTable.getTableName()) + assocTableCountMap.get(assocTable.getTableName()));
//                }

                if (StringUtils.isEmpty(assocTable.getForeignKey())) {
                    throw new ServiceException(String.format("关联表(%s)外键不可为空", assocTable.getTableName()));
                }

                if (StringUtils.isNotEmpty(assocTable.getForeignKey())) {
                    assocTable.setForeignKeyProperty(StringUtils.toCamelCase(assocTable.getForeignKey()));
                }

                // 处理sqlAlias
                Integer appearCount = assocTableTotalCountMap.get(assocTable.getTableName());
                String sqlAlias = StringUtil.getFirstChar(assocTable.getTableName(), "_");
                if (appearCount > 1) {
                    sqlAlias += assocTableCountMap.get(assocTable.getTableName());
                }
                assocTable.setSqlAlias(sqlAlias);
                assocTable.setAppearCount(appearCount);
            }
        }
        //endregion

        // 处理需要引入的包
        table.setImportList(getImportList(table));
    }

    /**
     * 根据列类型获取导入包
     *
     * @param genTable 业务表对象
     * @return 返回需要导入的包列表
     */
    private static List<String> getImportList(GenTable genTable) {
        // 基础包
        List<GenTableColumn> columns = genTable.getColumns();
        Set<String> importList = new HashSet<>();
        if (CollectionUtils.isNotEmpty(genTable.getSubTables())) {
            importList.add("java.util.List");
        }
        // 是否有需要标识非数据库字段的。有查询（并且是between的）、子表不为空，有关联表时，都需要引入TableField
        boolean needSearch = false;
        boolean isMulti = false;
        for (GenTableColumn column : columns) {
            if (column.isSuperColumn()) {
                continue;
            }
            if (column.isMulti() && !isMulti) {
                isMulti = true;
            }
//            if (column.isUnique()) {
//                importList.add("org.apache.commons.collections4.CollectionUtils");
//                importList.add("com.ruoyi.common.utils.StringUtil");
//            }
            if (!needSearch && column.getQueryType() != null && column.getQueryType() == QueryType.BETWEEN) {
                needSearch = true;
            }
            String columnPackage = column.getJavaType().getPackageName();
            if (StringUtil.isNotEmpty(columnPackage) && columnPackage.indexOf(genTable.getPackageName()) == -1) {
                importList.add(column.getJavaType().getPackageName());
            }
            if (GenConstants.TYPE_DATE.equals(column.getJavaType().getClassName())) {
                importList.add("com.fasterxml.jackson.annotation.JsonFormat");
                importList.add("org.springframework.format.annotation.DateTimeFormat");
            }
            if (column.getJavaType().getType() == YesOrNo.NO) {
                importList.add("com.ruoyi.common.handler.PrimaryKeyTypeHandler");
            }
        }

        if (needSearch || CollectionUtils.isNotEmpty(genTable.getSubTables()) || CollectionUtils.isNotEmpty(genTable.getAssocTableList())) {
            importList.add("com.baomidou.mybatisplus.annotation.TableField");
        }

        if (isMulti) {
            importList.add("com.baomidou.mybatisplus.annotation.TableField");
            importList.add("com.ruoyi.common.handler.MyJacksonTypeHandler");
        }

//        // 子记录 在同一个包下，不需要引入
//        if (CollectionUtils.isNotEmpty(genTable.getSubTables())) {
//            for (GenTable subTable : genTable.getSubTables()) {
//                importList.add(subTable.getPackageName() + ".domain."+ genTable.getClassName());
//            }
//        }
//        // 关联记录
//        if (CollectionUtils.isNotEmpty(genTable.getAssocTableList())) {
//            for (GenAssocTable assocTable : genTable.getAssocTableList()) {
//                if (StringUtil.isNotEmpty(assocTable.getPackageName()) && !assocTable.getPackageName().equals(genTable.getPackageName())) {
//                    importList.add(assocTable.getPackageName() + genTable.getClassName());
//                }
//            }
//        }

        // 排个序
        List<String> list = new ArrayList<>();
        list.addAll(importList);
        list.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        });
        return list;
    }
}
