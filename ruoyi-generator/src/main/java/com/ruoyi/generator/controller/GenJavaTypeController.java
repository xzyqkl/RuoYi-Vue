package com.ruoyi.generator.controller;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtil;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.generator.domain.GenJavaType;
import com.ruoyi.generator.service.IGenJavaTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Description: java类型Controller
 * @Author: warden
 * @Date: 2021-02-27
 * @Version: V1.0
 */
@RestController
@RequestMapping("/tool/type")
public class GenJavaTypeController extends BaseController
{
    @Autowired
    private IGenJavaTypeService javaTypeService;

    /**
     * 查询java类型列表
     */
    @PreAuthorize("@ss.hasPermi('tool:type:list')")
    @GetMapping("/list")
    public TableDataInfo list(GenJavaType javaType) {
        startPage();
        List<GenJavaType> list = javaTypeService.search(javaType);
        return getDataTable(list);
    }

    /**
     * 导出java类型列表
     */
    @PreAuthorize("@ss.hasPermi('tool:type:export')")
    @Log(title = "java类型", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(GenJavaType javaType) {
        List<GenJavaType> list = javaTypeService.search(javaType);
        ExcelUtil<GenJavaType> util = new ExcelUtil<GenJavaType>(GenJavaType.class);
        return util.exportExcel(list, "type");
    }

    /**
     * 获取java类型详细信息
     */
    @PreAuthorize("@ss.hasPermi('tool:type:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id) {
        return AjaxResult.success(javaTypeService.selectById(id));
    }

    /**
     * 新增java类型
     */
    @PreAuthorize("@ss.hasPermi('tool:type:add')")
    @Log(title = "java类型", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody GenJavaType javaType) {
        try {
            List<GenJavaType> javaTypes = this.javaTypeService.list(Wrappers.<GenJavaType>lambdaQuery()
                    .ne(StringUtil.isNotEmpty(javaType.getId()), GenJavaType::getId, javaType.getId())
                    .eq(GenJavaType::getClassName, javaType.getClassName().trim())
            );
            if (CollectionUtils.isNotEmpty(javaTypes)) {
                return AjaxResult.error("类名已存在");
            }
            return toAjax(javaTypeService.commonSave(javaType, true) != null);
        } catch (Exception e) {
            e.printStackTrace();
            return toAjax(false);
        }
    }

    /**
     * 修改java类型
     */
    @PreAuthorize("@ss.hasPermi('tool:type:edit')")
    @Log(title = "java类型", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody GenJavaType javaType) {
        try {
            List<GenJavaType> javaTypes = this.javaTypeService.list(Wrappers.<GenJavaType>lambdaQuery()
                    .ne(StringUtil.isNotEmpty(javaType.getId()), GenJavaType::getId, javaType.getId())
                    .eq(GenJavaType::getClassName, javaType.getClassName().trim())
            );
            if (CollectionUtils.isNotEmpty(javaTypes)) {
                return AjaxResult.error("类名已存在");
            }
            return toAjax(javaTypeService.commonSave(javaType, true) != null);
        } catch (Exception e) {
            e.printStackTrace();
            return toAjax(false);
        }
    }

    /**
     * 删除java类型
     */
    @PreAuthorize("@ss.hasPermi('tool:type:remove')")
    @Log(title = "java类型", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids) {
        return toAjax(javaTypeService.removeByIds(ids));
    }

    /**
     * 查询java类型列表
     */
    @GetMapping("/listall")
    public AjaxResult listAll(GenJavaType javaType) {
        List<GenJavaType> list = javaTypeService.search(javaType);
        return AjaxResult.success(list);
    }
}
