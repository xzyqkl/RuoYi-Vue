package com.ruoyi.generator.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.ruoyi.common.constant.GenConstants;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.handler.MyJacksonTypeHandler;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.generator.domain.enums.GenType;
import com.ruoyi.generator.domain.enums.TplCategory;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.ArrayUtils;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

/**
 * 业务表 gen_table
 *
 * @author ruoyi
 */
@Data
@Accessors(chain = true)
public class GenTable extends BaseEntity
{

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /** 表名称 */
    @NotBlank(message = "表名称不能为空")
    private String tableName;

    /** 表描述 */
    @NotBlank(message = "表描述不能为空")
    private String tableComment;

    /**
     * 关联父表的表名
     */
    private String parentName;

    /**
     * 关联主表的外键
     */
    private String foreignKey;

    /**
     * 实体类名称(首字母大写)
     */
    @NotBlank(message = "实体类名称不能为空")
    private String className;

    /**
     * 实例别名。默认为className
     * 例如：BaseSupplier supplier(该名称）
     */
    private String insAlias;

    /**
     * Form的列数
     */
    private Integer columnSize;

    /**
     * 使用的模板（crud单表操作 tree树表操作 sub主子表操作）
     */
    private TplCategory tplCategory;

    /**
     * 生成包路径
     */
//    @NotBlank(message = "生成包路径不能为空")
    private String packageName;

    /**
     * 生成模块名
     * 例如：packageName: com.ruoyi.livestock
     * 模块名为：
     *
     */
//    @NotBlank(message = "生成模块名不能为空")
    private String moduleName;

    /**
     * 生成业务名
     */
//    @NotBlank(message = "生成业务名不能为空")
    private String businessName;

    /**
     * 生成功能名
     */
    @NotBlank(message = "生成功能名不能为空")
    private String functionName;

    /**
     * 生成作者
     */
//    @NotBlank(message = "作者不能为空")
    private String functionAuthor;

    /**
     * 生成代码方式（0zip压缩包 1自定义路径） 附表不需要设置
     */
    private GenType genType;

    /**
     * 生成路径（不填默认项目路径） 附表不需要设置
     */
    private String genPath;

    /**
     * 其它生成选项
     */
    @TableField(typeHandler = MyJacksonTypeHandler.class)
    private Options options = new Options();

    // ------------------------------- 扩展字段 --------------------------------
    /**
     * 别名。后台根据表名称处理，表名首字母的缩写（小写)
     */
    @TableField(exist = false)
    private String sqlAlias;

    /**
     * 外键属性。后台根据表名称处理
     */
    @TableField(exist = false)
    private String foreignKeyProperty;

    //region 子记录处理
    /**
     * 记录唯一属性，当不为空时，将根据该数据判断是否数据库原有记录
     * 用于填充已有记录id。例如：当设备列表删除了A设备，又把A设备添加回来，此时应该更新，而不是删除后添加
     */
    @TableField(exist = false)
    private String uniqueColumn;

    /**
     * 根据uniqueColumn 自动生成属性名称
     */
    @TableField(exist = false)
    private String uniqueColumnProperty;

    /**
     * 是否全量更新子记录
     */
    @TableField(exist = false)
    private boolean updateAll = false;
    //endregion

    /**
     * 表主键
     */
    @TableField(exist = false)
    private GenTableColumn pkColumn;

    /**
     * 用于设置关联其他表获取属性
     * 例如：需要关联SysUser表
     */
    @TableField(exist = false)
    private List<GenAssocTable> assocTableList = new ArrayList<>();

    /**
     * 子表列表
     */
    @TableField(exist = false)
    private List<GenTable> subTables = new ArrayList<>();

    /**
     * 表列信息
     */
    @Valid
    @TableField(exist = false)
    private List<GenTableColumn> columns = new ArrayList<>();

    /**
     * 要导入的包列表
     */
    @TableField(exist = false)
    private List<String> importList = new ArrayList<>();

    /**
     * 表出现次数（用于前端） 数据来自于AssocTable
     */
    @TableField(exist = false)
    private Integer appearCount;

    /**
     * 父关联表
     */
    @TableField(exist = false)
    private GenTable parentTable;

    /**
     * 子表信息
     */
    @TableField(exist = false)
    @Deprecated
    private GenTable subTable;

    /**
     * 是否接口，是否数据接口
     */
    @TableField(exist = false)
    private boolean isInterface = false;

    public boolean isSub() {
        return this.tplCategory == TplCategory.ATTACHED;
    }

    public boolean isTree() {
        return this.tplCategory == TplCategory.TREE || this.tplCategory == TplCategory.TREE_TABLE;
    }

    public static boolean isTree(TplCategory tplCategory) {
        return tplCategory == TplCategory.TREE || tplCategory == TplCategory.TREE_TABLE;
    }

    public boolean isCrud() {
        return isCrud(this.tplCategory);
    }

    public static boolean isCrud(TplCategory tplCategory) {
        return tplCategory == TplCategory.SINGLE || tplCategory == TplCategory.PRIMARY || tplCategory == TplCategory.ATTACHED;
    }

    public boolean isSuperColumn(String javaField) {
        return isSuperColumn(this.tplCategory, javaField);
    }

    public static boolean isSuperColumn(TplCategory tplCategory, String javaField) {
        if (isTree(tplCategory)) {
            return StringUtils.equalsAnyIgnoreCase(javaField,
                    ArrayUtils.addAll(GenConstants.TREE_ENTITY, GenConstants.BASE_ENTITY));
        }
        return StringUtils.equalsAnyIgnoreCase(javaField, GenConstants.BASE_ENTITY);
    }

    @Override
    public String toString() {
        return "GenTable{" +
                "tableName='" + tableName + '\'' +
                '}';
    }
}