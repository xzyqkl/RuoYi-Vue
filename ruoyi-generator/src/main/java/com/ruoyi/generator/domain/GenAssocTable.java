package com.ruoyi.generator.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: 代码生成业务
 * @Author: warden
 * @Date: 2021-02-17
 * @Version: V1.0
 */
@Data
@Accessors(chain = true)
@TableName(value = "gen_assoc_table")
public class GenAssocTable extends BaseEntity
{

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 主表ID
     */
    private String tableId;

    /**
     * 表名称
     */
    private String tableName;

    /**
     * 生成功能名
     */
    private String functionName;

    /**
     * 生成包路径
     */
    private String packageName;

    /**
     * 实体类名称
     */
    private String className;

    /**
     * 实体类别名
     */
    private String insAlias;

    /**
     * 关联当前表的外键名称
     */
    private String foreignKey;

    /**
     * 当前关联表主键
     */
    private String primaryKey = "id";
    // ---------------------------------- 后台处理的属性 ---------------------------------
    /**
     * 关联字段
     */
    @TableField(exist = false)
    private List<GenAssocTableColumn> columns = new ArrayList<>();

    /**
     * 外键属性。后台根据表名称处理
     */
    @TableField(exist = false)
    private String foreignKeyProperty;

    /**
     * 表别名。后台自动获取
     */
    @TableField(exist = false)
    private String sqlAlias;

    /**
     * 表出现次数（用于前端）
     */
    @TableField(exist = false)
    private Integer appearCount;

    /**
     * 用于生成前端选择框。当被关联表需要生成选择框时有效
     */
    @TableField(exist = false)
    private GenTable referenceTable;

    /**
     * 关联的父表字段
     */
    @TableField(exist = false)
    private GenTableColumn parentColumn;

    /**
     * 数据接口
     */
    @TableField(exist = false)
    private String dataUrl;

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("linkId", getTableId())
                .append("tableName", getTableName())
                .toString();
    }
}