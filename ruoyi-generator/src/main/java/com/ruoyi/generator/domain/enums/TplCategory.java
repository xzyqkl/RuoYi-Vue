package com.ruoyi.generator.domain.enums;

import com.fasterxml.jackson.annotation.JsonValue;
import com.ruoyi.common.IBaseEnum;
import lombok.Getter;

/**
 * 代码生成模板
 */
@Getter
public enum TplCategory implements IBaseEnum<String>
{
    SINGLE("1", "单表"), PRIMARY("2", "主表"), ATTACHED("3", "附表"),
    TREE("4", "树"), TREE_TABLE("5", "树表");

    @JsonValue
    private String code;
    private String description;

    TplCategory(String code, String description) {
        this.code = code;
        this.description = description;
    }

    @Override
    public String getValue() {
        return this.code;
    }
}
