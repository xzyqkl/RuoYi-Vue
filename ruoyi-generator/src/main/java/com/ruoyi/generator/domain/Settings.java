package com.ruoyi.generator.domain;

import com.ruoyi.generator.domain.enums.GenType;
import lombok.Data;

@Data
public class Settings {
    /**
     * 生成方式
     */
    private GenType genType;

    /**
     * 作者
     */
    private String functionAuthor;

    /**
     * 包名称。fun.warden.system
     * 包名最后一个为模块名称
     */
    private String packageName;

    /**
     * 模块名称
     * 包名称。fun.warden.system.role
     * 包名最后一个为模块名称
     */
    private String moduleName;

    /**
     * 业务模块名称
     * 例如：包为：fun.warden.system
     * 业务为：user
     */
    private String businessName;

    /**
     * 权限前缀 模块：业务：？
     */
    private String permissionPrefix;

    /**
     * 父菜单
     */
    private String parentMenuId;

    /**
     * form列数
     */
    private Integer columnSize;

    /**
     * 表
     */
    private GenTable table;

    /**
     * 标识子表记录是否通过select子查询获取
     */
    private boolean subRecordBySelect = false;
}
