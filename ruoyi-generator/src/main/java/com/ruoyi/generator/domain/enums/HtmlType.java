package com.ruoyi.generator.domain.enums;

import com.fasterxml.jackson.annotation.JsonValue;
import com.ruoyi.common.IBaseEnum;
import lombok.Getter;

/**
 * html类型
 */
@Getter
public enum HtmlType implements IBaseEnum<String>
{
    input("input", "文本框"),
    textarea("textarea", "文本域"),
    select("select", "下拉框"),
    checkbox("checkbox", "复选框"),
    radio("radio", "单选框"),
    datetime("datetime", "日期时间控件"),
    date("date", "日期控件"), // todo 未考虑
    time("time", "时间控件"), // todo 未考虑
    imageUpload("imageUpload", "图片上传"),
    fileUpload("fileUpload", "上传控件"),
    editor("editor", "富文本控件");

    @JsonValue
    private String code;
    private String description;

    HtmlType(String code, String description) {
        this.code = code;
        this.description = description;
    }

    @Override
    public String getValue() {
        return this.code;
    }
}
