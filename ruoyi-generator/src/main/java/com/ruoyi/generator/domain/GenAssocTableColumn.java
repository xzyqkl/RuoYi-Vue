package com.ruoyi.generator.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.generator.domain.enums.HtmlType;
import com.ruoyi.generator.domain.enums.QueryType;
import com.ruoyi.generator.handler.JavaTypeTypeHandler;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * @Description: 关联字段
 * @Author: warden
 * @Date: 2021-02-17
 * @Version: V1.0
 */
@Data
@Accessors(chain = true)
@TableName(value = "gen_assoc_table_column")
public class GenAssocTableColumn extends BaseEntity
{

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 主表ID
     */
    private String linkId;

    /**
     * 列名称
     */
    private String columnName;

    /**
     * 列描述
     */
    private String columnComment;

    /**
     * 列类型
     */
    private String columnType;

    /**
     * JAVA类型
     */
    @TableField(value = "java_type", typeHandler = JavaTypeTypeHandler.class)
    private GenJavaType javaType;

    /**
     * JAVA字段名
     */
    private String javaField;

    /**
     * 是否主键（1是）
     */
    private String isPk;

    /**
     * 是否列表
     */
    private String isList;

    /**
     * 当为下拉列表时，是否可以多选
     */
    private String isMulti;

    /**
     * 查询方式（等于、不等于、大于、小于、范围）
     */
    private QueryType queryType;

    /**
     * 显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）
     */
    private HtmlType htmlType;

    /**
     * 字典类型
     */
    private String dictType;

    /**
     * 排序
     */
    private Integer sort;

//    @TableField(exist = false)
//    private String jdbcType;
    // ------------------------------------- extends -------------------------------------------

    public boolean isList() {
        return getBool(this.isList);
    }

    public boolean isMulti() {
        return getBool(this.isMulti);
    }

    public boolean getBool(String str) {
        return str != null && StringUtils.equals("1" , str);
    }
    //region 搜索条件
    //endregion

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("linkId", getLinkId())
                .append("name", getColumnName())
                .append("comment", getColumnComment())
                .append("type", getColumnType())
                .append("javaType", getJavaType())
                .append("javaField", getJavaField())
                .append("isPk", getIsPk())
                .append("queryType", getQueryType())
                .append("htmlType", getHtmlType())
                .append("dictType", getDictType())
                .append("sort", getSort())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .toString();
    }
}