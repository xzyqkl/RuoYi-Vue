package com.ruoyi.generator.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.generator.domain.enums.HtmlType;
import com.ruoyi.generator.domain.enums.QueryType;
import com.ruoyi.generator.handler.JavaTypeTypeHandler;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 代码生成业务字段表 gen_table_column
 *
 * @author ruoyi
 */
@Data
public class GenTableColumn extends BaseEntity
{

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 归属表编号
     */
    private String tableId;

    /**
     * 列名称
     */
    private String columnName;

    /**
     * 列描述
     */
    private String columnComment;

    /**
     * 列类型
     */
    private String columnType;

    /**
     * JAVA类型
     */
    @TableField(value = "java_type", typeHandler = JavaTypeTypeHandler.class)
    private GenJavaType javaType;

    /**
     * JAVA字段名
     */
    @NotBlank(message = "Java属性不能为空")
    private String javaField;

//    /**
//     * jdbcType
//     */
//    @TableField(exist = false)
//    private String jdbcType;

    /**
     * 是否主键（1是）
     */
    private String isPk;

    /**
     * 是否自增（1是）
     */
    private String isIncrement;

    /**
     * 是否必填（1是）
     */
    private String isRequired;

    /**
     * 是否唯一（1是）
     */
    private String isUnique;

    /**
     * 是否选择框
     */
    private String isSelect;

    /**
     * 是否编辑字段（0 否 1 编辑 2仅显示）
     */
    private String isEdit;

    /**
     * 是否列表字段（1是）
     */
    private String isList;

    /**
     * 当为下拉列表时，是否可以多选
     */
    private String isMulti;

    /**
     * 查询方式（EQ等于、NE不等于、GT大于、LT小于、LIKE模糊、BETWEEN范围）
     */
    private QueryType queryType;

    /**
     * 显示类型（input文本框、textarea文本域、select下拉框、checkbox复选框、radio单选框、datetime日期控件、image图片上传控件、upload文件上传控件、editor富文本控件）
     */
    private HtmlType htmlType;

    /**
     * 字典类型
     */
    private String dictType;

    /**
     * 排序
     */
    private Integer sort;

    // -------------------------- extends ---------------------------------

    @TableField(exist = false)
    private GenAssocTable assocTable;

    public boolean isPk() {
        return getBool(this.isPk);
    }

    public boolean isIncrement() {
        return getBool(this.isIncrement);
    }

    public boolean isRequired() {
        return getBool(this.isRequired);
    }

    public boolean isInsert() {
        return true;
    }

    public boolean isSelect() {
        return getBool(this.isSelect);
    }

    public boolean isUnique() {
        return getBool(this.isUnique);
    }

    public boolean isList() {
        return getBool(this.isList);
    }

    public boolean isMulti() {
        return getBool(this.isMulti);
    }

    public boolean getBool(String str) {
        return str != null && StringUtils.equals("1" , str);
    }

    public boolean isSuperColumn() {
        return isSuperColumn(this.javaField);
    }

    public static boolean isSuperColumn(String javaField) {
        return StringUtils.equalsAnyIgnoreCase(javaField,
                // BaseEntity
                "id" , "createBy" , "createTime" , "updateBy" , "updateTime" , "remark" ,
                // TreeEntity
                "parentName" , "parentId" , "orderNum" , "ancestors");
    }

    public boolean isUsableColumn() {
        return isUsableColumn(javaField);
    }

    public static boolean isUsableColumn(String javaField) {
        // isSuperColumn()中的名单用于避免生成多余Domain属性，若某些属性在生成页面时需要用到不能忽略，则放在此处白名单
        return StringUtils.equalsAnyIgnoreCase(javaField, "parentId", "orderNum", "remark");
    }

    public String readConverterExp() {
        String remarks = StringUtils.substringBetween(this.columnComment, "（", "）");
        StringBuffer sb = new StringBuffer();
        if (StringUtils.isNotEmpty(remarks)) {
            for (String value : remarks.split(" ")) {
                if (StringUtils.isNotEmpty(value)) {
                    Object startStr = value.subSequence(0, 1);
                    String endStr = value.substring(1);
                    sb.append(startStr).append("=").append(endStr).append(",");
                }
            }
            return sb.deleteCharAt(sb.length() - 1).toString();
        } else {
            return this.columnComment;
        }
    }

    @Override
    public String toString() {
        return "GenTableColumn{" +
                "columnName='" + columnName + '\'' +
                '}';
    }
}