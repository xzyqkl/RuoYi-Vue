package com.ruoyi.generator.domain.enums;

import com.fasterxml.jackson.annotation.JsonValue;
import com.ruoyi.common.IBaseEnum;
import lombok.Getter;

/**
 * html类型
 */
@Getter
public enum QueryType implements IBaseEnum<String>
{
    EQ("EQ", "="),
    NE("NE", "!="),
    GT("GT", ">"),
    GTE("GTE", ">="),
    LT("LT", "<"),
    LTE("LTE", "<="),
    LIKE("LIKE", "Like"),
    RLIKE("RLIKE", "RightLike"),
    LLIKE("LLIKE", "LeftLike"),
    BETWEEN("BETWEEN", "Between");

    @JsonValue
    private String code;
    private String description;

    QueryType(String code, String description) {
        this.code = code;
        this.description = description;
    }

    @Override
    public String getValue() {
        return this.code;
    }
}
