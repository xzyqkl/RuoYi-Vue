package com.ruoyi.generator.domain;

import com.ruoyi.generator.domain.enums.QueryType;
import lombok.Data;

@Data
public class SearchCondition {
    public SearchCondition() {
    }

    public SearchCondition(String column, QueryType match) {
        this.column = column;
        this.match = match;
    }

    /**
     * 数据库字段
     */
    private String column;

    /**
     * 匹配条件
     */
    private QueryType match;
}
