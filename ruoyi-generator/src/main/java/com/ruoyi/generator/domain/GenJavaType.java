package com.ruoyi.generator.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.enums.YesOrNo;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * @Description: java类型
 * @Author: warden
 * @Date: 2021-02-27
 * @Version: V1.0
 */
@Data
@Accessors(chain = true)
@TableName(value = "gen_java_type")
public class GenJavaType extends BaseEntity
{
    public GenJavaType() {}
    public GenJavaType(String className) {
        this.className = className;
    }

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 类名称
     */
    @Excel(name = "类名称")
    private String className;

    /**
     * 包路径
     */
    @Excel(name = "包路径")
    private String packageName;

    /**
     * 数据请求地址
     */
    @Excel(name = "数据请求地址")
    private String dataUrl;

    /**
     * 表名称
     */
    @Excel(name = "表名称")
    private String tableName;

    /**
     * 是否基本类型
     */
    @Excel(name = "是否基本类型", readConverterExp = "Y=是,N=否")
    private YesOrNo type;

    /**
     * 排序
     */
    @Excel(name = "排序")
    private Integer sort;

    /**
     * 是否预设（避免被删除）
     */
    @Excel(name = "是否预设", readConverterExp = "Y=是,N=否")
    private YesOrNo system;

    // ------------------------------------- extends -------------------------------------------
    //region 搜索条件
    //endregion

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("className", getClassName())
                .append("packageName", getPackageName())
                .append("dataUrl", getDataUrl())
                .append("tableName", getTableName())
                .append("type", getType())
                .append("sort", getSort())
                .append("system", getSystem())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .toString();
    }
}