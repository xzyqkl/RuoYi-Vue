package com.ruoyi.generator.domain.enums;

import com.fasterxml.jackson.annotation.JsonValue;
import com.ruoyi.common.IBaseEnum;
import lombok.Getter;

/**
 * 生成方式
 */
@Getter
public enum GenType implements IBaseEnum<String>
{
    ZIP("0", "压缩包"), CUSTOM("1", "自定义");

    @JsonValue
    private String code;
    private String description;

    GenType(String code, String description) {
        this.code = code;
        this.description = description;
    }

    @Override
    public String getValue() {
        return this.code;
    }
}
