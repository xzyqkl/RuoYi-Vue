package com.ruoyi.generator.handler;

import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import com.ruoyi.common.utils.ReflectUtil;
import com.ruoyi.generator.domain.GenJavaType;

public class JavaTypeTypeHandler extends JacksonTypeHandler {
    public JavaTypeTypeHandler(Class<?> type) {
        super(type);
    }

    @Override
    protected Object parse(String className) {
        return new GenJavaType(className);
    }

    @Override
    protected String toJson(Object obj) {
        if (obj == null) {
            return null;
        }
        String value = (String) ReflectUtil.getFieldValue(obj, "className");

        return value;
    }
}
