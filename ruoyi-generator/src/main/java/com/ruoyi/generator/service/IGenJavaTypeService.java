package com.ruoyi.generator.service;

import com.ruoyi.common.service.BaseService;
import com.ruoyi.generator.domain.GenJavaType;

import java.util.List;

/**
 * @Description: java类型 Service接口
 * @Author: warden
 * @Date: 2021-02-27
 * @Version: V1.0
 */
public interface IGenJavaTypeService extends BaseService<GenJavaType>
{

    /**
     * 分页查询
     *
     * @param javaType
     * @return
     */
    List<GenJavaType> search(GenJavaType javaType);

    /**
     * 查询所有属性
     *
     * @param javaType
     * @return
     */
    List<GenJavaType> listAll(GenJavaType javaType);

    /**
     * 新增、修改保存方法
     *
     * @param javaType
     * @param updateAll 当为true时，全量更新
     * @return
     */
    GenJavaType commonSave(GenJavaType javaType, Boolean updateAll) throws Exception;

    /**
     * 新增、修改保存方法（按属性不为空修改）
     *
     * @param javaType
     * @return
     */
    GenJavaType commonSave(GenJavaType javaType) throws Exception;

    /**
     * 查询单个java类型记录
     * 若有子记录，则也查询出子记录
     *
     * @param id
     * @return
     */
    GenJavaType selectById(String id);

    /**
     * 删除单个java类型
     *
     * @param id
     * @return
     */
    boolean removeById(String id);

    /**
     * 删除多个java类型
     *
     * @param ids
     * @return
     */
    boolean removeByIds(String[] ids);
}