package com.ruoyi.generator.service.impl;

import com.ruoyi.common.service.AbstractService;
import com.ruoyi.generator.domain.GenAssocTableColumn;
import com.ruoyi.generator.mapper.GenAssocTableColumnMapper;
import com.ruoyi.generator.service.IGenAssocTableColumnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description: 关联字段 Service接口实现
 * @Author: warden
 * @Date: 2021-02-17
 * @Version: V1.0
 */
@Service
public class GenAssocTableColumnServiceImpl extends AbstractService<GenAssocTableColumnMapper, GenAssocTableColumn> implements IGenAssocTableColumnService
{
    @Autowired
    private GenAssocTableColumnMapper assocColumnMapper;

}