package com.ruoyi.generator.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.common.enums.YesOrNo;
import com.ruoyi.common.service.AbstractService;
import com.ruoyi.common.utils.EntityUtil;
import com.ruoyi.generator.domain.GenJavaType;
import com.ruoyi.generator.mapper.GenJavaTypeMapper;
import com.ruoyi.generator.service.IGenJavaTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

/**
 * @Description: java类型 Service接口实现
 * @Author: warden
 * @Date: 2021-02-27
 * @Version: V1.0
 */
@Service
public class GenJavaTypeServiceImpl extends AbstractService<GenJavaTypeMapper, GenJavaType> implements IGenJavaTypeService
{
    @Autowired
    private GenJavaTypeMapper javaTypeMapper;

    /**
     * 分页查询
     *
     * @param javaType
     * @return
     */
    @Override
    public List<GenJavaType> search(GenJavaType javaType) {
        return this.javaTypeMapper.search(javaType);
    }

    /**
     * 查询全属性
     *
     * @param javaType
     * @return
     */
    @Override
    public List<GenJavaType> listAll(GenJavaType javaType) {
        return this.javaTypeMapper.listAll(javaType);
    }

    /**
     * 新增、修改保存方法
     *
     * @param javaType
     * @param updateAll 当为true时，全量更新
     * @return
     */
    @Override
    @Transactional
    public GenJavaType commonSave(GenJavaType javaType, Boolean updateAll) throws Exception {
        if (updateAll == null) {
            updateAll = false;
        }
        EntityUtil.trimEntity(javaType);

        // 1. 基础数据处理
        if (updateAll) {
            if (javaType.getSystem() == null) {
                javaType.setSystem(YesOrNo.NO);
            }
        }

        if (javaType.getId() == null) {
            this.save(javaType);
        } else {
            if (updateAll) {
                this.updateAll(javaType);
            } else {
                this.updateById(javaType);
            }
        }
        return javaType;
    }

    /**
     * 新增、修改保存方法（按属性不为空修改）
     *
     * @param javaType
     * @return
     */
    @Override
    @Transactional
    public GenJavaType commonSave(GenJavaType javaType) throws Exception {
        return this.commonSave(javaType, false);
    }

    /**
     * 查询单个java类型
     *
     * @param id
     * @return
     */
    @Override
    public GenJavaType selectById(String id) {
        return this.javaTypeMapper.selectById(id);
    }

    /**
     * 删除单个java类型
     *
     * @param id
     * @return
     */
    @Override
    @Transactional
    public boolean removeById(String id) {
        // 删除
        return this.remove(Wrappers.<GenJavaType>lambdaQuery()
                .eq(GenJavaType::getId, id)
                .ne(GenJavaType::getSystem, YesOrNo.YES));
    }

    /**
     * 删除多个java类型
     *
     * @param ids
     * @return
     */
    @Override
    @Transactional
    public boolean removeByIds(String[] ids) {
        // 删除
        List<String> idList = Arrays.asList(ids);
        return this.remove(Wrappers.<GenJavaType>lambdaQuery()
                .in(GenJavaType::getId, idList)
                .ne(GenJavaType::getSystem, YesOrNo.YES));
    }
}