package com.ruoyi.generator.service.impl;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.text.CharsetKit;
import com.ruoyi.common.enums.YesOrNo;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.EntityUtil;
import com.ruoyi.common.utils.SaveDiffUtil;
import com.ruoyi.common.utils.StringUtils;
import org.apache.commons.io.FileUtils;
import com.ruoyi.generator.domain.*;
import com.ruoyi.generator.domain.enums.TplCategory;
import com.ruoyi.generator.mapper.GenTableColumnMapper;
import com.ruoyi.generator.mapper.GenTableMapper;
import com.ruoyi.generator.service.IGenAssocTableColumnService;
import com.ruoyi.generator.service.IGenAssocTableService;
import com.ruoyi.generator.service.IGenTableService;
import com.ruoyi.generator.util.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * 业务 服务层实现
 *
 * @author ruoyi
 */
@Service
@Slf4j
public class GenTableServiceImpl implements IGenTableService
{
    @Autowired
    private GenTableMapper genTableMapper;

    @Autowired
    private GenTableColumnMapper genTableColumnMapper;

    @Autowired
    private IGenAssocTableService assocTableService;

    @Autowired
    private IGenAssocTableColumnService assocColumnService;

    /**
     * 查询业务列表
     *
     * @param genTable 业务信息
     * @return 业务集合
     */
    @Override
    public List<GenTable> search(GenTable genTable) {
        return genTableMapper.search(genTable);
    }

    /**
     * 查询业务信息
     *
     * @param id 业务ID
     * @return 业务信息
     */
    @Override
    public GenTable selectById(String id) {
        List<GenTable> tableList = genTableMapper.selectAll(new GenTable().setId(id));
        if (CollectionUtils.isEmpty(tableList)) {
            return null;
        }
        return tableList.get(0);
    }

    /**
     * 根据名称查询表
     *
     * @param name
     * @return
     */
    @Override
    public GenTable selectByName(String name) {
        List<GenTable> tableList = genTableMapper.selectAll(new GenTable().setTableName(name));
        if (CollectionUtils.isEmpty(tableList)) {
            return null;
        }
        return tableList.get(0);
    }

    /**
     * 查询据库列表
     *
     * @param genTable 业务信息
     * @return 数据库表集合
     */
    @Override
    public List<GenTable> selectDbTableList(GenTable genTable) {
        return genTableMapper.selectDbTableList(genTable);
    }

    /**
     * 查询据库列表
     *
     * @param tableNames 表名称组
     * @return 数据库表集合
     */
    @Override
    public List<GenTable> selectDbTableListByNames(String[] tableNames) {
        return genTableMapper.selectDbTableListByNames(tableNames);
    }

    /**
     * 查询所有表信息
     *
     * @return 表信息集合
     */
    @Override
    public List<GenTable> selectGenTableAll() {
        return genTableMapper.selectAll(new GenTable());
    }

    /**
     * 修改业务
     *
     * @param genTable 业务信息
     * @return 结果
     */
    @Override
    @Transactional
    public void updateGenTable(GenTable genTable) throws Exception {
        EntityUtil.trimEntity(genTable);
        int row = genTableMapper.alwaysUpdateSomeColumnById(genTable);
        if (row > 0) {
            for (GenTableColumn column : genTable.getColumns()) {
                if (column.getIsRequired() == null) {
                    column.setIsRequired("0");
                }
                if (column.getIsUnique() == null) {
                    column.setIsUnique("0");
                }
                if (column.getIsSelect() == null) {
                    column.setIsSelect("0");
                }
                if (column.getIsEdit() == null) {
                    column.setIsEdit("0");
                }
                if (column.getIsList() == null) {
                    column.setIsList("0");
                }
                if (column.getIsMulti() == null) {
                    column.setIsMulti("0");
                }
                genTableColumnMapper.alwaysUpdateSomeColumnById(column);
            }

            //region 处理关联表
            // 1. 处理关联表主表
            for (GenAssocTable assocTable : genTable.getAssocTableList()) {
                EntityUtil.trimEntity(assocTable);
                assocTable.setTableId(genTable.getId());
            }
            List<GenAssocTable> formAssocTabList = genTable.getAssocTableList();
            List<GenAssocTable> oriAssocTabList = this.assocTableService.list(Wrappers.<GenAssocTable>lambdaQuery()
                    .eq(GenAssocTable::getTableId, genTable.getId())
            );
            // 处理是否原来的字段
            SaveDiffUtil.saveFormDiff(formAssocTabList, oriAssocTabList, GenAssocTable.class, this.assocTableService, true, true);

            for (GenAssocTable assocTable : genTable.getAssocTableList()) {
                // 处理关联表子表
                for (GenAssocTableColumn assocColumn : assocTable.getColumns()) {
                    EntityUtil.trimEntity(assocColumn);
                    if (assocColumn.getIsList() == null) {
                        assocColumn.setIsList("0");
                    }
                    if (assocColumn.getIsMulti() == null) {
                        assocColumn.setIsMulti("0");
                    }
                    assocColumn.setLinkId(assocTable.getId());
                }

                List<GenAssocTableColumn> formAssocColumnList = assocTable.getColumns();
                List<GenAssocTableColumn> oriAssocColumnList = this.assocColumnService.list(Wrappers.<GenAssocTableColumn>lambdaQuery()
                        .eq(GenAssocTableColumn::getLinkId, assocTable.getId())
                );
                // 处理是否原来的字段
                SaveDiffUtil.updateObject(oriAssocColumnList, formAssocColumnList, "columnName", "id");
                SaveDiffUtil.saveFormDiff(formAssocColumnList, oriAssocColumnList, GenAssocTableColumn.class, this.assocColumnService, true, true);
            }
            //endregion
        }
    }

    /**
     * 删除业务对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    @Transactional
    public void deleteGenTableByIds(String[] ids) {
        List<String> idList = Arrays.asList(ids);
        genTableMapper.deleteBatchIds(idList);
        genTableColumnMapper.delete(Wrappers.<GenTableColumn>lambdaQuery().in(GenTableColumn::getTableId, idList));
        List<String> linkIds = assocTableService.list((Wrappers.<GenAssocTable>lambdaQuery().in(GenAssocTable::getTableId, idList))).stream().map(item -> item.getId()).collect(Collectors.toList());
        assocColumnService.remove(Wrappers.<GenAssocTableColumn>lambdaQuery().in(CollectionUtils.isNotEmpty(linkIds), GenAssocTableColumn::getLinkId, linkIds));
        assocTableService.remove(Wrappers.<GenAssocTable>lambdaQuery().in(GenAssocTable::getTableId, idList));
    }

    /**
     * 导入表结构
     *
     * @param tableList 导入表列表
     */
    @Override
    @Transactional
    public void importGenTable(List<GenTable> tableList) {
        try {
            for (GenTable table : tableList) {
                String tableName = table.getTableName();
                GenUtils.initTable(table);
                int row = genTableMapper.insert(table);
                if (row > 0) {
                    // 保存列信息
                    List<GenTableColumn> genTableColumns = genTableColumnMapper.selectDbTableColumnsByName(tableName);
                    for (GenTableColumn column : genTableColumns) {
                        GenUtils.initColumnField(column, table);
                        genTableColumnMapper.insert(column);
                    }
                }
            }
        } catch (Exception e) {
            throw new ServiceException("导入失败：" + e.getMessage());
        }
    }

    /**
     * 预览代码
     *
     * @param tableId 表编号
     * @return 预览数据列表
     */
    @Override
    public Map<String, String> previewCode(String tableId) {
        // 查询表信息
        GenTable table = dealTableById(tableId);

        return genCode(table);
    }

    /**
     * 生成代码（下载方式）
     *
     * @param tableName 表名称
     * @return 数据
     */
    @Override
    public byte[] downloadCode(String tableName) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(outputStream);
        generatorCode(tableName, zip);
        IOUtils.closeQuietly(zip);
        return outputStream.toByteArray();
    }

    /**
     * 生成代码（自定义路径）
     *
     * @param tableName 表名称
     */
    @Override
    public void generatorCode(String tableName) {
        GenTable table = dealTableByName(tableName);

        VelocityInitializer.initVelocity();

        VelocityContext context = VelocityUtils.prepareContext(table);

        // 获取模板列表
        List<String> templates = VelocityUtils.getTemplateList(table.getTplCategory());
        for (String template : templates) {
            if (!StringUtils.contains(template, "sql.vm")) {
                // 渲染模板
                StringWriter sw = new StringWriter();
                Template tpl = Velocity.getTemplate(template, Constants.UTF8);
                tpl.merge(context, sw);
                try {
                    String path = getGenPath(table, template);
                    FileUtils.writeStringToFile(new File(path), sw.toString(), CharsetKit.UTF_8);
                } catch (IOException e) {
                    throw new ServiceException("渲染模板失败，表名：" + table.getTableName());
                }
            }
        }
    }

    /**
     * 同步数据库
     *
     * @param tableName 表名称
     */
    @Override
    @Transactional
    public void synchDb(String tableName) {
        List<GenTable> tableList = genTableMapper.selectAll(new GenTable().setTableName(tableName));
        if (CollectionUtils.isEmpty(tableList)) {
            throw new ServiceException("未找到表信息");
        }
        GenTable table = tableList.get(0);
        List<GenTableColumn> tableColumns = table.getColumns();
        List<String> tableColumnNames = tableColumns.stream().map(GenTableColumn::getColumnName).collect(Collectors.toList());

        List<GenTableColumn> dbTableColumns = genTableColumnMapper.selectDbTableColumnsByName(tableName);
        if (StringUtils.isEmpty(dbTableColumns)) {
            throw new ServiceException("同步数据失败，原表结构不存在");
        }
        List<String> dbTableColumnNames = dbTableColumns.stream().map(GenTableColumn::getColumnName).collect(Collectors.toList());

        dbTableColumns.forEach(column -> {
            if (!tableColumnNames.contains(column.getColumnName())) {
                GenUtils.initColumnField(column, table);
                genTableColumnMapper.insert(column);
            }
        });

        List<String> delColumnIds = tableColumns.stream()
                .filter(column -> !dbTableColumnNames.contains(column.getColumnName())).map(column -> column.getId()).collect(Collectors.toList());
        if (StringUtils.isNotEmpty(delColumnIds)) {
            genTableColumnMapper.deleteBatchIds(delColumnIds);
        }
    }

    /**
     * 批量生成代码
     *
     * @param tableNames 表数组
     * @return 数据
     */
    @Override
    public byte[] downloadCode(String[] tableNames) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(outputStream);
        for (String tableName : tableNames) {
            generatorCode(tableName, zip);
        }
        IOUtils.closeQuietly(zip);
        return outputStream.toByteArray();
    }

    /**
     * 查询表信息并生成代码
     */
    private void generatorCode(String tableName, ZipOutputStream zip) {
        GenTable table = dealTableByName(tableName);

        Map<String, String> mapRes = genCode(table);

        for (Map.Entry<String, String> entry : mapRes.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            try {
                // 添加到zip
                zip.putNextEntry(new ZipEntry(key));
                IOUtils.write(value, zip, Constants.UTF8);
                zip.flush();
                zip.closeEntry();
            } catch (IOException e) {
                log.error("渲染模板失败，表名：" + table.getTableName(), e);
            }
        }
//        VelocityInitializer.initVelocity();
//
//        VelocityContext context = VelocityUtils.prepareContext(table);
//
//        // 获取模板列表
//        List<String> templates = VelocityUtils.getTemplateList(table.getTplCategory());
//        for (String template : templates) {
//            // 渲染模板
//            StringWriter sw = new StringWriter();
//            Template tpl = Velocity.getTemplate(template, Constants.UTF8);
//            tpl.merge(context, sw);
//            try {
//                // 添加到zip
//                zip.putNextEntry(new ZipEntry(VelocityUtils.getFileName(template, table)));
//                IOUtils.write(sw.toString(), zip, Constants.UTF8);
//                IOUtils.closeQuietly(sw);
//                zip.flush();
//                zip.closeEntry();
//            } catch (IOException e) {
//                log.error("渲染模板失败，表名：" + table.getTableName(), e);
//            }
//        }
    }

    /**
     * 修改保存参数校验
     *
     * @param genTable 业务信息
     */
    @Override
    public void validateEdit(GenTable genTable) {
        Map<String, GenAssocTable> foreignKeyMap = new HashMap<>(); // 外键, 关联表对象
        Map<String, List<String>> aliasMap = new HashMap<>(); // 表名， 实例名列表

        if (CollectionUtils.isNotEmpty(genTable.getAssocTableList())) {
            // 1. 关联表列不可为空 2. 关联表:如果有字段关联同一个表，则实例别名不可相同 3. 同一个字段不可关联多个表
            for (GenAssocTable assocTable : genTable.getAssocTableList()) {
                if (CollectionUtils.isEmpty(assocTable.getColumns())) {
                    throw new ServiceException("关联表列不可为空");
                }

                if (foreignKeyMap.containsKey(assocTable.getForeignKey())) {
                    throw new ServiceException("同一外键不可关联多个关联表");
                } else {
                    foreignKeyMap.put(assocTable.getForeignKey(), assocTable);
                }

                List<String> aliasList;
                if (!aliasMap.containsKey(assocTable.getTableName())) {
                    aliasList = new ArrayList<>();
                    aliasList.add(assocTable.getInsAlias());
                    aliasMap.put(assocTable.getTableName(), aliasList);
                } else {
                    aliasList = aliasMap.get(assocTable.getTableName());
                    if (aliasList.contains(assocTable.getInsAlias())) {
                        throw new ServiceException(String.format("表名（%s）相同的实例别名不能相同", assocTable.getTableName()));
                    }
                }
            }
        }

        // 验证列
        for (GenTableColumn column : genTable.getColumns()) {
            GenJavaType javaType = column.getJavaType();
            if (javaType != null && javaType.getType() != null) {
                if (javaType.getType() == YesOrNo.NO) {
                    // 必须存在相应关联的表
                    if (!foreignKeyMap.containsKey(column.getColumnName())) {
                        throw new ServiceException(String.format("字段%s未设置关联表%s", column.getColumnName(), javaType.getTableName()));
                    }

                    // 属性名称不能与关联表实例名称相同
                    if (column.getJavaField().equals(foreignKeyMap.get(column.getColumnName()).getInsAlias())) {
                        throw new ServiceException(String.format("属性名称(%s)不能与关联表(%s)实例名称相同", column.getJavaField(), foreignKeyMap.get(column.getColumnName()).getTableName()));
                    }
                }
            }

            if (column.isSelect()) {
                // 判断是否关联了表
                if (javaType.getType() != YesOrNo.NO) {
                    throw new ServiceException("仅关联表属性可以设置选择框");
                }
            }
        }

        switch (genTable.getTplCategory()) {
            case TREE:
                if (genTable.getOptions() == null) {
                    throw new ServiceException("缺少树相关参数");
                }
                Options options = genTable.getOptions();
                if (StringUtils.isEmpty(options.getTreeCode())) {
                    throw new ServiceException("树编码字段不能为空");
                } else if (StringUtils.isEmpty(options.getTreeParentCode())) {
                    throw new ServiceException("树父编码字段不能为空");
                } else if (StringUtils.isEmpty(options.getTreeName())) {
                    throw new ServiceException("树名称字段不能为空");
                }
                break;
            case ATTACHED:
                if (StringUtils.isEmpty(genTable.getParentName())) {
                    throw new ServiceException("关联子表的表名不能为空");
                } else if (StringUtils.isEmpty(genTable.getForeignKey())) {
                    throw new ServiceException("子表关联的外键名不能为空");
                }
                break;
        }
    }

    /**
     * 设置主键列信息
     *
     * @param table 业务表信息
     */
    public void setPkColumn(GenTable table) {
        for (GenTableColumn column : table.getColumns()) {
            if (column.isPk()) {
                table.setPkColumn(column);
                break;
            }
        }
        if (StringUtils.isNull(table.getPkColumn())) {
            table.setPkColumn(table.getColumns().get(0));
        }

        if (table.getSubTables() != null) {
            for (GenTable subTab : table.getSubTables()) {
                setPkColumn(subTab);
            }
        }
    }


    /**
     * 获取代码生成地址
     *
     * @param table    业务表信息
     * @param template 模板文件路径
     * @return 生成地址
     */
    public static String getGenPath(GenTable table, String template) {
        String genPath = table.getGenPath();
        if (StringUtils.equals(genPath, "/")) {
            return System.getProperty("user.dir") + File.separator + "src" + File.separator + VelocityUtils.getFileName(template, table);
        }
        return genPath + File.separator + VelocityUtils.getFileName(template, table);
    }

    private GenTable dealTableById(String id) {
        GenTable table = this.selectById(id);
        if (table == null) {
            throw new ServiceException("未找到表信息");
        }
        dealTable(table);
        return table;
    }

    private GenTable dealTableByName(String tableName) {
        // 查询表信息
        List<GenTable> tableList = genTableMapper.selectAll(new GenTable().setTableName(tableName));
        if (CollectionUtils.isEmpty(tableList)) {
            throw new ServiceException("未找到表信息");
        }
        GenTable table = tableList.get(0);
        if (table.getTplCategory() == TplCategory.ATTACHED) {
            throw new ServiceException("附表不可以生成代码");
        }
        dealTable(table);
        return table;
    }

    private void dealTable(GenTable table) {
        // 设置主子表信息
        setSubTable(table);
        // 设置主键列信息
        setPkColumn(table);

        // 处理关联表关系
        dealAssocTable(table);
    }

    private void dealAssocTable(GenTable table) {
        for (GenTable subTable : table.getSubTables()) {
            dealAssocTable(subTable);
        }

        // 处理关联表。如果关联表需要选择，则需要生成相应代码
        Map<String, GenTable> tableMap = new HashMap<>();
//        tableMap.put(table.getTableName(), table); // 不能直接引用，因为还需要清理不需要的column
        for (GenAssocTable assocTable : table.getAssocTableList()) {
            // 是否需要选择框
            boolean isNeed = false;
            for (GenTableColumn column : table.getColumns()) {
                // 列被标识需要选择框，并且列的数据库字段=关联表的外键
                if (column.isSelect() && column.getColumnName().equals(assocTable.getForeignKey())) {
                    isNeed = true;
                    assocTable.setParentColumn(column);
                    break;
                }
            }
            if (!isNeed) {
                continue;
            }
            if (tableMap.containsKey(assocTable.getTableName())) {
                assocTable.setReferenceTable(tableMap.get(assocTable.getTableName()));
            } else {
                GenTable referenceTable = this.selectByName(assocTable.getTableName());
                // 清理不需要的column
                Iterator<GenTableColumn> columnIt = referenceTable.getColumns().iterator();
                while (columnIt.hasNext()) {
                    GenTableColumn column = columnIt.next();
                    // 主键不可清除
                    if (column.isPk()) {
                        continue;
                    }

                    // 查找是否在关联表字段中存在
                    boolean isExist = false;
                    for (GenAssocTableColumn assocColumn : assocTable.getColumns()) {
                        if (assocColumn.getColumnName().equals(column.getColumnName())) {
                            isExist = true;

                            // 设置搜索条件 主页面使用设置的，子页面使用原有的
//                            column.setQueryType(assocColumn.getQueryType());
//                            column.setHtmlType(assocColumn.getHtmlType());
                            break;
                        }
                    }

                    if (!isExist) {
                        columnIt.remove();
                    }

                }
                tableMap.put(assocTable.getTableName(), referenceTable);
                assocTable.setReferenceTable(referenceTable);
            }
        }
    }

    private Map<String, String> genCode(GenTable table) {
        Map<String, String> mainRes = genCode(table, false);

        // 处理select弹出框
        // 同一个名称的表只处理一次
        List<String> tableNames = new ArrayList<>();

        Map<String, String> selectMap = genSelectCode(table, tableNames);
        mainRes.putAll(selectMap);

        return mainRes;
    }

    /**
     * 处理选择弹出框
     *
     * @param table
     * @return
     */
    private Map<String, String> genSelectCode(GenTable table, List<String> tableNames) {
        Map<String, String> mainRes = new HashMap<>();
        for (GenTable subTable : table.getSubTables()) {
            Map<String, String> subTableMap = genSelectCode(subTable, tableNames);
            mainRes.putAll(subTableMap);
        }
        for (GenAssocTable assocTable : table.getAssocTableList()) {
            GenTable referTable = assocTable.getReferenceTable();
            if (referTable != null) {
                if (tableNames.contains(referTable.getTableName())) {
                    continue;
                }
                // 包路径设置为主表一致
                tableNames.add(referTable.getTableName());
                referTable.setAppearCount(assocTable.getAppearCount());
                referTable.setParentTable(table);
                Map<String, String> accosMapRes = genCode(referTable, true);
                mainRes.putAll(accosMapRes);
            }
        }

        return mainRes;
    }

    /**
     * @param table
     * @param isSelect 标识是否为select选择框
     * @return
     */
    private Map<String, String> genCode(GenTable table, boolean isSelect) {
        // 设置其他信息
        Settings settings = new Settings();
        if (isSelect) {
            GenTable parentTable = table.getParentTable();
            this.setPkColumn(table);
            table.setInterface(true); // 该表为接口表
            settings.setPackageName(parentTable.getPackageName());
            settings.setModuleName(parentTable.getModuleName());
            settings.setBusinessName(parentTable.getBusinessName());
        } else {
            settings.setPackageName(table.getPackageName());
            settings.setModuleName(table.getModuleName());
            settings.setBusinessName(table.getBusinessName());
        }
        settings.setGenType(table.getGenType());
        settings.setFunctionAuthor(table.getFunctionAuthor());
        settings.setPermissionPrefix(StringUtils.format("{}:{}", table.getModuleName(), table.getBusinessName()));
        settings.setParentMenuId(table.getOptions().getParentMenuId() == null ? FreeMarkerUtil.DEFAULT_PARENT_MENU_ID : table.getOptions().getParentMenuId());
        settings.setColumnSize(table.getColumnSize() == null || table.getColumnSize() <= 0 ? 1 : table.getColumnSize());
        DealFieldUtil.dealField(settings, null, table);

        try {
            Map<String, String> mainRes = FreeMarkerUtil.genCode(settings, table, true, isSelect);

            return mainRes;
        } catch (IOException e) {
            e.printStackTrace();
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * 设置主子表信息
     *
     * @param table 业务表信息
     */
    private void setSubTable(GenTable table) {
        if (table.getTplCategory() != TplCategory.ATTACHED && table.getTplCategory() != TplCategory.PRIMARY) {
            return;
        }
        // 找table的子表
        List<GenTable> subTableList = genTableMapper.selectAll(new GenTable().setParentName(table.getTableName()));
        if (CollectionUtils.isEmpty(subTableList)) {
            return;
        }
        if (table.getSubTables() == null) {
            table.setSubTables(new ArrayList<>());
        }

        // 递归设置所有层级的主子表关系
        for (GenTable subTab : subTableList) {
            // 设置子表信息与主表相同
            subTab.setPackageName(table.getPackageName());
            subTab.setModuleName(table.getModuleName());
            subTab.setBusinessName(table.getBusinessName());
            subTab.setGenType(table.getGenType());
            subTab.setFunctionAuthor(table.getFunctionAuthor());
            table.getSubTables().add(subTab);
            setSubTable(subTab);
        }
    }
}