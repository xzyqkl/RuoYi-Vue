package com.ruoyi.generator.service;

import com.ruoyi.common.service.BaseService;
import com.ruoyi.generator.domain.GenAssocTableColumn;

/**
 * @Description: 关联字段 Service接口
 * @Author: warden
 * @Date: 2021-02-17
 * @Version: V1.0
 */
public interface IGenAssocTableColumnService extends BaseService<GenAssocTableColumn>
{

}