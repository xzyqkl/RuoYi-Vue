package com.ruoyi.generator.service;

import com.ruoyi.common.service.BaseService;
import com.ruoyi.generator.domain.GenAssocTable;

import java.util.List;

/**
 * @Description: 代码生成业务 Service接口
 * @Author: warden
 * @Date: 2021-02-17
 * @Version: V1.0
 */
public interface IGenAssocTableService extends BaseService<GenAssocTable>
{

    /**
     * 分页查询
     *
     * @param assocTable
     * @return
     */
    List<GenAssocTable> search(GenAssocTable assocTable);

    /**
     * 新增、修改保存方法
     *
     * @param assocTable
     * @param updateAll       当为true时，全量更新
     * @return
     */
    GenAssocTable commonSave(GenAssocTable assocTable, Boolean updateAll) throws Exception;

    /**
     * 新增、修改保存方法（按属性不为空修改）
     *
     * @param assocTable
     * @return
     */
    GenAssocTable commonSave(GenAssocTable assocTable) throws Exception;

    /**
     * 查询单个代码生成业务记录
     * 若有子记录，则也查询出子记录
     *
     * @param id
     * @return
     */
    GenAssocTable selectById(String id);

    /**
     * 删除单个代码生成业务
     *
     * @param id
     * @return
     */
    boolean removeById(String id);

    /**
     * 删除多个代码生成业务
     *
     * @param ids
     * @return
     */
    boolean removeByIds(String ids);

    List<GenAssocTable> listByLinkId(String linkId);
}