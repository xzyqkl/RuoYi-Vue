package com.ruoyi.generator.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.service.AbstractService;
import com.ruoyi.common.utils.EntityUtil;
import com.ruoyi.common.utils.SaveDiffUtil;
import com.ruoyi.generator.domain.GenAssocTable;
import com.ruoyi.generator.domain.GenAssocTableColumn;
import com.ruoyi.generator.mapper.GenAssocTableMapper;
import com.ruoyi.generator.service.IGenAssocTableColumnService;
import com.ruoyi.generator.service.IGenAssocTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

/**
 * @Description: 代码生成业务 Service接口实现
 * @Author: warden
 * @Date: 2021-02-17
 * @Version: V1.0
 */
@Service
public class GenAssocTableServiceImpl extends AbstractService<GenAssocTableMapper, GenAssocTable> implements IGenAssocTableService
{
    @Autowired
    private GenAssocTableMapper assocTableMapper;
    
    @Autowired
    private IGenAssocTableColumnService assocColumnService;
    
    /**
     * 分页查询
     *
     * @param assocTable
     * @return
     */
    @Override
    public List<GenAssocTable> search(GenAssocTable assocTable) {
        return this.assocTableMapper.search(assocTable);
    }

    /**
     * 新增、修改保存方法
     *
     * @param assocTable
     * @param updateAll       当为true时，全量更新
     * @return
     */
    @Override
    @Transactional
    public GenAssocTable commonSave(GenAssocTable assocTable, Boolean updateAll) throws Exception {
        if (updateAll == null) {
            updateAll = false;
        }
        EntityUtil.trimEntity(assocTable);

        // 1. 基础数据处理

        if (assocTable.getId() == null) {
            this.save(assocTable);
        } else {
            if (updateAll) {
                this.updateAll(assocTable);
            } else {
                this.updateById(assocTable);
            }
        }

        if (updateAll) {

            // 关联字段处理
            for (GenAssocTableColumn assocColumn : assocTable.getColumns()) {
                EntityUtil.trimEntity(assocColumn);
                assocColumn.setLinkId(assocTable.getId());
            }

            List<GenAssocTableColumn> formAssocColumnList = assocTable.getColumns();
            List<GenAssocTableColumn> oriAssocColumnList = this.assocColumnService.list(Wrappers.<GenAssocTableColumn>lambdaQuery()
                .eq(GenAssocTableColumn::getLinkId, assocTable.getId())
            );
            // 处理是否原来的字段
            SaveDiffUtil.updateObject(oriAssocColumnList, formAssocColumnList, "columnName", "id");
            SaveDiffUtil.saveFormDiff(formAssocColumnList, oriAssocColumnList, GenAssocTableColumn.class, this.assocColumnService, true);
        }

        return assocTable;
    }

    /**
     * 新增、修改保存方法（按属性不为空修改）
     *
     * @param assocTable
     * @return
     */
    @Override
    @Transactional
    public GenAssocTable commonSave(GenAssocTable assocTable) throws Exception {
        return this.commonSave(assocTable, false);
    }

    /**
     * 查询单个代码生成业务
     *
     * @param id
     * @return
     */
    @Override
    public GenAssocTable selectById(String id) {
        return this.assocTableMapper.selectById(id);
    }

    /**
     * 删除单个代码生成业务
     *
     * @param id
     * @return
     */
    @Override
    @Transactional
    public boolean removeById(String id) {
        // 删除
        boolean res = super.removeById(id);

        if (res) {

            // 删除关联字段
            this.assocColumnService.remove(Wrappers.<GenAssocTableColumn>lambdaQuery().eq(GenAssocTableColumn::getLinkId, id));
        }
        return res;
    }

    /**
     * 删除多个代码生成业务
     *
     * @param ids
     * @return
     */
    @Override
    @Transactional
    public boolean removeByIds(String ids) {
        // 删除
        List<String> idList = Arrays.asList(Convert.toStrArray(ids));
        boolean res = super.removeByIds(idList);

            if (res) {

                // 删除关联字段
                this.assocColumnService.remove(Wrappers.<GenAssocTableColumn>lambdaQuery().in(GenAssocTableColumn::getLinkId, idList));
            }
        return res;
    }

    @Override
    public List<GenAssocTable> listByLinkId(String linkId) {
        return this.assocTableMapper.selectByTableId(linkId);
    }
}