package com.ruoyi.generator.mapper;

import com.ruoyi.common.mapper.CommonMapper;
import com.ruoyi.generator.domain.GenJavaType;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Description: java类型Mapper接口
 * @Author: warden
 * @Date: 2021-02-27
 * @Version: V1.0
 */
@Mapper
public interface GenJavaTypeMapper extends CommonMapper<GenJavaType>
{
    /**
     * 用于分页查询
     *
     * @param javaType
     * @return
     */
    List<GenJavaType> search(GenJavaType javaType);

    /**
     * 用于查询单条记录
     *
     * @param id
     * @return
     */
    GenJavaType selectById(String id);

    /**
     * 获取所有属性
     *
     * @param javaType
     * @return
     */
    List<GenJavaType> listAll(GenJavaType javaType);
}