package com.ruoyi.generator.mapper;

import com.ruoyi.common.mapper.CommonMapper;
import com.ruoyi.generator.domain.GenAssocTable;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Description: 代码生成业务Mapper接口
 * @Author: warden
 * @Date: 2021-02-17
 * @Version: V1.0
 */
@Mapper
public interface GenAssocTableMapper extends CommonMapper<GenAssocTable>
{
    /**
     * 用于分页查询
     *
     * @param assocTable
     * @return
     */
    List<GenAssocTable> search(GenAssocTable assocTable);

    /**
     * 用于查询单条记录
     *
     * @param id
     * @return
     */
    GenAssocTable selectById(String id);

    List<GenAssocTable> selectByTableId(String linkId);
}