package com.ruoyi.generator.mapper;

import com.ruoyi.common.mapper.CommonMapper;
import com.ruoyi.generator.domain.GenTable;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 业务 数据层
 *
 * @author ruoyi
 */
@Mapper
public interface GenTableMapper extends CommonMapper<GenTable>
{
    /**
     * 查询业务列表
     *
     * @param genTable 业务信息
     * @return 业务集合
     */
    public List<GenTable> search(GenTable genTable);

    /**
     * 查询据库列表
     *
     * @param genTable 业务信息
     * @return 数据库表集合
     */
    public List<GenTable> selectDbTableList(GenTable genTable);

    /**
     * 查询据库列表
     *
     * @param tableNames 表名称组
     * @return 数据库表集合
     */
    public List<GenTable> selectDbTableListByNames(String[] tableNames);

    /**
     * 根据条件查询（包含字段信息）
     *
     * @return 表信息集合
     */
    public List<GenTable> selectAll(GenTable genTable);
}