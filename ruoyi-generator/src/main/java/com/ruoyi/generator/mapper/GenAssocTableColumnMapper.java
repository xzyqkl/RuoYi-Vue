package com.ruoyi.generator.mapper;

import com.ruoyi.common.mapper.CommonMapper;
import com.ruoyi.generator.domain.GenAssocTableColumn;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description: 关联字段Mapper接口
 * @Author: warden
 * @Date: 2021-02-17
 * @Version: V1.0
 */
@Mapper
public interface GenAssocTableColumnMapper extends CommonMapper<GenAssocTableColumn>
{

}