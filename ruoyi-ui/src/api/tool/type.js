import request from '@/utils/request'

// 查询java类型列表
export function listType(query) {
  return request({
    url: '/tool/type/list',
    method: 'get',
    params: query
  })
}

// 查询java类型详细
export function getType(id) {
  return request({
    url: '/tool/type/' + id,
    method: 'get'
  })
}

// 新增java类型
export function addType(data) {
  return request({
    url: '/tool/type',
    method: 'post',
    data: data
  })
}

// 修改java类型
export function updateType(data) {
  return request({
    url: '/tool/type',
    method: 'put',
    data: data
  })
}

// 删除java类型
export function delType(id) {
  return request({
    url: '/tool/type/' + id,
    method: 'delete'
  })
}

// 导出java类型
export function exportType(query) {
  return request({
    url: '/tool/type/export',
    method: 'get',
    params: query
  })
}

// 获取所有Java类型
export function listAllType() {
  return request({
    url: '/tool/type/listall',
    method: 'get',
    params: {}
  })
}
