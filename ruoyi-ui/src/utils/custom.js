/**
 * 处理图片路径
 * @param img
 * @returns {string[]|*}
 */
export function dealImg(img) {
  if (img == null || img == 'undefined') {
    return ['']
  }

  let imgs = img.split(',')
  for (let i in imgs) {
    imgs[i] = process.env.VUE_APP_BASE_API + imgs[i]
  }

  return imgs
}
