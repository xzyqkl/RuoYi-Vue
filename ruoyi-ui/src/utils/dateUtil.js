
var mons = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
function isLeapYear(year) {
  var  r = year / 100
  if (r === parseInt(r)) {
    r = year / 400
    return r === parseInt(r)
  }
  r = year / 4
  if (r === parseInt(r)) {
    return true
  }
  return false
}

function getDaysOfMonth(month, year) {
  if (month === 2 && isLeapYear(year)) {
    return 29
  }
  return mons[month]
}

function getMonthsOfYear(year) {
  if (isLeapYear(year)) {
    return 366
  }
  return 365
}

export function diffDate(d1, d2) {
  var arr1 = d1.split('-').map(Number)
  var arr2 = d2.split('-').map(Number)
  var [year, month, day] = arr2.map((n, i) => n - arr1[i])
  if (day < 0) {
    day += getDaysOfMonth(arr2[1], arr2[0])
    month--
  }
  if (month < 0) {
    month += getMonthsOfYear(arr2[0])
    year--
  }
  return [year, month, day]
}
