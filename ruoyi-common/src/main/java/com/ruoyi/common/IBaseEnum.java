package com.ruoyi.common;

import com.baomidou.mybatisplus.annotation.IEnum;

import java.io.Serializable;

/**
 * mybatis-plus通用枚举
 * 切记：如果需要序列号和反序列化，需要显示的属性（toString 或 标识@JsonValue)千万不要用Integer或int
 * @param <T>
 */
public interface IBaseEnum<T extends Serializable> extends IEnum<T> {
    String getDescription();
}
