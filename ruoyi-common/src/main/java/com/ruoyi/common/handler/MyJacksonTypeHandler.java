package com.ruoyi.common.handler;

import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import com.ruoyi.common.utils.EntityUtil;

public class MyJacksonTypeHandler extends JacksonTypeHandler {
    public MyJacksonTypeHandler(Class<?> type) {
        super(type);
    }

    @Override
    protected String toJson(Object obj) {
        if (obj == null) {
            return null;
        }
        EntityUtil.trimEntity(obj);
        return super.toJson(obj);
    }
}
