package com.ruoyi.common.handler;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import com.ruoyi.common.utils.ReflectUtil;

import java.lang.reflect.Field;
import java.util.List;

public class PrimaryKeyTypeHandler extends JacksonTypeHandler {
    public PrimaryKeyTypeHandler(Class<?> type) {
        super(type);
    }

    @Override
    protected String toJson(Object obj) {
        if (obj == null) {
            return null;
        }
        List<Field> fields = ReflectUtil.getAllFields(obj.getClass());
        for (Field field : fields) {
            boolean isTableField = field.isAnnotationPresent(TableId.class);
            if (isTableField) {
                try {
                    field.setAccessible(true);
                    if (field.get(obj) == null) {
                        return null;
                    }
                    return field.get(obj).toString();
                } catch (IllegalAccessException e) {
                    throw new RuntimeException();
                }
            }
        }
        throw new RuntimeException("未找到主键");
    }
}
