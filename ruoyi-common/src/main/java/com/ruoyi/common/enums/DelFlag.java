package com.ruoyi.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;
import com.ruoyi.common.IBaseEnum;
import lombok.Getter;

/**
 * 生成方式
 */
@Getter
public enum DelFlag implements IBaseEnum<String> {
    NORMAL("0", "正常"), DELETE("1", "删除");

    @JsonValue
    private String code;
    private String description;

    DelFlag(String code, String description) {
        this.code = code;
        this.description = description;
    }

    @Override
    public String getValue() {
        return this.code;
    }
}
