package com.ruoyi.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;
import com.ruoyi.common.IBaseEnum;
import lombok.Getter;

/**
 * 生成方式
 */
@Getter
public enum YesOrNo implements IBaseEnum<String> {
    YES("Y", "是"), NO("N", "否");

    @JsonValue
    private String code;
    private String description;

    YesOrNo(String code, String description) {
        this.code = code;
        this.description = description;
    }

    @Override
    public String getValue() {
        return this.code;
    }
}
