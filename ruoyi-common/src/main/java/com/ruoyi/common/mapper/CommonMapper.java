package com.ruoyi.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;

public interface CommonMapper<T> extends BaseMapper<T> {
    /**
     * 全量更新，不忽略null字段，等价于update
     * 解决mybatis-plus会自动忽略null字段不更新
     * {@link com.baomidou.mybatisplus.extension.injector.methods.AlwaysUpdateSomeColumnById}
     *
     * @param entity
     * @return
     */
    int alwaysUpdateSomeColumnById(@Param(Constants.ENTITY) T entity);
}
