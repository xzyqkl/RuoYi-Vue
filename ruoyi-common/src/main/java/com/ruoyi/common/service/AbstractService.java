package com.ruoyi.common.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.mapper.CommonMapper;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AbstractService<M extends CommonMapper<T>, T> extends ServiceImpl<M, T> implements BaseService<T> {

    @Override
    public int updateAll(T entity) {
        return this.baseMapper.alwaysUpdateSomeColumnById(entity);
    }
}
