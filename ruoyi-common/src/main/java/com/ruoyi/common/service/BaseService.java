package com.ruoyi.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.stereotype.Service;

@Service
public interface BaseService<T> extends IService<T> {

    /**
     * 全字段更新
     *
     * @param entity
     * @return
     */
    int updateAll(T entity);
}
