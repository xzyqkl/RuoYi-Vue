package com.ruoyi.common.utils;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class StringUtil extends org.apache.commons.lang3.StringUtils {

    private static final char SEPARATOR = '_';
    private static final String CHARSET_NAME = "UTF-8";
    public static final String ZG_FACILITY_NUMBER = "FA";
    public static final String ZG_FAC_MAINTENANCE_PLAN_NUMBER = "PL";
    public static final String ZG_FAC_MAINTENANCE_RESOURCE_NUMBER = "RE";
    public static final String ZG_FAC_FAULT_NUMBER = "GZ";
    public static final String ZG_FAC_FAULT_TYPE_NUMBER = "YY";
    public static final String ZG_FAC_OVERHAUL_PLAN_NUMBER = "JX";

    /**
     * 获取对象字符串
     *
     * @param obj
     * @return
     */
    public static String getObjectValue(Object obj) {
        return obj == null ? "" : obj.toString();
    }

    /**
     * 检查当前url是否在排除url中
     *
     * @param excludeUrl 需要过滤的uri字符串
     * @param requestUri 当前请求的uri
     * @return
     */
    public static boolean include(String excludeUrl, String requestUri) {
        if (excludeUrl != null && excludeUrl.length() > 0) {
            String[] customExcludes = excludeUrl.split(",");
            for (String e : customExcludes) {
                if (e.contains("*")) {
                    e = e.replace("*", "");
                    if (requestUri.startsWith(e)) {
                        return true;
                    }
                }
                if (requestUri.endsWith(e)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * 转换为字节数组
     *
     * @param str
     * @return
     */
    public static byte[] getBytes(String str) {
        if (str != null) {
            try {
                return str.getBytes(CHARSET_NAME);
            } catch (UnsupportedEncodingException e) {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * 转换为字节数组
     *
     * @param bytes
     * @return
     */
    public static String toString(byte[] bytes) {
        try {
            return new String(bytes, CHARSET_NAME);
        } catch (UnsupportedEncodingException e) {
            return EMPTY;
        }
    }

    /**
     * 是否包含字符串
     *
     * @param str  验证字符串
     * @param strs 字符串组
     * @return 包含返回true
     */
    public static boolean inString(String str, String... strs) {
        if (str != null) {
            for (String s : strs) {
                if (str.equals(trim(s))) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 替换掉HTML标签方法
     */
    public static String replaceHtml(String html) {
        if (isBlank(html)) {
            return "";
        }
        String regEx = "<.+?>";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(html);
        String s = m.replaceAll("");
        return s;
    }

    /**
     * 替换为手机识别的HTML，去掉样式及属性，保留回车。
     *
     * @param html
     * @return
     */
    public static String replaceMobileHtml(String html) {
        if (html == null) {
            return "";
        }
        return html.replaceAll("<([a-z]+?)\\s+?.*?>", "<$1>");
    }


    /**
     * 缩略字符串（不区分中英文字符）
     *
     * @param str    目标字符串
     * @param length 截取长度
     * @return
     */
    public static String abbr(String str, int length) {
        if (str == null) {
            return "";
        }
        try {
            StringBuilder sb = new StringBuilder();
            int currentLength = 0;
            for (char c : replaceHtml(StringEscapeUtils.unescapeHtml4(str)).toCharArray()) {
                currentLength += String.valueOf(c).getBytes("GBK").length;
                if (currentLength <= length - 3) {
                    sb.append(c);
                } else {
                    sb.append("...");
                    break;
                }
            }
            return sb.toString();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String abbr2(String param, int length) {
        if (param == null) {
            return "";
        }
        StringBuffer result = new StringBuffer();
        int n = 0;
        char temp;
        boolean isCode = false; // 是不是HTML代码
        boolean isHTML = false; // 是不是HTML特殊字符,如&nbsp;
        for (int i = 0; i < param.length(); i++) {
            temp = param.charAt(i);
            if (temp == '<') {
                isCode = true;
            } else if (temp == '&') {
                isHTML = true;
            } else if (temp == '>' && isCode) {
                n = n - 1;
                isCode = false;
            } else if (temp == ';' && isHTML) {
                isHTML = false;
            }
            try {
                if (!isCode && !isHTML) {
                    n += String.valueOf(temp).getBytes("GBK").length;
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            if (n <= length - 3) {
                result.append(temp);
            } else {
                result.append("...");
                break;
            }
        }
        // 取出截取字符串中的HTML标记
        String temp_result = result.toString().replaceAll("(>)[^<>]*(<?)",
                "$1$2");
        // 去掉不需要结素标记的HTML标记
        temp_result = temp_result
                .replaceAll(
                        "</?(AREA|BASE|BASEFONT|BODY|BR|COL|COLGROUP|DD|DT|FRAME|HEAD|HR|HTML|IMG|INPUT|ISINDEX|LI|LINK|META|OPTION|P|PARAM|TBODY|TD|TFOOT|TH|THEAD|TR|area|base|basefont|body|br|col|colgroup|dd|dt|frame|head|hr|html|img|input|isindex|li|link|meta|option|p|param|tbody|td|tfoot|th|thead|tr)[^<>]*/?>",
                        "");
        // 去掉成对的HTML标记
        temp_result = temp_result.replaceAll("<([a-zA-Z]+)[^<>]*>(.*?)</\\1>",
                "$2");
        // 用正则表达式取出标记
        Pattern p = Pattern.compile("<([a-zA-Z]+)[^<>]*>");
        Matcher m = p.matcher(temp_result);
        List<String> endHTML = new ArrayList<>();
        while (m.find()) {
            endHTML.add(m.group(1));
        }
        // 补全不成对的HTML标记
        for (int i = endHTML.size() - 1; i >= 0; i--) {
            result.append("</");
            result.append(endHTML.get(i));
            result.append(">");
        }
        return result.toString();
    }

    /**
     * 转换为Double类型
     */
    public static Double toDouble(Object val) {
        if (val == null) {
            return 0D;
        }
        try {
            return Double.valueOf(trim(val.toString()));
        } catch (Exception e) {
            return 0D;
        }
    }

    /**
     * 转换为Float类型
     */
    public static Float toFloat(Object val) {
        return toDouble(val).floatValue();
    }

    /**
     * 转换为Long类型
     */
    public static Long toLong(Object val) {
        return toDouble(val).longValue();
    }

    /**
     * 转换为Integer类型
     */
    public static Integer toInteger(Object val) {
        return toLong(val).intValue();
    }

    /**
     * 获得用户远程地址
     */
    public static String getRemoteAddr(HttpServletRequest request) {
        String remoteAddr = request.getHeader("X-Real-IP");
        if (isNotBlank(remoteAddr)) {
            remoteAddr = request.getHeader("X-Forwarded-For");
        } else if (isNotBlank(remoteAddr)) {
            remoteAddr = request.getHeader("Proxy-Client-IP");
        } else if (isNotBlank(remoteAddr)) {
            remoteAddr = request.getHeader("WL-Proxy-Client-IP");
        }
        return remoteAddr != null ? remoteAddr : request.getRemoteAddr();
    }

    /**
     * 驼峰命名法工具
     *
     * @return toCamelCase(" hello_world ") == "helloWorld"
     * toCapitalizeCamelCase("hello_world") == "HelloWorld"
     * toUnderScoreCase("helloWorld") = "hello_world"
     */
    public static String toCamelCase(String s) {
        if (s == null) {
            return null;
        }

        s = s.toLowerCase();

        StringBuilder sb = new StringBuilder(s.length());
        boolean upperCase = false;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);

            if (c == SEPARATOR) {
                upperCase = true;
            } else if (upperCase) {
                sb.append(Character.toUpperCase(c));
                upperCase = false;
            } else {
                sb.append(c);
            }
        }

        return sb.toString();
    }

    /**
     * 驼峰命名法工具
     *
     * @return toCamelCase(" hello_world ") == "helloWorld"
     * toCapitalizeCamelCase("hello_world") == "HelloWorld"
     * toUnderScoreCase("helloWorld") = "hello_world"
     */
    public static String toCapitalizeCamelCase(String s) {
        if (s == null) {
            return null;
        }
        s = toCamelCase(s);
        return s.substring(0, 1).toUpperCase() + s.substring(1);
    }

    /**
     * 驼峰命名法工具
     *
     * @return toCamelCase(" hello_world ") == "helloWorld"
     * toCapitalizeCamelCase("hello_world") == "HelloWorld"
     * toUnderScoreCase("helloWorld") = "hello_world"
     */
    public static String toUnderScoreCase(String s) {
        if (s == null) {
            return null;
        }

        StringBuilder sb = new StringBuilder();
        boolean upperCase = false;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);

            boolean nextUpperCase = true;

            if (i < (s.length() - 1)) {
                nextUpperCase = Character.isUpperCase(s.charAt(i + 1));
            }

            if ((i > 0) && Character.isUpperCase(c)) {
                if (!upperCase || !nextUpperCase) {
                    sb.append(SEPARATOR);
                }
                upperCase = true;
            } else {
                upperCase = false;
            }

            sb.append(Character.toLowerCase(c));
        }

        return sb.toString();
    }

    /**
     * 获取首字母
     *
     * @param str
     * @param separator
     * @return
     */
    public static String getFirstChar(String str, String separator) {

        if (isEmpty(str) || isEmpty(separator)) {
            return null;
        }

        String[] strs = str.split(separator);
        StringBuilder sb = new StringBuilder();
        for (String s : strs) {
            sb.append(s.substring(0, 1));
        }

        return sb.toString();
    }

    /**
     * 如果不为空，则设置值
     *
     * @param target
     * @param source
     */
    public static void setValueIfNotBlank(String target, String source) {
        if (isNotBlank(source)) {
            target = source;
        }
    }

    /**
     * 转换为JS获取对象值，生成三目运算返回结果
     *
     * @param objectString 对象串
     *                     例如：row.user.id
     *                     返回：!row?'':!row.user?'':!row.user.id?'':row.user.id
     */
    public static String jsGetVal(String objectString) {
        StringBuilder result = new StringBuilder();
        StringBuilder val = new StringBuilder();
        String[] vals = split(objectString, ".");
        for (int i = 0; i < vals.length; i++) {
            val.append("." + vals[i]);
            result.append("!" + (val.substring(1)) + "?'':");
        }
        result.append(val.substring(1));
        return result.toString();
    }

    /**
     * 隐藏敏感信息
     *
     * @param str userName
     * @return
     */
    public static String hideSensitiveStr(String str) {
        String userNameAfterReplaced = "";
        int nameLength = str.length();
        if (nameLength < 3 && nameLength > 0) {
            if (nameLength == 1) {
                userNameAfterReplaced = "*";
            } else {
                userNameAfterReplaced = str.replaceAll(str, "^.{1,2}");
            }
        } else {
            Integer num1, num2, num3;
            num2 = (new Double(Math.ceil(new Double(nameLength) / 3))).intValue();
            num1 = (new Double(Math.floor(new Double(nameLength) / 3))).intValue();
            num3 = nameLength - num1 - num2;
            String star = StringUtils.repeat("*", num2);
            userNameAfterReplaced = str.replaceAll("(.{" + num1 + "})(.{" + num2 + "})(.{" + num3 + "})", "$1" + star + "$3");
        }
        return userNameAfterReplaced;
    }

    /**
     * 判断是否手机号
     */
    public static boolean isChinaPhoneLegal(String str) throws PatternSyntaxException {
        String regExp = "^((13[0-9])|(14[5|7])|(15[^4])|(17[0-8])|(18[0,2,3,5-9]))\\d{8}$";
        Pattern p = Pattern.compile(regExp);
        Matcher m = p.matcher(str);
        return m.matches();
    }

    /**
     * 填充物
     *
     * @param targetStr 尾数是多少
     * @param prefix    前缀
     * @param length    总长度
     * @param fillChart 填充字符 （一位字符）
     * @return
     */
    public static String fillSomthing(String targetStr, String prefix, int length, String fillChart) {
        StringBuffer buf;
        if (isBlank(prefix)) {
            buf = new StringBuffer();
        } else {
            buf = new StringBuffer(prefix);
        }
        int prefixLength = prefix == null ? 0 : prefix.length();
        int targetStrLength = targetStr == null ? 0 : targetStr.length();
        if (targetStrLength < length - prefixLength) {
            for (int i = 0; i < length - prefixLength - targetStrLength; i++) {
                buf.append(fillChart);
            }
        }
        if (targetStr != null) {
            buf.append(targetStr);
        }
        return buf.toString();
    }

    // 获取
    public static String genCode(String prefix) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String dateStr = sdf.format(new Date());
        StringBuilder sb = new StringBuilder();
        sb.append(prefix).append(dateStr);
        return sb.toString();
    }

    public static String genCode(String code, String startWith, int length) {
        return genCode(code, startWith, length, "0");
    }

    public static String genCode(String code, String startWith, int length, String fillChart) {
        if (StringUtil.isBlank(code)) {
            return StringUtil.fillSomthing("1", startWith, length, fillChart);
        } else {
            code = code.replaceAll(startWith, "");
            Integer seqNo = Integer.parseInt(code);
            seqNo++;
            return StringUtil.fillSomthing(seqNo.toString(), startWith, length, fillChart);
        }
    }

    public static String getHoleScope(List<Integer> holeList) {
        // 1-3，4-5,7-13
        StringBuffer shelfTubeScope = new StringBuffer();
        StringBuffer tubeScope = new StringBuffer();
        // 处理试管号
        int length = shelfTubeScope.length();
        Integer tubeNo = null;
        for (int i = 0; i < holeList.size(); i++) {

            if (tubeNo != null) {
                if (holeList.get(i) != tubeNo + 1) {
                    append(tubeNo, tubeScope, shelfTubeScope);
                    tubeScope = new StringBuffer();
                    tubeNo = null;
                } else {
                    tubeNo = holeList.get(i);
                }
            }

            if (tubeNo == null) {
                tubeNo = holeList.get(i);
                if (shelfTubeScope.length() <= length) {
                    tubeScope.append(tubeNo).append("-");
                } else {
                    tubeScope.append(",").append(tubeNo).append("-");
                }
            }
        }
        if (tubeNo != null) {
            append(tubeNo, tubeScope, shelfTubeScope);
        }

        return shelfTubeScope.toString();
    }

    private static void append(Integer tubeNo, StringBuffer tubeScope, StringBuffer shelfTubeScope) {
        String[] strs = tubeScope.toString().split(",");
        String str = strs[strs.length - 1];
        str = str.split("-")[0];
//        String str = tubeScope.substring(tubeScope.length() - (tubeNo.toString().length() + 1), tubeScope.length() - 1);
        if (Integer.parseInt(str) == tubeNo) {
            String res = tubeScope.toString().substring(0, tubeScope.length() - 1);
            tubeScope = new StringBuffer(res);
        } else {
            tubeScope.append(tubeNo);
        }
        shelfTubeScope.append(tubeScope);
    }

    /**
     * 去掉所有空格
     *
     * @param str
     * @return
     */
    public static String trimBlank(String str) {
        if (isEmpty(str)) {
            return null;
        }

        return str.replaceAll(" +", "");
    }

    /**
     * 获取字符串锁
     *
     * @param lockStr
     * @return
     */
    public static String getLock(String lockStr) {
        StringBuilder builder = new StringBuilder().append(lockStr);
        return lockStr.intern(); // 放到常量池
    }

    /**
     * 随机生成6位较复杂密码
     *
     * @return
     */
    public static String generatePassword() {
        String[] pa = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O",
                "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
        StringBuffer sb = new StringBuffer("");
        for (int i = 0; i < 6; i++) {
            sb.append(pa[(Double.valueOf(Math.random() * pa.length).intValue())]);
        }
        String[] spe = {"`", "~", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "-", "_", "=", "+", "[", "]", "{", "}", "\\", "/", "?", ",", ".", "<", ">"};
        sb.append(spe[(Double.valueOf(Math.random() * spe.length).intValue())]);
        sb.append((int) (Math.random() * 100));
        return sb.toString();
    }

    public static String getCollectionStr(Collection collection) {
        if (collection == null) {
            return null;
        }

        StringBuilder sbuf = new StringBuilder();
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            Object obj = it.next();
            if (sbuf.length() == 0) {
                sbuf.append(obj);
            } else {
                sbuf.append(", ").append(obj);
            }
        }
        return sbuf.toString();
    }

    public static void main(String[] args) {
        System.out.println(genCode("TS202001060000023", "TS20200106", 15));
    }
}
