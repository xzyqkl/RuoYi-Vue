package com.ruoyi.common.utils;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.service.BaseService;
import com.ruoyi.common.utils.uuid.IdUtils;

import java.util.*;

/**
 * 比对列表数据，用于判断差异并保存到数据库中
 * 全量保存
 */
public class SaveDiffUtil {

    public static Map<String, Set<String>> saveFormDiff(List formList, List oriList, Class clz, BaseService service) throws Exception {
        return saveFormDiff(formList, oriList, clz, service, null, null);
    }

    public static Map<String, Set<String>> saveFormDiff(List formList, List oriList, Class clz, BaseService service, Boolean exeUpdate) throws Exception {
        return saveFormDiff(formList, oriList, clz, service, exeUpdate, null);
    }

    /**
     * 需要处理手工修改的记录
     *
     * @param formList    表单提交的数据
     * @param oriList     数据库数据
     * @param clz         类 类型
     * @param service      处理实体的service
     * @param exeUpdate   是否执行更新
     * @param updateAll   更新操作是否全字段更新
     * @throws Exception
     */
    public static Map<String, Set<String>> saveFormDiff(List formList, List oriList, Class clz, BaseService service,
                                                        Boolean exeUpdate, Boolean updateAll) throws Exception {
        if (exeUpdate == null) {
            exeUpdate = false;
        }
        if (updateAll == null) {
            updateAll = false;
        }
        // 需要比较的
        List<String> compareList = new ArrayList<>();
        // 原始数据
        List<String> currList = new ArrayList<>();

        if (formList == null) {
            formList = new ArrayList();
        }

        updateObject(formList, compareList);
        updateObject(oriList, currList);

        Map<String, Set<String>> result = EntityUtil.diffList(compareList, currList);

        if (result != null) {
            Set<String> addList = result.get(EntityUtil.ADD);
            Set<String> removeList = result.get(EntityUtil.REMOVE);
            Set<String> updateList = result.get(EntityUtil.NORMAL);

            // 添加的对象
            if (addList != null && addList.size() > 0) {
                List addObjList = new ArrayList();
                for (String id : addList) {
                    Iterator it = formList.iterator();
                    while (it.hasNext()) {
                        Object obj = it.next();
                        String objId = ReflectUtil.getFieldValue(obj, "id").toString();
                        if (id.equals(objId)) {
//                            BeanUtil.setCreateUser(currentUser, obj);
//                            BeanUtil.setUpdateUser(currentUser, obj);
                            addObjList.add(obj);
//                            it.remove(); 不可删除，需要返回
                            break;
                        }
                    }
                }
                if (addObjList != null && addObjList.size() > 0) {
                    service.saveBatch(addObjList);
                }
            }

            if (removeList != null && removeList.size() > 0) {
                // 处理删除的数据
                service.remove(Wrappers.query().in("id", removeList));
            }

            if (exeUpdate && updateList != null && updateList.size() > 0) {
                List updateObjList = new ArrayList();

                for (String id : updateList) {
                    Object compareObj = findObj(formList, id);
                    Object currObj = findObj(oriList, id);

                    // 比对数据库字段是否有修改
                    boolean diff = EntityUtil.diffObject(compareObj, currObj);
                    if (!diff) {
//                        EntityUtil.setUpdateUser(currentUser, compareObj);
                        updateObjList.add(compareObj);
                    }
                }

                if (updateObjList.size() > 0) {
                    for (Object obj : updateObjList) {
                        if (updateAll) {
                            // 全量更新
                            service.updateAll(obj);
                        } else {
                            // 更新不为空
                            service.updateById(obj);
                        }
                    }
                }
            }
        }

        return result;
    }

    private static void updateObject(List objList, List targetList, String idFieldName) {
        if (idFieldName == null) {
            idFieldName = "id";
        }

        for (Object obj : objList) {
            Object objStr = ReflectUtil.getFieldValue(obj, idFieldName);
            if (objStr != null) {
                targetList.add(objStr.toString());
            } else {
                String uuid = IdUtils.simpleUUID();
                targetList.add(uuid);
                ReflectUtil.setFieldValue(obj, "id", uuid);
            }
        }
    }

    private static void updateObject(List objList, List targetList) {
        updateObject(objList, targetList, null);
    }

    /**
     * 根据查询的属性找对象的key并赋值到目标列表中
     *
     * @param oriList
     * @param targetList
     * @param searchFieldName
     * @param idFieldName
     */
    public static void updateObject(List oriList, List targetList, String searchFieldName, String idFieldName) {
        if (oriList == null) {
            return;
        }
        for (Object oriObj : oriList) {
            Object searchStr = ReflectUtil.getFieldValue(oriObj, searchFieldName);

            if (searchStr != null) {
                for (Object tarObj : targetList) {
                    Object tarStr = ReflectUtil.getFieldValue(tarObj, searchFieldName);
                    if (searchStr.equals(tarStr)) {
                        Object idStr = ReflectUtil.getFieldValue(oriObj, idFieldName);
                        if (idStr != null) {
                            ReflectUtil.setFieldValue(tarObj, idFieldName, idStr);
                        }
                        continue;
                    }
                }
            }

        }
    }

    /**
     * 根据属性及属性值，从列表中查找对象
     *
     * @param objList
     * @param idFieldName
     * @param keyValue
     * @return
     */
    private static Object findObj(List objList, String idFieldName, String keyValue) {

        if (idFieldName == null) {
            idFieldName = "id";
        }

        Object result = null;
        for (Object obj : objList) {
            String idStr = ReflectUtil.getFieldValue(obj, idFieldName).toString();
            if (keyValue.equals(idStr)) {
                result = obj;
                break;
            }
        }

        return result;
    }

    private static Object findObj(List objList, String keyValue) {
        return findObj(objList, null, keyValue);
    }

    /**
     * 修改关系记录（根据linkID+pkId添加和删除）
     */
    /**
     * @param currentUser 当前用户
     * @param formList    表单提交的数据
     * @param oriList     数据库中的数据
     * @param clz         要插入表中的对象对应的类
     * @param findId      根据哪个属性查找传入的formList、oriList
     * @param pkIdName    根据哪个属性删除（添加）关系记录
     * @param linkIdName  外键字段名称
     * @param linkId      外键字段值
     * @param service
     * @throws Exception
     */
    public static void saveRelaDiff(SysUser currentUser,
                                    List formList,
                                    List oriList,
                                    Class clz,
                                    String findId,
                                    String pkIdName,
                                    String linkIdName,
                                    String linkId,
                                    BaseService service) throws Exception {
        // 需要比较的
        List<String> compareList = new ArrayList<>();
        // 原始数据
        List<String> currList = new ArrayList<>();

        updateObject(formList, compareList, findId);
        updateObject(oriList, currList, findId);

        Map<String, Set<String>> result = EntityUtil.diffList(compareList, currList);

        if (result != null) {
            Set<String> addList = result.get(EntityUtil.ADD);
            Set<String> removeList = result.get(EntityUtil.REMOVE);

            //删除数据
            if (removeList != null && removeList.size() > 0) {
                for (String removeId : removeList) {
                    service.remove(Wrappers.query()
                            .eq(linkIdName, linkId)
                            .eq(pkIdName, removeId)
                    );
                }
            }

            //添加资源
            if (addList != null && addList.size() > 0) {
                List addObjList = new ArrayList();
                for (String addId : addList) {
                    Object obj = clz.newInstance();
                    ReflectUtil.setFieldValue(obj, linkIdName, linkId);
                    ReflectUtil.setFieldValue(obj, pkIdName, addId);

                    addObjList.add(obj);
                }

                if (addObjList.size() > 0) {
                    service.saveBatch(addObjList);
                }
            }
        }
    }
}
