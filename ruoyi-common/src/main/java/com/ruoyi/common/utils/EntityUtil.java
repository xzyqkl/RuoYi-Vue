package com.ruoyi.common.utils;

import com.baomidou.mybatisplus.annotation.TableField;
import org.apache.commons.collections4.CollectionUtils;

import java.lang.reflect.Field;
import java.util.*;

public class EntityUtil {
    public static final String ADD = "ADD";
    public static final String REMOVE = "REMOVE";
    public static final String NORMAL = "NORMAL";

    public static void trimEntity(Object entity) {
        if (entity == null) {
            return;
        }
        // 获取实体类的所有属性，返回Field数组
        try {
            List<Field> fieldList = ReflectUtil.getAllFields(entity.getClass());
            for (Field field : fieldList) {
                field.setAccessible(true);

                if (field.getType() == String.class) {
                    String val = (String) field.get(entity);
                    if (StringUtils.isNotBlank(val) && !("null".equals(val) && "undefined".equals(val))) {
                        field.set(entity, val.trim());
                    } else {
                        field.set(entity, null);
                    }
                } else if (field.getType() == List.class) {
                    List list = (List) field.get(entity);
                    if (list != null) {
                        for (Object obj : list) {
                            trimEntity(obj);
                        }
                    }
                } else {
                    if (!isBasicType(field)) {
                        Object obj = field.get(entity);
                        if (obj != null) {
                            trimEntity(obj);
                        }
                    }

                }
            }
        } catch (IllegalAccessException e) {
        }
    }

    /**
     * 只有本地定义的类（不包括Enum）都认为是非基础类型
     *
     * @param field
     * @return
     */
    public static boolean isBasicType(Field field) {
        Class clz = field.getType();
        // 是否为基本类型
        if (clz.isPrimitive()) {
            return true;
        }
        if (clz.getPackage() == null) {
            return true;
        }
        if (clz.getPackage().getName() == null) {
            return true;
        }
        if (clz.getPackage().getName().startsWith("java.lang") ||
                clz.getPackage().getName().startsWith("java.math") ||
                clz.getPackage().getName().startsWith("java.util")) {
            return true;
        }

        if (clz.isEnum()) {
            return true;
        }

        if (clz.getPackage().getName().startsWith("fun.warden")) {
            return false;
        }

        return true;
    }

    /**
     * 比对两条记录数据库字段是否相同，相同返回true，不同返回false
     *
     * @param compare
     * @param current
     * @return
     */
    public static boolean diffObject(Object compare, Object current) {
        List<Field> fieldList = new ArrayList<>();

        List<Field> allFields = ReflectUtil.getAllFields(compare.getClass());

        String[] dontCompareFields = {"createBy", "createTime", "updateBy", "updateTime"};
        List<String> dCList = new ArrayList<>();
        dCList.addAll(Arrays.asList(dontCompareFields));
        for (Field field : allFields) {
            if (dCList.contains(field.getName())) {
                continue;
            }
            TableField tableField = field.getAnnotation(TableField.class);
            if (tableField != null && !tableField.exist()) {
                continue;
            }
            fieldList.add(field);
        }

        for (Field field : fieldList) {
            try {
                //抑制Java对其的检查
                field.setAccessible(true);

                //获取 object 中 field 所代表的属性值
                Object comp = field.get(compare);
                Object curr = field.get(current);
                if (comp != null && curr != null) {
                    if (!comp.toString().equals(curr.toString())) {
                        return false;
                    }
                } else {
                    if ((comp == null && curr != null) || (comp != null && curr == null)) {
                        return false;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    /**
     * 比较原List与现List的差异，返回Map
     * Map的三个KEY值：ADD(新增)\REMOVE（删除）\NORMAL（相同）
     *
     * @param compareList 需要比较的list(页面传过来的）
     * @param currList    当前的list（当前数据库中的）
     * @return
     */
    public static Map<String, Set<String>> diffList(Set<String> compareList, Set<String> currList) {

        if (CollectionUtils.isEmpty(compareList) && CollectionUtils.isEmpty(currList)) {
            return null;
        }

        Map<String, Set<String>> result = new HashMap<String, Set<String>>();

        result.put(ADD, new HashSet<String>());
        result.put(REMOVE, new HashSet<String>());
        result.put(NORMAL, new HashSet<String>());

        if (CollectionUtils.isEmpty(compareList)) {
            result.put(REMOVE, currList);
        } else if (CollectionUtils.isEmpty(currList)) {
            result.put(ADD, compareList);
        } else {
            for (String comStr : compareList) {
                if (currList.contains(comStr)) {
                    result.get(NORMAL).add(comStr);
                } else {
                    result.get(ADD).add(comStr);
                }
            }

            for (String currStr : currList) {
                if (compareList.contains(currStr)) {
                    result.get(NORMAL).add(currStr);
                } else {
                    result.get(REMOVE).add(currStr);
                }
            }
        }
        return result;
    }

    public static Map diffList(List<String> compareList, List<String> currList) {
        Set<String> compareSet = new HashSet<>();
        Set<String> currSet = new HashSet<>();

        if (CollectionUtils.isNotEmpty(compareList)) {
            for (String str : compareList) {
                compareSet.add(str);
            }
        }

        if (CollectionUtils.isNotEmpty(currList)) {
            for (String str : currList) {
                currSet.add(str);
            }
        }

        return diffList(compareSet, currSet);

    }
}
