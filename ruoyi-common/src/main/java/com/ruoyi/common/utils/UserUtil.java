package com.ruoyi.common.utils;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;

public class UserUtil {
    public static SysUser getUser() {
        SysUser currUser = null;
        LoginUser loginUser = (LoginUser) SecurityUtils.getAuthentication().getPrincipal();
        if (loginUser != null) {

            return loginUser.getUser();
        }

        return currUser;
    }
}
