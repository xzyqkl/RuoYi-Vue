package com.ruoyi.common.utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class ReflectUtil {

    /**
     * 循环向上转型, 获取对象的 DeclaredMethod
     *
     * @param object         : 子类对象
     * @param methodName     : 父类中的方法名
     * @param parameterTypes : 父类中的方法参数类型
     * @return 父类中的方法对象
     */
    public static Method getDeclaredMethod(Object object, String methodName, Class<?>... parameterTypes) {
        Method method = null;

        for (Class<?> clazz = object.getClass(); clazz != Object.class; clazz = clazz.getSuperclass()) {
            try {
                method = clazz.getDeclaredMethod(methodName, parameterTypes);
                return method;
            } catch (Exception e) {
                //这里甚么都不要做！并且这里的异常必须这样写，不能抛出去。
                //如果这里的异常打印或者往外抛，则就不会执行clazz = clazz.getSuperclass(),最后就不会进入到父类中了
            }
        }

        return null;
    }

    /**
     * 直接调用对象方法, 而忽略修饰符(private, protected, default)
     *
     * @param object         : 子类对象
     * @param methodName     : 父类中的方法名
     * @param parameterTypes : 父类中的方法参数类型
     * @param parameters     : 父类中的方法参数
     * @return 父类中方法的执行结果
     */
    public static Object invokeMethod(Object object, String methodName, Class<?>[] parameterTypes,
                                      Object[] parameters) {
        //根据 对象、方法名和对应的方法参数 通过反射 调用上面的方法获取 Method 对象
        Method method = getDeclaredMethod(object, methodName, parameterTypes);

        //抑制Java对方法进行检查,主要是针对私有方法而言
        method.setAccessible(true);

        try {
            if (null != method) {
                //调用object 的 method 所代表的方法，其方法的参数是 parameters
                return method.invoke(object, parameters);
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * 循环向上转型, 获取对象的 DeclaredField
     *
     * @param object    : 子类对象
     * @param fieldName : 父类中的属性名
     * @return 父类中的属性对象
     */
    public static Field getDeclaredField(Object object, String fieldName) {
        Field field = null;

        Class<?> clazz = object.getClass();

        for (; clazz != Object.class; clazz = clazz.getSuperclass()) {
            try {
                field = clazz.getDeclaredField(fieldName);
                return field;
            } catch (Exception e) {
                //这里甚么都不要做！并且这里的异常必须这样写，不能抛出去。
                //如果这里的异常打印或者往外抛，则就不会执行clazz = clazz.getSuperclass(),最后就不会进入到父类中了

            }
        }

        return null;
    }

    /**
     * 直接设置对象属性值, 忽略 private/protected 修饰符, 也不经过 setter
     *
     * @param object    : 子类对象
     * @param fieldName : 父类中的属性名
     * @param value     : 将要设置的值
     */

    public static void setFieldValue(Object object, String fieldName, Object value) {

        //根据 对象和属性名通过反射 调用上面的方法获取 Field对象
        Field field = getDeclaredField(object, fieldName);

        try {
            //抑制Java对其的检查
            field.setAccessible(true);
            //将 object 中 field 所代表的值 设置为 value
            field.set(object, value);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {

        }

    }

    /**
     * 直接读取对象的属性值, 忽略 private/protected 修饰符, 也不经过 getter
     *
     * @param object    : 子类对象
     * @param fieldName : 父类中的属性名
     * @return : 父类中的属性值
     */
    public static Object getFieldValue(Object object, String fieldName) {

        //根据 对象和属性名通过反射 调用上面的方法获取 Field对象
        Field field = getDeclaredField(object, fieldName);

        try {
            //抑制Java对其的检查
            field.setAccessible(true);

            //获取 object 中 field 所代表的属性值
            return field.get(object);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * 拷贝srcObj属性值到targetObj属性中
     *
     * @param srcObj
     * @param tarObj
     */
    public static void copyFieldValue(Object srcObj, Object tarObj) {
        List<Field> srcFieldList = getAllFields(srcObj.getClass());
        List<Field> tarFieldList = getAllFields(tarObj.getClass());

        for (Field srcField : srcFieldList) {
            for (Field tarField : tarFieldList) {
                if (srcField.getName().equals(tarField.getName()) && srcField.getType().equals(tarField.getType())) {
                    Object value = getFieldValue(srcObj, srcField.getName());
                    setFieldValue(tarObj, srcField.getName(), value);
                }
            }
        }
    }

    /**
     * 拷贝srcObj的值到tarObj
     *
     * @param srcObj
     * @param tarObj
     */
    public static void copyFieldValueNoNull(Object srcObj, Object tarObj) {
        List<Field> srcFieldList = getAllFields(srcObj.getClass());
        List<Field> tarFieldList = getAllFields(tarObj.getClass());

        for (Field srcField : srcFieldList) {
            for (Field tarField : tarFieldList) {
                if (srcField.getName().equals(tarField.getName()) && srcField.getType().equals(tarField.getType())) {
                    Object value = getFieldValue(srcObj, srcField.getName());
                    if (value != null) {
                        setFieldValue(tarObj, srcField.getName(), value);
                    }
                }
            }
        }
    }

    /**
     * 获取所有字段属性
     *
     * @param clz
     * @return
     */
    public static List<Field> getAllFields(Class clz) {
        List<Field> fieldList = new ArrayList<>();

        for (; clz != Object.class; clz = clz.getSuperclass()) {
            fieldList.addAll(Arrays.asList(clz.getDeclaredFields()));
        }

        return fieldList;
    }

    public static boolean hasField(final Object obj, final String fieldName) {
        Field field = getDeclaredField(obj, fieldName);
        if (field == null) {
            return false;
        }
        return true;
    }

    public static Map.Entry<String, String> validNotNull(Map<String, String> validFields, Object object) {
        for (Map.Entry<String, String> entry : validFields.entrySet()) {
            String field = entry.getKey();
            Object value = ReflectUtil.getFieldValue(object, field);
            if (value == null || (value instanceof String && StringUtils.isBlank((String) value))) {
                return entry;
            }
        }
        return null;
    }

    /**
     * 重置字段值（不在列表中的字段）
     *
     * @param allFieldList
     * @param object
     */
    public static void resetFieldNotInList(List<String> allFieldList, Object object) {
        List<Field> fieldList = getAllFields(object.getClass());

        for (Field field : fieldList) {
            if (!allFieldList.contains(field.getName())) {
                // 将值设置为空
                setFieldValue(object, field.getName(), null);
            }
        }
    }

    public static Object getAnnotationValue(Annotation annotation, String property) {
        Object result = null;
        if (annotation != null) {
            InvocationHandler invo = Proxy.getInvocationHandler(annotation); //获取被代理的对象
            Map map = (Map) getFieldValue(invo, "memberValues");
            if (map != null) {
                result = map.get(property);
            }
        }
        return result;
    }
}