package com.ruoyi.framework.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.UserUtil;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * mybatis-plus 字段填充功能
 */
@Component
public class MetaObjectHandlerConfig implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        SysUser user = UserUtil.getUser();
        if (user != null) {
            Date now = new Date();
            setFieldValByName("createBy", user.getUserId().toString(), metaObject);
            setFieldValByName("createTime", now, metaObject);
            setFieldValByName("updateBy", user.getUserId().toString(), metaObject);
            setFieldValByName("updateTime", now, metaObject);
        }
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        SysUser user = UserUtil.getUser();
        if (user != null) {
            Date now = new Date();
            setFieldValByName("updateBy", user.getUserId().toString(), metaObject);
            setFieldValByName("updateTime", now, metaObject);
        }
    }
}