package com.ruoyi.framework.convert;

import com.ruoyi.common.IBaseEnum;
import org.springframework.core.convert.converter.Converter;

import java.util.HashMap;
import java.util.Map;

public class EnumConverter<T extends IBaseEnum> implements Converter<String, T>
{
    private Map<String, T> enumMap = new HashMap();

    public EnumConverter(Class<T> enumType) {
        T[] enums = enumType.getEnumConstants();
        for (T e : enums) {
            enumMap.put((String) e.getValue(), e);
        }
    }

    @Override
    public T convert(String source) {
        T t = enumMap.get(source);
        if (t == null) {
            throw new IllegalArgumentException("无法匹配对应的枚举类型");
        }
        return t;
    }
}
