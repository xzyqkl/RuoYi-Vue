package com.ruoyi.framework.convert;

import com.ruoyi.common.IBaseEnum;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.converter.ConverterFactory;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class EnumConverterFactory implements ConverterFactory<String, IBaseEnum>
{
    private static final Map<Class, Converter> CONVERTERS = new HashMap();

    @Override
    public <T extends IBaseEnum> Converter<String, T> getConverter(Class<T> targetType) {
        Converter<String, T> converter = CONVERTERS.get(targetType);
        if (converter == null) {
            converter = new EnumConverter(targetType);
            CONVERTERS.put(targetType, converter);
        }
        return converter;
    }
}
